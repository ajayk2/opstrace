// export const getInfrastructureName = (
//   org: string,
//   clusterName: string
// ): string => {
//   // Note(JP): this appears to be an infrastructure component name prefix.
//   // We could make it a property of `stack` and call it `.infraNamePrefix`.
//   // `org` already is a property on `stack`, and `name` also is.
//   // Update(JP): slowly carving this out, also as part of "org removal".
//   // we might want to go back later and add a concept for an infrastructure
//   // prefix but as of now there is no clear need, and in terms of resource
//   // name length limitation that might actually reduce the amount of
//   // meaningful information we can encode in resource names (the Opstrace
//   // cluster name should be _guaranteed_ to be part of resource names).
//   return `${org}-${clusterName}`;
// };

export function getBucketName({
  clusterName,
  suffix
}: {
  clusterName: string;
  suffix: string;
}): string {
  return `${clusterName}-${suffix}`;
}
