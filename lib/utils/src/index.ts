export * from "./common";
export * from "./die";
export * from "./docker";
export * from "./time";
export * from "./math";
export * from "./log";
export * from "./file";
export * from "./httpclient";
export * from "./naming";
export * from "./sagas";
export * from "./errors";
export * from "./diffutils";
export * from "./merge";
export { BUILD_INFO } from "./buildinfo";

export {
  sleep,
  mtime,
  mtimeDiffSeconds,
  mtimeDeadlineInSeconds
} from "./deadline";

export function hasUpperCase(s: string): boolean {
  return s.toLowerCase() != s;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export declare interface Dict<T = any> {
  [key: string]: T;
}
