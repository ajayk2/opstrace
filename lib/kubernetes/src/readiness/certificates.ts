import { V1CertificateResourceType } from "../custom-resources";

export function getCertificateRolloutMessage(
  c: V1CertificateResourceType
): string {
  const certificate = c.spec;
  const conditions = certificate.status?.conditions ?? [];

  for (const c of conditions) {
    if (c.reason == "Ready" && c.status == "True" && c.type == "Ready") {
      // certificate is ready
      return "";
    }
  }

  return `Waiting for Certificate ${c.namespace}/${c.name} to be ready`;
}
