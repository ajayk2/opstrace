import {
  certificaterequests,
  certificates,
  challenges,
  clusterIssuers,
  issuers,
  orders,
  clickhouseinstallations,
  clickhouseinstallationtemplates,
  clickhouseoperatorconfigurations,
  cortices,
  jaegers,
  alertmanager,
  podmonitor,
  probe,
  prometheus,
  prometheusrule,
  servicemonitor,
  thanosruler
} from "../src/crds";

import { GenerateCodeForCRD } from "./apigen";

const outDir = `${__dirname}/../src/custom-resources`;

[
  certificaterequests,
  certificates,
  challenges,
  clusterIssuers,
  issuers,
  orders,

  clickhouseinstallations,
  clickhouseinstallationtemplates,
  clickhouseoperatorconfigurations,

  cortices,

  jaegers,

  alertmanager,
  podmonitor,
  probe,
  prometheus,
  prometheusrule,
  servicemonitor,
  thanosruler
].map(async crd => {
  try {
    await GenerateCodeForCRD(crd, outDir);
    console.log(`Successfully generated Resource for: ${crd.spec.names.kind}`);
  } catch (e: any) {
    console.error(
      `\nFailed to generate Resource for: ${crd.spec.names.kind}\n`
    );
    console.error(e);
  }
});
