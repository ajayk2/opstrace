export type DNSRecord = {
  kind?: string;
  name?: string;
  ttl?: number;
  type?: string;
  rrdatas?: string[];
};

export type DNSZone = {
  name?: string;
  dnsName?: string;
  records?: DNSRecord[];
};

// Denotes support for these 2 providers
export type Provider = "aws" | "gcp";

export type PartialRequired<T, K extends keyof T> = Omit<T, K> &
  Required<Pick<T, K>>;
