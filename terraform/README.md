# Opstrace Terraform

## Requirements

* Tested with terraform version 0.15.3.
* Google GCP credentials

## Instructions

[Set up GCP credentials](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started#adding-credentials).

Then move to the test directory to set up an Opstrace test instance:

```bash
cd environments/test
```

Initialize terraform:

```bash
terraform init
```

Edit `terraform.tfvars` and pick a unique `instance_name` and set up the `custom_dns_name`:

```yaml
project_id = "vast-pad-240918"

instance_name = "sreis-example"
num_nodes     = 3
region        = "us-central1"
location      = "us-central1-a"

# This requires setting up a DNS zone in the GCP project manually.
custom_dns_name = sreis.opstracegcp.com
```

Proceed to set up the infrastructure and install the Opstrace controller:

```bash
terraform apply
```

## Cleanup

To destroy your Opstrace instance run:

```bash
terraform destroy
```

This instructs terraform to set the controller `terminate` field to true and trigger the controller to delete all the resources, including all the Persistent Volumes.

Terraform will then wait for the Kubernetes namespaces created by the controller to be deleted before proceeding to destroy the infrastructure.

In the event the controller is unable to delete all the resources you can manually run the following command in a separate terminal, while the terraform destroy operation is running:

```bash
kubectl delete ns application clickhouse clickhouse-operator-system cortex-operator-system cortex default-tenant ingress jaeger-operator-system monitoring system-tenant
```

## Known issues and workarounds

### Error: Post `https://XXX.XXX.XXX.XXX/api/*`: x509: certificate signed by unknown authority

The initial `terraform apply` might fail with the following errors:

```text
│ Error: Post "https://35.232.222.215/api/v1/namespaces/default/configmaps": x509: certificate signed by unknown authority
│
│   with kubernetes_config_map.opstrace_tenants_database,
│   on manifests.tf line 64, in resource "kubernetes_config_map" "opstrace_tenants_database":
│   64: resource "kubernetes_config_map" "opstrace_tenants_database" {
│
╵
╷
│ Error: Post "https://35.232.222.215/api/v1/namespaces/kube-system/secrets": x509: certificate signed by unknown authority
│
│   with kubernetes_secret.hasura_admin_secret,
│   on manifests.tf line 79, in resource "kubernetes_secret" "hasura_admin_secret":
│   79: resource "kubernetes_secret" "hasura_admin_secret" {
│
╵
╷
│ Error: Post "https://35.232.222.215/api/v1/namespaces/kube-system/serviceaccounts": x509: certificate signed by unknown authority
│
│   with kubernetes_service_account.opstrace_controller_sa,
│   on manifests.tf line 92, in resource "kubernetes_service_account" "opstrace_controller_sa":
│   92: resource "kubernetes_service_account" "opstrace_controller_sa" {
│
╵
╷
│ Error: Post "https://35.232.222.215/apis/rbac.authorization.k8s.io/v1/clusterrolebindings": x509: certificate signed by unknown authority                                    │
│   with kubernetes_cluster_role_binding.opstrace_controller_rb,
│   on manifests.tf line 99, in resource "kubernetes_cluster_role_binding" "opstrace_controller_rb":
│   99: resource "kubernetes_cluster_role_binding" "opstrace_controller_rb" {
│
╵
```

If this happens to you rerun `terraform apply` and it should be able to create the Kubernetes resources.
