module "opstrace" {
  source = "../../modules/opstrace/gcp"

  project_id = var.project_id

  instance_name = var.instance_name
  region        = var.region
  location      = var.location
  num_nodes     = var.num_nodes

  # This requires setting up a DNS zone in the GCP project manually.
  custom_dns_name = var.custom_dns_name

  custom_auth0_client_id = var.custom_auth0_client_id
  custom_auth0_domain    = var.custom_auth0_domain

  controller_image = var.controller_image

  metric_retention_days = var.metric_retention_days

  # Save the kubeconfig to access the Kubernetes cluster in the current folder
  kubeconfig_path = "${path.module}/kubeconfig"
}
