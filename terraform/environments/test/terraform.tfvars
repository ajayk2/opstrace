project_id = "vast-pad-240918"

instance_name = "sreis-example"
num_nodes     = 3
region        = "us-central1"
location      = "us-central1-a"

# This requires setting up a DNS zone in the GCP project manually.
custom_dns_name = "sreis-example.opstracegcp.com"

custom_auth0_client_id = "5MoCYfPXPuEzceBLRUr6T6SAklT2GDys"
custom_auth0_domain    = "opstrace-dev.us.auth0.com"

metric_retention_days = 7
trace_retention_days = 7

controller_image = "opstrace/controller:30d8b2353-dev"
