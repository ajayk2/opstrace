resource "google_storage_bucket" "cortex" {
  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      # TODO: expose in variables
      age        = 7
      with_state = "ANY"
    }

  }

  location      = var.region
  name          = "${var.instance_name}-cortex"
  project       = var.project_id
  storage_class = "STANDARD"

  force_destroy = true
}

resource "google_storage_bucket" "cortex_config" {
  location      = var.region
  name          = "${var.instance_name}-cortex-config"
  project       = var.project_id
  storage_class = "STANDARD"

  force_destroy = true
}
