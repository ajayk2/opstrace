# GKE cluster
resource "google_container_cluster" "primary" {
  name     = var.instance_name
  location = var.location

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  private_cluster_config {
    enable_private_nodes    = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = "172.16.0.16/28"
  }

  master_authorized_networks_config {
    cidr_blocks {
      display_name = "all"
      cidr_block   = "0.0.0.0/0"
    }
  }

  ip_allocation_policy {
    cluster_ipv4_cidr_block  = "10.148.0.0/14"
    services_ipv4_cidr_block = "10.152.0.0/20"
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  name = "${google_container_cluster.primary.name}-node-pool"
  # location - (Optional) The location (region or zone) in which the cluster
  # master will be created, as well as the default node location. If you specify
  # a zone (such as us-central1-a), the cluster will be a zonal cluster with a
  # single cluster master. If you specify a region (such as us-west1), the
  # cluster will be a regional cluster with multiple masters spread across zones
  # in the region, and with default node locations in those zones as well
  location = var.location

  cluster    = google_container_cluster.primary.name
  node_count = var.num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = {
      env = var.project_id
    }

    # preemptible  = true
    # TODO: make this a variable?
    machine_type = "n1-standard-4"
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

# Separately Managed Node Pool for Cortex
resource "google_container_node_pool" "cortex_nodes" {
  name = "${google_container_cluster.primary.name}-cortex-node-pool"
  # location - (Optional) The location (region or zone) in which the cluster
  # master will be created, as well as the default node location. If you specify
  # a zone (such as us-central1-a), the cluster will be a zonal cluster with a
  # single cluster master. If you specify a region (such as us-west1), the
  # cluster will be a regional cluster with multiple masters spread across zones
  # in the region, and with default node locations in those zones as well
  location = var.location

  cluster    = google_container_cluster.primary.name
  node_count = var.num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = {
      env = var.project_id
    }

    # preemptible  = true
    # TODO: make this a variable?
    machine_type = "n1-standard-4"
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

# Separately Managed Node Pool for Cortex
resource "google_container_node_pool" "cortex_ingester_nodes" {
  name = "${google_container_cluster.primary.name}-cortex-ingester-node-pool"
  # location - (Optional) The location (region or zone) in which the cluster
  # master will be created, as well as the default node location. If you specify
  # a zone (such as us-central1-a), the cluster will be a zonal cluster with a
  # single cluster master. If you specify a region (such as us-west1), the
  # cluster will be a regional cluster with multiple masters spread across zones
  # in the region, and with default node locations in those zones as well
  location = var.location

  cluster    = google_container_cluster.primary.name
  node_count = var.num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = {
      env = var.project_id
    }

    # preemptible  = true
    # TODO: make this a variable?
    machine_type = "n1-standard-4"
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

# Separately Managed Node Pool for Cortex
resource "google_container_node_pool" "ingress_nodes" {
  name = "${google_container_cluster.primary.name}-ingress-node-pool"
  # location - (Optional) The location (region or zone) in which the cluster
  # master will be created, as well as the default node location. If you specify
  # a zone (such as us-central1-a), the cluster will be a zonal cluster with a
  # single cluster master. If you specify a region (such as us-west1), the
  # cluster will be a regional cluster with multiple masters spread across zones
  # in the region, and with default node locations in those zones as well
  location = var.location

  cluster    = google_container_cluster.primary.name
  node_count = var.num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = {
      env = var.project_id
    }

    # preemptible  = true
    # TODO: make this a variable?
    machine_type = "n1-standard-4"
    tags         = ["gke-node", "${var.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}
