resource "random_id" "db_name_suffix" {
  byte_length = 16
}

resource "google_compute_global_address" "private_ip_address" {
  name          = "google-managed-services-${google_compute_network.vpc.name}"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  address       = "192.168.64.0"
  prefix_length = 19
  network       = google_compute_network.vpc.id
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = google_compute_network.vpc.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

# TODO: noticed it created the sql instance with a public ip
resource "google_sql_database_instance" "postgres" {
  depends_on = [
    google_compute_network.vpc,
    google_compute_global_address.private_ip_address,
    google_service_networking_connection.private_vpc_connection,
  ]

  name                = "${var.instance_name}-${random_id.db_name_suffix.hex}"
  project             = var.project_id
  region              = var.region
  database_version    = "POSTGRES_11"
  deletion_protection = false

  settings {
    user_labels = {
      opstrace_cluster_name = var.instance_name
    }

    activation_policy = "ALWAYS"
    availability_type = "ZONAL"
    backup_configuration {
      enabled                        = true
      start_time                     = "11:00"
      transaction_log_retention_days = 7

      backup_retention_settings {
        retained_backups = 7
        retention_unit   = "COUNT"
      }
    }

    disk_autoresize = true
    disk_size       = 10
    disk_type       = "PD_SSD"
    ip_configuration {
      private_network = google_compute_network.vpc.id
      ipv4_enabled    = false
    }

    location_preference {
      zone = var.location
    }

    pricing_plan = "PER_USE"
    tier         = "db-custom-2-3840"
  }
}

resource "google_sql_database" "opstrace" {
  name     = "opstrace"
  instance = google_sql_database_instance.postgres.name
}

# The terraform provider does not support setting the root_password:
# https://github.com/hashicorp/terraform-provider-google/issues/10122
# so we define a sql user instead
resource "google_sql_user" "opstrace" {
  project  = var.project_id
  name     = "opstrace"
  instance = google_sql_database_instance.postgres.name
  # TODO: secure this or move to IAM
  password = "2020WasQuiteTheYear"

  # deletion_policy - (Optional) The deletion policy for the user. Setting
  # ABANDON allows the resource to be abandoned rather than deleted. This is
  # useful for Postgres, where users cannot be deleted from the API if they have
  # been granted SQL roles.
  deletion_policy = "ABANDON"
}
