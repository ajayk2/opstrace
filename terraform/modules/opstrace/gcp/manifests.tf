# Kubernetes provider
# The Terraform Kubernetes Provider configuration below is used as a learning reference only.
# It references the variables and resources provisioned in this file.
# We recommend you put this in another file -- so you can have a more modular configuration.
# https://learn.hashicorp.com/terraform/kubernetes/provision-gke-cluster#optional-configure-terraform-kubernetes-provider
# To learn how to schedule deployments and services using the provider, go here: https://learn.hashicorp.com/tutorials/terraform/kubernetes-provider.

# provider "kubernetes" {
#   load_config_file = "false"
#   host     = google_container_cluster.primary.endpoint
#
#   client_certificate     = google_container_cluster.primary.master_auth.0.client_certificate
#   client_key             = google_container_cluster.primary.master_auth.0.client_key
#   cluster_ca_certificate = google_container_cluster.primary.master_auth.0.cluster_ca_certificate
# }

# Configure kubernetes provider with Oauth2 access token.
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config
# This fetches a new token, which will expire in 1 hour.
data "google_client_config" "primary" {
  depends_on = [google_container_node_pool.primary_nodes]
}

# Defer reading the cluster data until the GKE cluster exists.
data "google_container_cluster" "primary" {
  name       = var.instance_name
  depends_on = [google_container_cluster.primary]
}

provider "kubernetes" {
  host  = "https://${data.google_container_cluster.primary.endpoint}"
  token = data.google_client_config.primary.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.primary.master_auth[0].cluster_ca_certificate,
  )
}

resource "kubernetes_config_map" "opstrace_controller_config" {
  # TODO: instead of tagging all the resources, is there a better way to achieve
  # this?
  depends_on = [
    null_resource.configure_kubectl
  ]

  metadata {
    name      = "opstrace-controller-config"
    namespace = "default"
  }
  data = {
    "config.json" = jsonencode({
      "name": var.instance_name,
      "target": "gcp",
      "region": var.region,

      "tlsCertificateIssuer": var.cert_issuer,
      "infrastructureName": var.instance_name,

      "metricRetentionDays": var.metric_retention_days,
      "traceRetentionDays": var.trace_retention_days,

      "dnsName": "opstrace.io.",

      "terminate": false,
      "controllerTerminated": false,

      "uiSourceIpFirewallRules": ["0.0.0.0/0"],
      "apiSourceIpFirewallRules": ["0.0.0.0/0"],

      "custom_dns_name": var.custom_dns_name,
      "custom_auth0_client_id": var.custom_auth0_client_id,
      "custom_auth0_domain": var.custom_auth0_domain,
      "gcp": {
        "projectId": var.project_id,
        "certManagerServiceAccount": google_service_account.certmanager_service_account.email,
        "externalDNSServiceAccount": google_service_account.externaldns_service_account.email,
        "cortexServiceAccount": google_service_account.cortex_service_account.email,
      },
      "postgreSQLEndpoint": "postgres://${google_sql_user.opstrace.name}:${google_sql_user.opstrace.password}@${google_sql_database_instance.postgres.private_ip_address}:5432/",
      "opstraceDBName": "opstrace",

      # TODO: expose in variables
      "gitlab": {
        "instance_url": "https://gitlab.com",
        "group_allowed_access": "6543",
        "group_allowed_system_access": "6543",
        "oauth_client_id": "GITLAB_OAUTH_CLIENT_ID",
        "oauth_client_secret": "GITLAB_OAUTH_CLIENT_SECRET",
      },

      node_selector_terms: {
        # By default run pods in this node pool.
        "default": {
          "key": "cloud.google.com/gke-nodepool",
          "values": [ google_container_node_pool.primary_nodes.name ]
        },
        "rules": [
          # Run the cortex ingester in a specific node pool
          {
            "kind": "statefulset",
            "namespace": "cortex",
            "name": "ingester",
            "match_expression": { "key": "cloud.google.com/gke-nodepool", "values": [ google_container_node_pool.cortex_ingester_nodes.name ] }
          },
          # Run all the other cortex components in a different node pool
          {
            "kind": "any",
            "namespace": "cortex",
            "name": "",
            "match_expression": { "key": "cloud.google.com/gke-nodepool", "values": [ google_container_node_pool.cortex_nodes.name ] }
          },
          # Run all the pods in the ingress namespace in a separate node pool.
          {
            "kind": "any",
            "namespace": "ingress",
            "name": "",
            "match_expression": { "key": "cloud.google.com/gke-nodepool", "values": [ google_container_node_pool.ingress_nodes.name ] }
          }
        ]
      }
    })
  }
}

resource "kubernetes_config_map" "opstrace_tenants_database" {
  # TODO: instead of tagging all the resources, is there a better way to achieve
  # this?
  depends_on = [
    null_resource.configure_kubectl
  ]

  metadata {
    name      = "opstrace-tenants-database"
    namespace = "default"
    annotations = {
      opstrace = "protected"
    }
  }
  data = {
    "tenants.json" = jsonencode([{ "name": "system", "type": "SYSTEM" }, { "name": "default", "type": "USER" }])
  }
}

# TODO: create a secret for the docker hub credentials

resource "kubernetes_secret" "hasura_admin_secret" {
  # TODO: instead of tagging all the resources, is there a better way to achieve
  # this?
  depends_on = [
    null_resource.configure_kubectl
  ]

  metadata {
    name      = "hasura-admin-secret"
    namespace = "kube-system"
    annotations = {
      "opstrace" = "no-update"
    }
  }
  data = {
    HASURA_ADMIN_SECRET = "supersecretpassword"
  }
}

resource "kubernetes_secret" "postgres-secret" {
  # TODO: instead of tagging all the resources, is there a better way to achieve
  # this?
  depends_on = [
    null_resource.configure_kubectl
  ]

  metadata {
    name      = "postgres-secret"
    namespace = "kube-system"
  }

  data = {
    endpoint = "postgres://${google_sql_user.opstrace.name}:${google_sql_user.opstrace.password}@${google_sql_database_instance.postgres.private_ip_address}:5432/"
  }
}

resource "kubernetes_service_account" "opstrace_controller_sa" {
  # TODO: instead of tagging all the resources, is there a better way to achieve
  # this?
  depends_on = [
    null_resource.configure_kubectl
  ]

  metadata {
    name      = "opstrace-controller"
    namespace = "kube-system"
  }
}

resource "kubernetes_cluster_role_binding" "opstrace_controller_rb" {
  # TODO: instead of tagging all the resources, is there a better way to achieve
  # this?
  depends_on = [
    null_resource.configure_kubectl
  ]

  metadata {
    name = "opstrace-controller-clusteradmin-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "opstrace-controller"
    namespace = "kube-system"
  }
}

resource "kubernetes_deployment" "opstrace_controller" {
  depends_on = [
    google_sql_database_instance.postgres,
    kubernetes_service_account.opstrace_controller_sa,
    kubernetes_cluster_role_binding.opstrace_controller_rb,
    kubernetes_config_map.opstrace_tenants_database,
    kubernetes_config_map.opstrace_controller_config
  ]

  metadata {
    name      = "opstrace-controller"
    namespace = "kube-system"
    labels = {
      k8sapp = "opstrace-controller"
    }
  }

  spec {
    replicas = 1

    strategy {
      type = "Recreate"
    }
    selector {
      match_labels = {
        name = "opstrace-controller"
      }
    }

    template {
      metadata {
        labels = {
          name = "opstrace-controller"
        }
      }

      spec {
        # TODO: set if docker hub credentials are configured
        # imagePullSecrets= getImagePullSecrets()
        service_account_name = "opstrace-controller"
        container {
          name              = "opstrace-controller"
          image             = var.controller_image
          image_pull_policy = "Always"
          command           = ["node", "./cmd.js"]
          args              = ["${var.instance_name}"]
          resources {
            limits = {
              cpu    = "1"
              memory = "1Gi"
            }
            requests = {
              cpu    = "0.5"
              memory = "500Mi"
            }
          }
          port {
            name           = "metrics"
            container_port = 8900
          }
          port {
            name           = "readiness"
            container_port = 9000
          }

          env {
            name = "HASURA_GRAPHQL_ADMIN_SECRET"
            value_from {
              secret_key_ref {
                name = "hasura-admin-secret"
                key  = "HASURA_ADMIN_SECRET"
              }
            }
          }

          env {
            name = "POSTGRES_ENDPOINT"
            value_from {
              secret_key_ref {
                name = "postgres-secret"
                key  = "endpoint"
              }
            }
          }

          env {
            name  = "GRAPHQL_ENDPOINT"
            value = "http://graphql.application.svc.cluster.local:8080/v1/graphql"
          }

          env {
            name  = "ALERTMANAGER_ENDPOINT"
            value = "http://alertmanager.cortex.svc.cluster.local/api/v1/alerts"
          }

          env {
            name  = "RULER_ENDPOINT"
            value = "http://ruler.cortex.svc.cluster.local/api/v1/rules"
          }

          env {
            name  = "CLICKHOUSE_ENDPOINT"
            value = "http://cluster.clickhouse.svc.cluster.local:8123/"
          }

          readiness_probe {
            http_get {
              path   = "/ready"
              port   = "readiness"
              scheme = "HTTP"
            }
          }
        }
      }
    }
  }

  # Set the terminate field in the opstrace controller config to true. Wait for
  # the namespaces to be deleted signalling the opstrace controller finished the
  # clean up. A Kubernetes namespaces is deleted when all the resources it
  # contains are deleted.
  provisioner "local-exec" {
    when = destroy
    # TODO: Which KUBECONFIG is set? The kubernetes_provider one or the one set
    # in the user shell? TODO: requires sed and kubectl
    command = <<-EOT
      echo "Instructing controller to clean up resources" ;
      kubectl get cm opstrace-controller-config -o yaml | sed 's/"terminate":false/"terminate":true/g' | kubectl apply -f - ;
      echo "Waiting for namespaces to be deleted by the controller" ;
      kubectl wait ns/application ns/cortex-operator-system ns/clickhouse-operator-system ns/ingress ns/jaeger-operator-system ns/monitoring --for=delete --timeout=10m ;
    EOT
  }
}
