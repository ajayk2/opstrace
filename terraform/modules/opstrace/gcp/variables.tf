variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
}

variable "gke_username" {
  default     = ""
  description = "gke username"
}

variable "gke_password" {
  default     = ""
  description = "gke password"
}

variable "location" {
  default     = ""
  description = "The location (region or zone) in which the cluster master will be created, as well as the default node location. If you specify a zone (such as us-central1-a), the cluster will be a zonal cluster with a single cluster master. If you specify a region (such as us-west1), the cluster will be a regional cluster with multiple masters spread across zones in the region, and with default node locations in those zones as well"
}

variable "instance_name" {
  default     = "example"
  description = "the Opstrace instance name"
}

variable "num_nodes" {
  default     = 3
  description = "number of gke nodes"
}

variable "custom_dns_name" {
  default     = ""
  description = "where the Opstrace instance will be available"
}

variable "kubeconfig_path" {
  default     = ""
  description = "Path to kubeconfig"
}

variable "custom_auth0_client_id" {
  default     = ""
  description = ""
}

variable "custom_auth0_domain" {
  default     = ""
  description = ""
}

variable "cert_issuer" {
  default     = "letsencrypt-prod"
  description = "Defines the issuer to use for all TLS-terminating certificates, such as for the externally available data API endpoints."
}

variable "metric_retention_days" {
  default     = 7
  description = "Defines metrics data retention in number of days"
}

variable "trace_retention_days" {
  default     = 7
  description = "Defines trace data retention in number of days"
}

variable "controller_image" {
  default     = "opstrace/controller:eaf0490f-ci"
  description = "Defines the Docker image to run the controller from. Docker image reference, including the Docker hub user/org, the repository name, and the image tag."
}
