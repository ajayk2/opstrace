# data "template_file" "kubeconfig" {
#   template = file("${path.module}/kubeconfig-template.yaml")
#
#   vars = {
#     cluster_name    = google_container_cluster.primary.name
#     endpoint        = google_container_cluster.primary.endpoint
#     cluster_ca      = google_container_cluster.primary.master_auth[0].cluster_ca_certificate
#     client_cert     = google_container_cluster.primary.master_auth[0].client_certificate
#     client_cert_key = google_container_cluster.primary.master_auth[0].client_key
#   }
# }
#
# resource "local_file" "kubeconfig" {
#   content  = data.template_file.kubeconfig.rendered
#   filename = "${path.module}/kubeconfig"
# }
#
# output "kubeconfig_path" {
#   value = local_file.kubeconfig.filename
# }

# Create a kubeconfig to access the cluster in the module directory
resource "null_resource" "configure_kubectl" {
  provisioner "local-exec" {
    command = "gcloud beta container clusters get-credentials ${var.instance_name} --region ${var.location} --project ${var.project_id}"

    # Set KUBECONFIG env var so gcloud will update the file
    environment = {
      KUBECONFIG = var.kubeconfig_path
    }
  }

  # TODO: how can we improve this?
  triggers = {
    always_run = timestamp()
  }

  depends_on = [google_container_node_pool.primary_nodes]
}
