import * as yup from "yup";

import { LocalDateTime, DateTimeFormatter } from "@js-joda/core";

import { GCPConfig } from "@opstrace/gcp";
import { AWSConfig } from "@opstrace/aws";

/**
 * The yup date() schema uses a transformer to parse date strings,
 * like those retrieved from the controller-config's raw value
 * stored in a ConfigMap. This works when not running in strict mode,
 * which turns off transformers. When upgrading the config however,
 * strict validation must be turned on to make sure an old schema that
 * is a strict subset (same but fewer values) of of a new one doesn't
 * coerce to the new schema. Then validation fails. This custom schema
 * permits a string timestamp with ISO format enforced.
 */
const timestampSchema = yup
  .string()
  .test(
    "isISO",
    "must be RFC3339 w/o fractional seconds and UTC with Z tz specifier, e.g. 1990-12-31T23:59:59Z",
    (_s: string): boolean => {
      // expect "1990-12-31T23:59:59Z", i.e. RFC 3339
      // without the fractional second part
      // with the Z specifier
      try {
        LocalDateTime.parse(
          "1990-12-31T23:59:59Z",
          DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        );
      } catch (err: any) {
        // Don't import the complete winston-based logging machinery here, but
        // also don't let down the dev who might change something and then run
        // into a validation error: expose the actual validation error detail.
        process.stdout.write(`\nvalidation error:\n${err}\n`);
        return false;
      }
      return true;
    }
  );

const NodeSelectorTermsSchema = yup.object({
  default: yup.object({
    key: yup.string(),
    values: yup.array(yup.string())
  }),
  rules: yup.array(
    yup.object({
      //
      kind: yup
        .string()
        .oneOf(["deployment", "statefulset", "daemonset", "any"]),
      namespace: yup.string(),
      name: yup.string(),
      //
      match_expression: yup.object({
        key: yup.string(),
        values: yup.array(yup.string())
      })
    })
  )
});

export type NodeSelectorTermsType = yup.InferType<
  typeof NodeSelectorTermsSchema
>;

export const ControllerConfigSchemaV3 = yup
  .object({
    name: yup.string(),
    target: yup
      .mixed<"gcp" | "aws">()
      .oneOf(["gcp", "aws"])
      .required("must specify a target (gcp | aws)"),
    region: yup.string().required("must specify region"),
    metricRetentionDays: yup
      .number()
      .required("must specify metric retention in number of days"),
    traceRetentionDays: yup
      .number()
      .required("must specify trace retention in number of days"),
    dnsName: yup.string().required(),

    custom_dns_name: yup.string().notRequired(),
    custom_auth0_client_id: yup.string().notRequired(),
    custom_auth0_domain: yup.string().notRequired(),
    gitlab: yup
      .object({
        instance_url: yup.string().required(),
        group_allowed_access: yup.string().required(),
        group_allowed_system_access: yup.string().required(),
        oauth_client_id: yup.string().required(),
        oauth_client_secret: yup.string().required()
      })
      .required(),

    node_selector_terms: NodeSelectorTermsSchema.notRequired(),

    terminate: yup.bool().default(false),
    uiSourceIpFirewallRules: yup.array(yup.string()).ensure(),
    apiSourceIpFirewallRules: yup.array(yup.string()).ensure(),
    postgreSQLEndpoint: yup.string().notRequired(),
    opstraceDBName: yup.string().notRequired(),
    envLabel: yup.string(),
    // Note: remove one of cert_issuer and `tlsCertificateIssuer`.
    cert_issuer: yup
      .string()
      .oneOf(["letsencrypt-prod", "letsencrypt-staging"])
      .required(),
    tlsCertificateIssuer: yup
      .mixed<"letsencrypt-staging" | "letsencrypt-prod">()
      .oneOf(["letsencrypt-staging", "letsencrypt-prod"])
      .required(),
    infrastructureName: yup.string().required(),
    aws: yup.mixed<AWSConfig | undefined>(),
    gcp: yup.mixed<GCPConfig | undefined>(),
    controllerTerminated: yup.bool().default(false),
    cliMetadata: yup.object({
      allCLIVersions: yup
        .array(
          yup.object({
            version: yup.string(),
            timestamp: timestampSchema
          })
        )
        .ensure()
    })
  })
  .noUnknown()
  .defined();

export type ControllerConfigTypeV3 = yup.InferType<
  typeof ControllerConfigSchemaV3
>;
