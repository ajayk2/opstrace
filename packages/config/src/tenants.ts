import { Tenant } from "@opstrace/tenants";

const system: Tenant = {
  name: "system",
  type: "SYSTEM"
};
const defaultTenant: Tenant = {
  name: "default",
  type: "USER"
};

export const getTenantsConfig = (tenants?: string[]): Tenant[] => {
  if (tenants && tenants.length) {
    return [system, ...tenants.map<Tenant>(name => ({ name, type: "USER" }))];
  }
  return [system, defaultTenant];
};
