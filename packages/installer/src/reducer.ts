import { combineReducers } from "redux";

import { reducer as tenantReducer } from "@opstrace/tenants";
import { reducer as configReducer } from "@opstrace/controller-config";

import {
  statefulSetsReducer,
  secretsReducer,
  deploymentsReducer,
  daemonSetsReducer,
  configMapsReducer,
  V1CertificateReducer,
  V1CertificaterequestReducer
} from "@opstrace/kubernetes";

export const rootReducers = {
  tenants: tenantReducer,
  kubernetes: combineReducers({
    cluster: combineReducers({
      StatefulSets: statefulSetsReducer,
      Secrets: secretsReducer,
      Deployments: deploymentsReducer,
      DaemonSets: daemonSetsReducer,
      ConfigMaps: configMapsReducer,
      Config: configReducer,
      Certificates: V1CertificateReducer,
      CertificateRequests: V1CertificaterequestReducer
    })
  })
};

export const rootReducer = combineReducers(rootReducers);
export type State = ReturnType<typeof rootReducer>;
