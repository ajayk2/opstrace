import fs from "fs";
import { strict as assert } from "assert";
import { urlJoin } from "url-join-ts";

import got, { Response as GotResponse, Options as GotOptions } from "got";
import { ZonedDateTime, ZoneOffset, DateTimeFormatter } from "@js-joda/core";

import { fork, call, race, delay, cancel } from "redux-saga/effects";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import {
  log,
  sleep,
  SECOND,
  BUILD_INFO,
  retryUponAnyError,
  Dict,
  checkIfDockerImageExistsOrErrorOut
} from "@opstrace/utils";

import {
  getTenantsConfig,
  getFirewallConfig,
  getClusterConfig,
  getDnsConfig,
  LatestClusterConfigType
} from "@opstrace/config";

import {
  getKubeConfig,
  k8sListNamespacesOrError,
  createOrUpdateConfigMapWithRetry
} from "@opstrace/kubernetes";

import {
  getValidatedGCPAuthOptionsFromFile,
  GCPAuthOptions,
  getCertManagerServiceAccount,
  getExternalDNSServiceAccount,
  getCortexServiceAccount
} from "@opstrace/gcp";
import { set as updateTenantsConfig } from "@opstrace/tenants";
import {
  ControllerResourcesDeploymentStrategy,
  deployControllerResources,
  LatestControllerConfigType,
  LatestControllerConfigSchema,
  serializeControllerConfig
} from "@opstrace/controller-config";

import { rootReducer } from "./reducer";
import { ensureGCPInfraExists } from "./gcp";
import {
  ensureAWSInfraExists
  //waitUntilRoute53EntriesAreAvailable
} from "./aws";
import { ClusterCreateTimeoutError } from "./errors";
import { runInformers } from "./informers";
import {
  installationProgressReporter,
  waitForControllerDeployment
} from "./readiness";
import { EnsureInfraExistsResponse } from "./types";

// typescript barrel export: https://basarat.gitbook.io/typescript/main-1/barrel
export { EnsureInfraExistsResponse } from "./types";
export { ensureAWSInfraExists } from "./aws";
export { ensureGCPInfraExists } from "./gcp";

// GCP-specific cluster creation code can rely on this being set. First I tried
// to wrap this into the non-user-given cluster config schema but then realized
// that this is _part_ of credentials, and really just some detail parameter
// used at runtime that has little to do with "config": users provide svc acc
// credentials and these implicitly define the gcp project ID.
let gcpProjectID: string;
export function setGcpProjectID(p: string): void {
  gcpProjectID = p;
}
export { gcpProjectID };

// configuration for the cluster creation process which does _not_ belong
// semantically to the cluster config itself.
export interface ClusterCreateConfigInterface {
  // if true, will not deploy the controller. This is useful for running the controller locally instead during development.
  holdController: boolean;
  // if set, write a KUBECONFIG file to this path, asap after k8s cluster
  // has been provisioned.
  kubeconfigFilePath: string;
}

let clusterCreateConfig: ClusterCreateConfigInterface;
export function setCreateConfig(c: ClusterCreateConfigInterface): void {
  clusterCreateConfig = c;
}

// number of Opstrace cluster creation attempts
const CREATE_ATTEMPTS = 2;

// timeout per attempt
const CREATE_ATTEMPT_TIMEOUT_SECONDS = 60 * 40;

function* createClusterCore() {
  const ccfg: LatestClusterConfigType = getClusterConfig();
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const gcpCredFilePath: string =
    process.env["GOOGLE_APPLICATION_CREDENTIALS"]!;

  const firewallConf = getFirewallConfig({
    api: ccfg.data_api_authorized_ip_ranges
  });

  let gcpAuthOptions: GCPAuthOptions | undefined;

  // not sure why the controller needs to know about 'region', but here we go.
  let region: string;

  if (ccfg.cloud_provider === "gcp") {
    // note: legacy, tmp state, when we are in this routine the
    // GOOGLE_APPLICATION_CREDENTIALS env variable is set to an existing file,
    // and basic content validation has already been performed. details from
    // the file, such as project ID, will be set on global config object. this
    // is only here to keep following code working w/o change.
    gcpAuthOptions = getValidatedGCPAuthOptionsFromFile(gcpCredFilePath);

    if (ccfg.gcp === undefined) {
      throw Error("`gcp` property expected");
    }

    region = ccfg.gcp.region;
  } else {
    assert(ccfg.cloud_provider === "aws");

    if (ccfg.aws === undefined) {
      throw Error("`aws` property expected");
    }

    // set `region` (legacy code maintenance, clean up)
    region = ccfg.aws.region;
  }

  const dnsConf = getDnsConfig(ccfg.cloud_provider);

  let controllerConfig: LatestControllerConfigType = {
    name: ccfg.cluster_name,
    target: ccfg.cloud_provider,
    region: region, // not sure why that's needed
    cert_issuer: ccfg.cert_issuer,
    envLabel: ccfg.env_label,
    infrastructureName: ccfg.cluster_name,
    metricRetentionDays: ccfg.metric_retention_days,
    traceRetentionDays: ccfg.trace_retention_days,
    dnsName: dnsConf.dnsName, // note that this is meaningless(?) when `custom_dns_name` is set -- consolidate
    terminate: false,
    controllerTerminated: false,
    tlsCertificateIssuer: ccfg.cert_issuer,
    uiSourceIpFirewallRules: firewallConf.ui,
    apiSourceIpFirewallRules: firewallConf.api,
    custom_dns_name: ccfg.custom_dns_name,
    custom_auth0_client_id: ccfg.custom_auth0_client_id,
    custom_auth0_domain: ccfg.custom_auth0_domain,
    gitlab: {
      instance_url: ccfg.gitlab.instance_url,
      group_allowed_access: ccfg.gitlab.group_allowed_access,
      group_allowed_system_access: ccfg.gitlab.group_allowed_system_access,
      oauth_client_id: ccfg.gitlab.oauth_client_id,
      oauth_client_secret: ccfg.gitlab.oauth_client_secret
    },
    cliMetadata: {
      allCLIVersions: [
        {
          version: BUILD_INFO.VERSION_STRING,
          // Current time in UTC using RFC3339 string representation (w/o
          // fractional seconds, with Z tz specififer), e.g.
          // '2021-07-28T15:43:07Z'
          timestamp: ZonedDateTime.now(ZoneOffset.UTC).format(
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
          )
        }
      ]
    }
  };

  // Fail fast if specified controller docker image cannot be found on docker
  // hub, see https://github.com/opstrace/opstrace/issues/1298.
  if (!clusterCreateConfig.holdController) {
    yield call(checkIfDockerImageExistsOrErrorOut, ccfg.controller_image);
  }

  let kubeconfigString = "";
  let postgreSQLEndpoint = "";
  let opstraceDBName = "";

  if (ccfg.cloud_provider === "gcp") {
    if (!gcpAuthOptions) {
      throw Error("could not locate authentication credentials for gcp");
    }
    const res: EnsureInfraExistsResponse = yield call(
      ensureGCPInfraExists,
      gcpAuthOptions
    );
    kubeconfigString = res.kubeconfigString;
    postgreSQLEndpoint = res.postgreSQLEndpoint;
    opstraceDBName = res.opstraceDBName;

    controllerConfig.gcp = {
      projectId: gcpAuthOptions.projectId,
      certManagerServiceAccount: getCertManagerServiceAccount(),
      externalDNSServiceAccount: getExternalDNSServiceAccount(),
      cortexServiceAccount: getCortexServiceAccount()
    };
  }
  if (ccfg.cloud_provider === "aws") {
    const res: EnsureInfraExistsResponse = yield call(ensureAWSInfraExists);
    kubeconfigString = res.kubeconfigString;
    postgreSQLEndpoint = res.postgreSQLEndpoint;

    if (!res.certManagerRoleArn) {
      throw Error("must return the certManagerRoleArn from aws infra install");
    }

    controllerConfig.aws = {
      certManagerRoleArn: res.certManagerRoleArn
    };
  }

  // Update our controllerConfig with the Postgres Endpoint and revalidate for
  // good measure
  controllerConfig = {
    ...controllerConfig,
    postgreSQLEndpoint,
    opstraceDBName
  };
  LatestControllerConfigSchema.validateSync(controllerConfig, { strict: true });

  if (!kubeconfigString) {
    throw Error("couldn't compute a kubeconfig");
  }

  const kubeConfig = getKubeConfig({
    loadFromCluster: false,
    kubeconfig: kubeconfigString
  });

  // If asked for by the user, write out the kubeconfig to a file, so that they
  // can start interacting with the k8s cluster (misc integration).
  if (clusterCreateConfig.kubeconfigFilePath !== "") {
    const path = clusterCreateConfig.kubeconfigFilePath;
    log.info("try to write kubeconfig to file: %s", path);
    try {
      fs.writeFileSync(path, kubeconfigString, { encoding: "utf8" });
    } catch (err: any) {
      // This is not critical for cluster creation, just convenience. Log
      // how/why writing failed, otherwise proceed
      log.warning(
        "could not write kubeconfig to file %s: %s: %s",
        path,
        err.code,
        err.message
      );
    }
  }

  // Try to interact with the k8s API (for debugging, kept from legacy code)
  try {
    yield call(k8sListNamespacesOrError, kubeConfig);
  } catch (err: any) {
    log.warning(
      "problem when interacting with the k8s cluster (thrown by k8sListNamespacesOrError): %s",
      err
    );
  }

  const tenantsConfig = getTenantsConfig(ccfg.tenants);

  // Running `create` more than once can in special cases help, but is not
  // generally well-defined. Also see
  // https://github.com/opstrace/opstrace/issues/20. A small start towards
  // improvement: do not overwrite existing controller config.
  try {
    yield call(
      createOrUpdateConfigMapWithRetry,
      serializeControllerConfig(controllerConfig, kubeConfig),
      { forceCreate: true }
    );
  } catch (e: any) {
    if (e.response && e.response.statusCode === 409) {
      log.warning(
        "controller config map already exists, do not overwrite (is this a continuation of a partial previous create?)"
      );
    } else {
      throw e;
    }
  }

  // TODO: this also should not be updated if it exists.
  yield call(updateTenantsConfig, tenantsConfig, kubeConfig);

  if (clusterCreateConfig.holdController) {
    log.info(
      `Not deploying controller. Raw instance creation finished: ${ccfg.cluster_name} (${ccfg.cloud_provider})`
    );
    return;
  }

  // TODO: continuation mode: skip if controller deployment exists.
  log.info("deploying controller");
  yield call(deployControllerResources, {
    controllerImage: ccfg.controller_image,
    opstraceClusterName: ccfg.cluster_name,
    postgresEndpoint: urlJoin(
      controllerConfig.postgreSQLEndpoint,
      controllerConfig.opstraceDBName
    ),
    kubeConfig: kubeConfig,
    deploymentStrategy: ControllerResourcesDeploymentStrategy.Create
  });

  log.info("starting k8s informers");
  //@ts-ignore: TS7075 generator lacks return type (TS 4.3)
  const informers = yield fork(runInformers, kubeConfig);

  yield call(waitForControllerDeployment);

  yield call(installationProgressReporter);

  // `informers` is a so-called attached fork. Cancel this task.
  yield cancel(informers);

  // Is this needed with the custom DNS setup?
  // if (ccfg.cloud_provider == "aws") {
  //   yield call(
  //     waitUntilRoute53EntriesAreAvailable,
  //     ccfg.cluster_name,
  //     ccfg.tenants
  //   );
  // }

  const opstraceInstanceDNSname = ccfg.custom_dns_name;
  log.info(
    "expected DNS name for this Opstrace instance: %s",
    opstraceInstanceDNSname
  );

  yield call(waitUntilHTTPEndpointsAreReachable, ccfg);

  log.info(
    `create operation finished: ${ccfg.cluster_name} (${ccfg.cloud_provider})`
  );
  log.info(`Log in here: https://${opstraceInstanceDNSname}`);
}

export function instanceDNSNameFromClusterConfig(
  ccfg: LatestClusterConfigType
): string {
  let opstraceInstanceDNSname = `${ccfg.cluster_name}.opstrace.io`;
  if (ccfg.custom_dns_name !== undefined) {
    opstraceInstanceDNSname = ccfg.custom_dns_name;
  }

  return opstraceInstanceDNSname;
}

/**
 * Confirm DNS-reachability, and also readiness of deployments.
 * K8s-internal readiness probes aren't always enough for this.
 *
 * Note that this does not check that authenticated access works, as
 * that requires a full OAuth handshake, or port-forwarding to the
 * tenant Grafana instance and specifying x-auth-request-email to
 * generate an auth token for the check. The latter option is
 * technically possible but will create weird stub accounts/keys
 * in the UI that might be surprising or offputting to the user.
 *
 * So to keep things simple, we just make a best-effort check that
 * everything is reachable on the surface.
 */
export async function waitUntilHTTPEndpointsAreReachable(
  ccfg: LatestClusterConfigType
): Promise<void> {
  const opstraceInstanceDNSname = instanceDNSNameFromClusterConfig(ccfg);
  const tnames = [...ccfg.tenants];
  tnames.push("system");

  const urlsToExpectedCodes: Dict<number> = {};
  for (const tname of tnames) {
    if (tname == "system") {
      // Not deployed for system tenant
      continue;
    }
    // Just check that we get a 401 error from the ingress.
    // Getting a real token requires completing UI OAuth login flow.
    urlsToExpectedCodes[
      `https://dd.${tname}.${opstraceInstanceDNSname}/api/v1/series`
    ] = 401;
  }
  for (const tname of tnames) {
    // Just check that we get a 401 error from the ingress.
    // Getting a real token requires completing UI OAuth login flow.
    urlsToExpectedCodes[
      `https://cortex.${tname}.${opstraceInstanceDNSname}/api/v1/labels`
    ] = 401;
  }
  for (const tname of tnames) {
    // This effectively goes to a UI login page via redirects
    urlsToExpectedCodes[`https://${tname}.${opstraceInstanceDNSname}/`] = 200;
  }

  log.info(
    "waiting for expected HTTP response codes at these URLs: %s",
    JSON.stringify(urlsToExpectedCodes, null, 2)
  );
  const actors: Array<Promise<void>> = [];
  for (const [probeUrl, expectedRespCode] of Object.entries(
    urlsToExpectedCodes
  )) {
    // Just wait for a 401 from the ingress.
    // Getting a valid auth token requires being authed against the UI.
    actors.push(waitForProbeURL(probeUrl, expectedRespCode));
  }
  await Promise.all(actors);
  log.info(
    "wait for endpoints: all probe URLs returned expected HTTP responses, continue"
  );
}

async function waitForProbeURL(probeUrl: string, expectedRespCode: number) {
  const requestSettings: GotOptions = {
    throwHttpErrors: false,
    retry: 0,
    https: { rejectUnauthorized: false },
    timeout: {
      connect: 3000,
      request: 10000
    }
  };

  let attempt = 0;
  while (true) {
    attempt++;

    let resp: GotResponse<string>;
    try {
      //@ts-ignore `got(probeUrl, rs)` returns `unknown` from tsc's point of view
      resp = await got(probeUrl, requestSettings);
    } catch (e: any) {
      if (e instanceof got.RequestError) {
        // Assume that for most of the 'waiting time' the probe fails in this
        // error handler.

        // When the debug log level is active then I think it's the right
        // thing to log every negative probe outcome as it happens (e.g. DNS
        // resolution error or TCP connection timeout).
        log.debug(`${probeUrl}: HTTP request failed with: ${e.message}`);

        // But on info level just emit the fact that the expected outcome is
        // still being waited for, every now and then (maybe every ~20
        // seconds).
        if (attempt % 5 === 0) {
          log.info(
            `${probeUrl}: still waiting for expected signal. Last error: ${e.message}`
          );
        }

        await sleep(5.0);
        continue;
      } else {
        throw e;
      }
    }

    if (resp.statusCode == expectedRespCode) {
      return;
    }

    log.debug(`HTTP response details:
status: ${resp.statusCode}
body[:500]: ${resp.body.slice(0, 500)}`);

    if (attempt % 2 === 0) {
      log.info(`${probeUrl}: still waiting, unexpected HTTP response`);
    }

    await sleep(5.0);
  }
}

/**
 * Timeout control around a single cluster creation attempt.
 */
function* createClusterAttemptWithTimeout() {
  log.debug("createClusterAttemptWithTimeout");
  const { timeout } = yield race({
    create: call(createClusterCore),
    timeout: delay(CREATE_ATTEMPT_TIMEOUT_SECONDS * SECOND)
  });

  if (timeout) {
    // Note that in this case redux-saga guarantees to have cancelled the
    // task(s) that lost the race, i.e. the `create` task above.
    log.warning(
      "cluster creation attempt timed out after %s seconds",
      CREATE_ATTEMPT_TIMEOUT_SECONDS
    );
    throw new ClusterCreateTimeoutError();
  }
}

function* rootTaskCreate() {
  yield call(retryUponAnyError, {
    task: createClusterAttemptWithTimeout,
    maxAttempts: CREATE_ATTEMPTS,
    doNotLogDetailForTheseErrors: [ClusterCreateTimeoutError],
    actionName: "cluster creation",
    delaySeconds: 10
  });
}

/**
 * Entry point for cluster creation, to be called by CLI.
 */
export async function createCluster(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  smOnError: (e: Error, detail: any) => void
): Promise<void> {
  const sm = createSagaMiddleware({ onError: smOnError });

  createStore(rootReducer, applyMiddleware(sm));
  await sm.run(rootTaskCreate).toPromise();

  // this is helpful when the runtime is supposed to crash but doesn't
  log.debug("end of createCluster()");
}
