import { EditorOptions } from "../lib/types";

export function getRange(
  model: monaco.editor.ITextModel,
  start: number,
  end: number
): monaco.IRange {
  const p1 = model.getPositionAt(start);
  const p2 = model.getPositionAt(end);
  const { lineNumber: startLineNumber, column: startColumn } = p1;
  const { lineNumber: endLineNumber, column: endColumn } = p2;

  return { startLineNumber, startColumn, endLineNumber, endColumn };
}

export function getTextEditorOptions({
  readOnly,
  model
}: {
  readOnly: boolean;
  model: monaco.editor.ITextModel;
}): EditorOptions {
  return {
    readOnly,
    model,
    cursorStyle: "line",
    automaticLayout: false,
    minimap: {
      enabled: false
    },
    formatOnType: true,
    fontSize: 13,
    roundedSelection: false,
    renderLineHighlight: "all",
    scrollbar: {
      useShadows: true,
      verticalScrollbarSize: 10,
      horizontalScrollbarSize: 10
    }
  };
}
