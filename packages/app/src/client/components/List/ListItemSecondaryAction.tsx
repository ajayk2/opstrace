import React from "react";

import MuiListItemSecondaryAction, {
  ListItemSecondaryActionProps
} from "@material-ui/core/ListItemSecondaryAction";

export const ListItemSecondaryAction = (
  props: ListItemSecondaryActionProps
) => <MuiListItemSecondaryAction {...props} />;
