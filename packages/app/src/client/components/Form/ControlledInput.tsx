import React from "react";
import { Control, Controller, Path } from "react-hook-form";
import { HelpCircle } from "react-feather";

import {
  FormLabel,
  FormHelperText,
  TextField,
  TextFieldProps
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { Typography } from "client/components/Typography";
import { Box } from "client/components/Box";

type ControlledInputProps<ControlValues> = {
  name: Path<ControlValues>;
  label?: string;
  helperText?: string | React.ReactNode | (() => React.ReactNode);
  inputProps?: TextFieldProps;
  control: Control<ControlValues>;
  labelClass?: string;
  controlClass?: string;
};

const useStyles = makeStyles(theme => ({
  helperText: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap"
  },
  helperIcon: {
    marginRight: 5
  }
}));

export const ControlledInput = <ControlValues,>({
  name,
  label,
  inputProps = {},
  helperText,
  control,
  labelClass,
  controlClass
}: ControlledInputProps<ControlValues>) => {
  const classes = useStyles();

  return (
    <Controller
      render={({ field }) => (
        <Box>
          {label !== undefined && (
            <div className={labelClass}>
              <FormLabel>
                <Typography variant="h6" color="textSecondary">
                  {label}
                </Typography>
              </FormLabel>
            </div>
          )}
          <div className={controlClass}>
            <TextField {...field} {...inputProps} variant="outlined" />
            {helperText !== undefined && (
              <FormHelperText>
                <Typography variant="caption" className={classes.helperText}>
                  <HelpCircle
                    width={12}
                    height={12}
                    className={classes.helperIcon}
                  />
                  <span>{helperText}</span>
                </Typography>
              </FormHelperText>
            )}
          </div>
        </Box>
      )}
      control={control}
      name={name}
    />
  );
};
