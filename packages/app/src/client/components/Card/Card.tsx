import React from "react";

import MuiCard, { CardProps as MuiCardProps } from "@material-ui/core/Card";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  }
}));

const BaseCard = (props: MuiCardProps) => {
  const classes = useStyles();
  return <MuiCard variant="outlined" className={classes.root} {...props} />;
};

export default BaseCard;
