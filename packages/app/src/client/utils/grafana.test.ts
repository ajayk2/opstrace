import nock from "nock";
import { Integration } from "state/integration/types";
import { Tenant } from "state/tenant/types";
import { deleteFolder } from "./grafana";

const getMockIntegration = (): Integration => ({
  id: 1,
  tenant_id: 2,
  name: "mock-integration",
  key: "some-key",
  kind: "some-kind",
  data: {},
  created_at: "today",
  updated_at: "tomorrow"
});

const getMockTenant = (): Tenant => ({
  id: 4,
  name: "mock-tenant",
  key: "some-key",
  type: "some-type",
  created_at: "today",
  updated_at: "tomorrow"
});

beforeEach(() => {
  nock.cleanAll();
});

describe("deleteFolder", () => {
  it("sends correct request", async () => {
    const integration = getMockIntegration();
    const tenant = getMockTenant();
    const folderId = 99;

    nock(`http://${tenant.name}.localhost`)
      .delete(`/grafana/api/folders/i9n-${integration.id}`)
      .reply(200, { id: folderId });

    const result = await deleteFolder({
      integration,
      tenant
    });

    expect(result).toEqual({ id: folderId });
  });
});
