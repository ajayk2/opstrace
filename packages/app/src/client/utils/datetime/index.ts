import formatFn from "date-fns/format";
import formatISO from "date-fns/formatISO";
import { getUnixTime } from "date-fns";

const getLocale = (locale: string): Promise<Locale> =>
  import(`date-fns/locale/${locale}/index.js`);

/**
 * Wraps and lazy loads the locale for date-fns/format
 * @param date
 * @param formatStyle
 * @param locale
 */
export const format = async (
  date: number | Date,
  formatStyle: string,
  locale: string = "en-US"
) => {
  const asyncLocale = await getLocale(locale);
  return formatFn(date, formatStyle, {
    locale: asyncLocale
  });
};

export function isoNow() {
  return formatISO(new Date());
}

export const getUnixNanoSecTime = (date: Date) =>
  getUnixTime(date) * 1000000000;
