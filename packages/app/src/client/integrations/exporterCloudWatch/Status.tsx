import React from "react";

import ExporterStatus from "client/integrations/common/ExporterStatus";
import { StatusProps } from "../types";

export const ExporterCloudWatchStatus = (props: StatusProps) => (
  <ExporterStatus
    {...props}
    errorFilter={`|= "Exception:"`}
    activeFilter={`|= "INFO: Started @"`}
  />
);

export default ExporterCloudWatchStatus;
