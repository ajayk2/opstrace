// For each dashboard we want, we import it here and list it below
import makeSummaryDashboard from "./summary";

type DashboardProps = {
  integrationId: string;
  folderId: number;
};

// Returns an array of Promtail/logs dashboard creation request payloads for submitting to Grafana.
export function makePromtailDashboardRequests({
  integrationId,
  folderId
}: DashboardProps) {
  return [
    {
      uid: `sum-${integrationId}`,
      dashboard: makeSummaryDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    }
  ];
}
