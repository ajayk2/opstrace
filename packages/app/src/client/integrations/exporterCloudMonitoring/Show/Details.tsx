import React from "react";

import { Integration } from "state/integration/types";

import { Box } from "client/components/Box";
import Attribute from "client/components/Attribute";
import { Card, CardContent } from "client/components/Card";
import { Typography } from "client/components/Typography";
import { ExternalLink } from "client/components/Link";

type Props = {
  integration: Integration;
};

export const Details = ({ integration }: Props) => {
  const config = integration.data.config;
  return (
    <Box width="100%" height="100%" p={1}>
      <Card>
        <CardContent>
          <Box mb={3}>
            <Typography variant="h5">Configuration Flags</Typography>
            <Typography
              variant="subtitle2"
              color="textSecondary"
              gutterBottom={true}
            >
              See the{" "}
              <ExternalLink
                target="_blank"
                href="https://github.com/prometheus-community/stackdriver_exporter#flags"
              >
                documentation
              </ExternalLink>{" "}
              for further details
            </Typography>
          </Box>
          <Box display="flex">
            <Box display="flex" flexDirection="column">
              <Attribute.Key>google.project-id</Attribute.Key>
              <Attribute.Key>monitoring.metrics-type-prefixes</Attribute.Key>
              <Attribute.Key>monitoring.metrics-interval</Attribute.Key>
              <Attribute.Key>monitoringMetricsOffset</Attribute.Key>
            </Box>
            <Box display="flex" flexDirection="column" flexGrow={1}>
              <Attribute.Value>
                {config["google.project-id"].join(",")}
              </Attribute.Value>
              <Attribute.Value>
                {config["monitoring.metrics-type-prefixes"].join(",")}
              </Attribute.Value>
              <Attribute.Value>
                {config["monitoring.metrics-interval"]}
              </Attribute.Value>
              <Attribute.Value>
                {config["monitoring.metrics-offset"]}
              </Attribute.Value>
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
};
