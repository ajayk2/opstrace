import React from "react";

import { Tenant } from "state/tenant/types";
import { Integration } from "state/integration/types";

import { UninstallBtn } from "client/integrations/common/UninstallIntegrationBtn";

import { Box } from "client/components/Box";
import { Card, CardContent, CardHeader } from "client/components/Card";

type Props = {
  tenant: Tenant;
  integration: Integration;
};

export const Actions = ({ integration, tenant }: Props) => {
  return (
    <Box width="100%" height="100%" p={1}>
      <Card>
        <CardHeader titleTypographyProps={{ variant: "h5" }} title="Actions" />
        <CardContent>
          <Box flexGrow={1} pb={2}>
            <UninstallBtn
              integration={integration}
              tenant={tenant}
              disabled={false}
            />
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
};
