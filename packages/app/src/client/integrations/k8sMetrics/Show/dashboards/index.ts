// For each dashboard we want, we import it here and list it below
import makeApiserverDashboard from "./apiserver";
import makeKubeletDashboard from "./kubelet";
import makeResourceDashboard from "./resource";

type DashboardProps = {
  integrationId: string;
  folderId: number;
};
// Returns an array of Prometheus/metrics dashboard creation request payloads for submitting to Grafana.
export function makePrometheusDashboardRequests({
  integrationId,
  folderId
}: DashboardProps) {
  return [
    {
      uid: `api-${integrationId}`,
      dashboard: makeApiserverDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `kub-${integrationId}`,
      dashboard: makeKubeletDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `res-${integrationId}`,
      dashboard: makeResourceDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    }
  ];
}
