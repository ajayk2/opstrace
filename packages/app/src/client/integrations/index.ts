import { pluck, zipObj } from "ramda";

import { cockroachMetricsIntegration } from "./cockroachMetrics";
import { k8sLogsIntegration } from "./k8sLogs";
import { k8sMetricsIntegration } from "./k8sMetrics";
import { exporterCloudWatchIntegration } from "./exporterCloudWatch";
import { exporterCloudMonitoringIntegration } from "./exporterCloudMonitoring";
import { exporterAzureIntegration } from "./exporterAzure";
import { exporterBlackboxIntegration } from "./exporterBlackbox";

import { IntegrationDefs, IntegrationDefRecords } from "./types";

export * from "./types";
export * from "./paths";

export const integrationsDefs: IntegrationDefs = [
  cockroachMetricsIntegration,
  k8sMetricsIntegration,
  k8sLogsIntegration,
  exporterCloudWatchIntegration,
  exporterCloudMonitoringIntegration,
  exporterAzureIntegration,
  exporterBlackboxIntegration
];

export const integrationDefRecords: IntegrationDefRecords = zipObj(
  pluck("kind", integrationsDefs),
  integrationsDefs
);

export default integrationsDefs;
