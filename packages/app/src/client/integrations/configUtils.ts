import { Tenant } from "state/tenant/types";
import { Integration } from "state/integration/types";

export const getConfigFileName = (tenant: Tenant, integration: Integration) =>
  `opstrace-${tenant.name}-integration-${integration.kind}.yaml`;
