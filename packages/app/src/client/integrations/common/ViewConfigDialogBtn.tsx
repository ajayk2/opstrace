import React, { useEffect } from "react";

import { ViewConfig } from "client/integrations/common/ViewConfig";

import { Button } from "client/components/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export const ViewConfigDialogBtn = ({
  filename,
  config
}: {
  filename: string;
  config: string;
}) => {
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const descriptionElementRef = React.useRef<HTMLElement>(null);

  useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);

  return (
    <>
      <Button variant="outlined" size="small" onClick={handleOpen}>
        View YAML
      </Button>
      <Dialog open={open} onClose={handleClose} scroll="paper" maxWidth="lg">
        <DialogTitle>{filename}</DialogTitle>
        <DialogContent dividers={true}>
          <DialogContentText ref={descriptionElementRef} tabIndex={-1}>
            <ViewConfig
              filename={filename}
              config={config}
              heightBuffer={100}
            />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
