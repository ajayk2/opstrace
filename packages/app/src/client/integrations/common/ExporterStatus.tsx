import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Warning from "@material-ui/icons/Warning";
import green from "@material-ui/core/colors/green";
import orange from "@material-ui/core/colors/orange";
import red from "@material-ui/core/colors/red";

import { Integration } from "state/integration/types";
import { Tenant } from "state/tenant/types";

const useStyles = makeStyles(theme => ({
  integrationRow: {
    cursor: "pointer"
  },
  statusCell: {
    display: "flex",
    alignItems: "center",
    flexWrap: "wrap"
  },
  statusActive: { color: green["500"] },
  statusText: {
    marginRight: 10
  },
  statusPending: {
    color: orange["500"]
  },
  statusError: {
    color: red["500"]
  }
}));

type Props = {
  integration: Integration;
  tenant: Tenant;
  errorFilter: string;
  activeFilter: string;
};

export default function ExporterStatus(_: Props) {
  const classes = useStyles();

  return (
    <div className={classes.statusCell}>
      <span className={classes.statusText}>Unknown</span>
      <Warning className={classes.statusPending} />
    </div>
  );
}
