// For each dashboard we want, we import it here and list it below
import makeChangefeedsDashboard from "./changefeeds";
import makeDistributedDashboard from "./distributed";
import makeHardwareDashboard from "./hardware";
import makeOverviewDashboard from "./overview";
import makeQueuesDashboard from "./queues";
import makeReplicationDashboard from "./replication";
import makeRuntimeDashboard from "./runtime";
import makeSlowRequestDashboard from "./slow_request";
import makeSqlDashboard from "./sql";
import makeStorageDashboard from "./storage";

type DashboardProps = {
  integrationId: string;
  folderId: number;
};
// Returns an array of Prometheus/metrics dashboard creation request payloads for submitting to Grafana.
export function makePrometheusDashboardRequests({
  integrationId,
  folderId
}: DashboardProps) {
  return [
    {
      uid: `cha-${integrationId}`,
      dashboard: makeChangefeedsDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `dis-${integrationId}`,
      dashboard: makeDistributedDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `har-${integrationId}`,
      dashboard: makeHardwareDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `ove-${integrationId}`,
      dashboard: makeOverviewDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `que-${integrationId}`,
      dashboard: makeQueuesDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `rep-${integrationId}`,
      dashboard: makeReplicationDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `run-${integrationId}`,
      dashboard: makeRuntimeDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `slo-${integrationId}`,
      dashboard: makeSlowRequestDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `sql-${integrationId}`,
      dashboard: makeSqlDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    },
    {
      uid: `sto-${integrationId}`,
      dashboard: makeStorageDashboard(integrationId),
      folderId: folderId,
      overwrite: true
    }
  ];
}
