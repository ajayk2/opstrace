import { CockroachMetricsForm } from "./Form";
import { CockroachMetricsShow } from "./Show";
import CockroachMetricsStatus from "./Status";
import CockroachMetricsLogo from "./Logo.png";

import { IntegrationDef } from "client/integrations/types";

export const cockroachMetricsIntegration: IntegrationDef = {
  kind: "cockroach-metrics",
  category: "infrastructure",
  label: "CockroachDB Metrics",
  desc:
    "Run a grafana-agent metrics exporter to send metrics from your CockroachDB cluster to this tenant. We'll install bundled dashboards for monitoring CockroachDB with this integration.",
  Form: CockroachMetricsForm,
  Show: CockroachMetricsShow,
  Status: CockroachMetricsStatus,
  enabled: true,
  Logo: CockroachMetricsLogo
};
