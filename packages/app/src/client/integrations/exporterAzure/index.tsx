import Form from "./Form";
import Show from "./Show";
import Status from "./Status";
import Logo from "./Logo.png";

import { IntegrationDef } from "client/integrations/types";

export const exporterAzureIntegration: IntegrationDef = {
  kind: "exporter-azure",
  category: "exporter",
  label: "Microsoft Azure",
  desc:
    "Pipe any of your metrics from Microsoft Azure into Opstrace. You can select metrics from any of the Microsoft Azure Services such as Service Fabric or Load Balancer, as long as you've enabled monitoring on the service in the Microsoft Azure Console.",
  Form: Form,
  Show: Show,
  Status: Status,
  Logo: Logo,
  enabled: true
};
