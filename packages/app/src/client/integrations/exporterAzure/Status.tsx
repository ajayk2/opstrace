import React from "react";

import ExporterStatus from "client/integrations/common/ExporterStatus";
import { StatusProps } from "../types";

export const ExporterAzureStatus = (props: StatusProps) => (
  <ExporterStatus
    {...props}
    errorFilter={`!= "listening on port :9276"`}
    activeFilter={`|= "listening on port :9276"`}
  />
);

export default ExporterAzureStatus;
