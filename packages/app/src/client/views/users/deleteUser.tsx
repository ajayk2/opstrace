import React from "react";
import useCurrentUser from "state/user/hooks/useCurrentUser";

import { Button } from "client/components/Button";
import { User } from "state/user/types";
import useUserConfirmDeletionPicker from "./useUserConfirmDeletionPicker";

const DeleteUserButton = ({ user }: { user: User }) => {
  const currentUser = useCurrentUser();

  const { activatePickerWithText } = useUserConfirmDeletionPicker(user);

  return (
    <Button
      variant="outlined"
      state="error"
      size="small"
      disabled={currentUser.email === user.email}
      onClick={e => {
        e.stopPropagation();
        activatePickerWithText(`delete user?: `);
      }}
    >
      Delete
    </Button>
  );
};

export default DeleteUserButton;
