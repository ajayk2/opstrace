import React from "react";
import "@testing-library/jest-dom";
import { screen } from "@testing-library/react";
import AddUserDialog, { addUserCommandId } from "./AddUserDialog";
import {
  CommandServiceTrigger,
  renderWithEnv,
  userEvent
} from "client/utils/testutils";
import { createMainStore } from "state/store";
import { graphql } from "msw";
import { setupServer } from "msw/node";
import { User } from "state/graphql-api-types";
import { setUserList } from "state/user/actions";
import faker from "faker";

const createMockUser = (userConfig: Partial<User> = {}): User => {
  const email = faker.internet.email();
  return {
    id: faker.datatype.uuid(),
    email: email,
    username: email,
    role: "user_admin",
    active: true,
    avatar: "",
    created_at: "2021-08-25T14:28:16.714233+00:00",
    session_last_updated: null,
    ...userConfig
  };
};

const mockUserCreationEndpoint = (user: User) => {
  mockServer.use(
    graphql.mutation("CreateUser", (req, res, ctx) => {
      expect(req.body!.variables.email).toBe(user.email);
      return res(
        ctx.data({
          data: {
            update_user_by_pk: { user }
          }
        })
      );
    })
  );
};

const mockUserRecreationEndpoint = (user: User) => {
  mockServer.use(
    graphql.mutation("ReactivateUser", (req, res, ctx) => {
      const { id, active } = user;
      expect(req.body!.variables.id).toBe(id);
      return res(
        ctx.data({
          data: {
            insert_user_preference_one: { id, active }
          }
        })
      );
    })
  );
};

const mockServer = setupServer();

beforeAll(() => mockServer.listen());

beforeEach(() => {
  mockServer.resetHandlers();
});

afterAll(() => mockServer.close());

test("adds new user", async () => {
  const store = createMainStore();
  const mockUser = createMockUser();

  mockUserCreationEndpoint(mockUser);

  renderWithEnv(
    <CommandServiceTrigger commandId={addUserCommandId}>
      <AddUserDialog />
    </CommandServiceTrigger>,
    { store }
  );
  expect(await screen.findByText("Enter user's email")).toBeInTheDocument();
  const input = screen.getByRole("textbox", { name: "picker filter" });
  userEvent.type(input, mockUser.email + "{enter}");
});

test("reactivates users", async () => {
  const store = createMainStore();
  const mockUser = createMockUser({ active: false });

  store.dispatch(setUserList([mockUser]));

  mockUserRecreationEndpoint(mockUser);

  renderWithEnv(
    <CommandServiceTrigger commandId={addUserCommandId}>
      <AddUserDialog />
    </CommandServiceTrigger>,
    { store }
  );
  expect(await screen.findByText("Enter user's email")).toBeInTheDocument();
  const input = screen.getByRole("textbox", { name: "picker filter" });
  userEvent.type(input, mockUser.email + "{enter}");
});

test("handles when no name is entered", async () => {
  const username = "";
  renderWithEnv(
    <CommandServiceTrigger commandId={addUserCommandId}>
      <AddUserDialog />
    </CommandServiceTrigger>
  );
  expect(await screen.findByText("Enter user's email")).toBeInTheDocument();
  const input = screen.getByRole("textbox", { name: "picker filter" });
  userEvent.type(input, username + "{enter}");

  expect(screen.getByText("Enter new user's email")).toBeInTheDocument();
});

test("handles when name is no email", async () => {
  const username = "not an email";
  renderWithEnv(
    <CommandServiceTrigger commandId={addUserCommandId}>
      <AddUserDialog />
    </CommandServiceTrigger>
  );
  expect(await screen.findByText("Enter user's email")).toBeInTheDocument();
  const input = screen.getByRole("textbox", { name: "picker filter" });
  userEvent.type(input, username + "{enter}");

  expect(
    screen.getByText("It must be a valid email address")
  ).toBeInTheDocument();
});

test("handles user creation error", async () => {
  const store = createMainStore();
  const mockUser = createMockUser();
  const errorMessage = "Oh my - what an error!";

  mockServer.use(
    graphql.mutation("CreateUser", (req, res, ctx) => {
      return res(
        ctx.errors([
          {
            message: errorMessage
          }
        ])
      );
    })
  );

  renderWithEnv(
    <CommandServiceTrigger commandId={addUserCommandId}>
      <AddUserDialog />
    </CommandServiceTrigger>,
    { store }
  );
  expect(await screen.findByText("Enter user's email")).toBeInTheDocument();
  const input = screen.getByRole("textbox", { name: "picker filter" });
  userEvent.type(input, mockUser.email + "{enter}");

  expect(await screen.findByText("Could not add user")).toBeInTheDocument();
  expect(await screen.findByText(errorMessage)).toBeInTheDocument();
});

test("handles user reactivation error", async () => {
  const store = createMainStore();
  const mockUser = createMockUser({ active: false });
  const errorMessage = "Oh my - what an error!";

  store.dispatch(setUserList([mockUser]));

  mockServer.use(
    graphql.mutation("ReactivateUser", (req, res, ctx) => {
      return res(
        ctx.errors([
          {
            message: errorMessage
          }
        ])
      );
    })
  );

  renderWithEnv(
    <CommandServiceTrigger commandId={addUserCommandId}>
      <AddUserDialog />
    </CommandServiceTrigger>,
    { store }
  );
  expect(await screen.findByText("Enter user's email")).toBeInTheDocument();
  const input = screen.getByRole("textbox", { name: "picker filter" });
  userEvent.type(input, mockUser.email + "{enter}");

  expect(await screen.findByText("Could not add user")).toBeInTheDocument();
  expect(await screen.findByText(errorMessage)).toBeInTheDocument();
});
