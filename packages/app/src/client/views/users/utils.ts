import { map } from "ramda";

import { User, Users } from "state/user/types";
import { PanelItem } from "client/components/Panel";

export const userToItem = (user: User): PanelItem => {
  return { id: user.username, text: user.username, data: user };
};

export const usersToItems: (users: Users) => PanelItem[] = map(userToItem);
