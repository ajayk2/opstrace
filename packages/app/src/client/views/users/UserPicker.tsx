import React from "react";
import { useHistory } from "react-router-dom";

import { useCommandService } from "client/services/Command";

import { User } from "state/user/types";

import useUserList from "state/user/hooks/useUserList";
import { PickerOption, usePickerService } from "client/services/Picker";

export const userToPickerOption = (user: User): PickerOption<User> => ({
  text: user.email,
  id: user.id,
  data: user
});

const UserPicker = () => {
  const history = useHistory();
  const users = useUserList();

  const { activatePickerWithText } = usePickerService(
    {
      activationPrefix: "user:",
      options: users ? users.map(userToPickerOption) : [],
      onSelected: option => {
        history.push(`/cluster/users/${option.id}`);
      }
    },
    [users, history]
  );

  useCommandService({
    id: "select-user-picker",
    description: "Select User",
    handler: e => {
      e.keyboardEvent?.preventDefault();
      activatePickerWithText("user: ");
    }
  });

  return null;
};

export default React.memo(UserPicker);
