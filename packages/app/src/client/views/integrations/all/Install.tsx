import React from "react";
import { useDispatch } from "react-redux";
import { useParams, useHistory } from "react-router-dom";

import {
  integrationDefRecords,
  NewIntegration,
  NewIntegrationOptions,
  showIntegrationPath
} from "client/integrations";

import {
  addIntegration,
  loadGrafanaStateForIntegration
} from "state/integration/actions";
import graphqlClient, {
  getGraphQLClientErrorMessage,
  isGraphQLClientError
} from "state/clients/graphqlClient";

import { createFolder, isGrafanaError } from "client/utils/grafana";

import NotFound from "client/views/404/404";
import { useSelectedTenantWithFallback } from "state/tenant/hooks/useTenant";
import { useSimpleNotification } from "client/services/Notification";

export const InstallIntegration = () => {
  const { integrationKind: kind } = useParams<{
    integrationKind: string;
  }>();
  const dispatch = useDispatch();
  const history = useHistory();
  const tenant = useSelectedTenantWithFallback();
  const { registerNotification } = useSimpleNotification();

  const integration = integrationDefRecords[kind];
  if (!integration) return <NotFound />;

  const onCreate = async <IntegrationData,>(
    data: NewIntegration<IntegrationData>,
    options?: NewIntegrationOptions
  ) => {
    let response;
    try {
      response = await graphqlClient.InsertIntegration({
        name: data.name,
        kind: kind,
        data: data.data || {},
        tenant_id: tenant.id
      });
    } catch (error: any) {
      registerNotification({
        state: "error" as const,
        title: "Could not install integration",
        information: isGraphQLClientError(error)
          ? getGraphQLClientErrorMessage(error)
          : error.message
      });
      return;
    }
    const integration = response.data?.insert_integration_one;
    if (integration) {
      dispatch(addIntegration({ integration }));

      if (options?.createGrafanaFolder) {
        try {
          await createFolder({ integration, tenant });
        } catch (error: any) {
          registerNotification({
            state: "error" as const,
            title: "Could not create grafana folder",
            information: isGrafanaError(error)
              ? error.response.data.message
              : error.message
          });
          return;
        }
        dispatch(loadGrafanaStateForIntegration({ id: integration.id }));
      }

      history.push(
        showIntegrationPath({
          tenant,
          integration: integration
        })
      );
    }
  };

  return <integration.Form handleCreate={onCreate} />;
};
