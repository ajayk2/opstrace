import React from "react";

import { integrationDefRecords } from "client/integrations";

import NotFound from "client/views/404/404";
import { useSelectedIntegration } from "state/integration/hooks";

export const ShowIntegration = () => {
  const integration = useSelectedIntegration();

  if (!integration) {
    return null;
  }

  const integrationDef = integrationDefRecords[integration.kind];

  if (!integrationDef) return <NotFound />;

  return <integrationDef.Show />;
};
