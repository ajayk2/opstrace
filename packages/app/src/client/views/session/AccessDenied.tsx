import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

import { loginUrl } from "client/components/withSession/paths";

import { Page } from "client/components/Page";
import { Box } from "client/components/Box";
import { Button } from "client/components/Button";
import { Typography } from "client/components/Typography";
import { ErrorView } from "client/components/Error";

export const AccessDeniedPage = () => {
  const { user, logout } = useAuth0();

  const title = user ? `Access denied for ${user.email}.` : "Access denied.";
  const message = user
    ? `Contact your administrator or log out and try again with a different account.`
    : "Contact your administrator or try again with a different account.";

  return (
    <Page centered height="100vh" width="100vw">
      <ErrorView
        title="Unauthorized"
        subheader=""
        actions={null}
        emoji="💩"
        maxWidth={400}
      >
        <Typography>{title}</Typography>
        <br />
        <br />
        <Typography>{message}</Typography>
        <Box mt={3} pb={0}>
          <Button
            variant="contained"
            state="primary"
            size="large"
            onClick={() =>
              logout({
                returnTo: loginUrl()
              })
            }
          >
            Logout
          </Button>
        </Box>
      </ErrorView>
    </Page>
  );
};
