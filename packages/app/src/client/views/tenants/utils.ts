import { map } from "ramda";

import { Tenant, Tenants } from "state/tenant/types";
import { PanelItem } from "client/components/Panel";

export const tenantToItem = (tenant: Tenant): PanelItem => {
  return { id: tenant.name, text: tenant.name, data: tenant };
};

export const tenantsToItems: (tenants: Tenants) => PanelItem[] =
  map(tenantToItem);
