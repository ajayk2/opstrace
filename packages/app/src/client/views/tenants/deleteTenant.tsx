import React from "react";
import { useDispatch } from "react-redux";

import { usePickerService } from "client/services/Picker";

import { deleteTenant } from "state/tenant/actions";
import { Button } from "client/components/Button";
import { Tenant } from "state/tenant/types";

const DeleteTenantButton = ({ tenant }: { tenant: Tenant }) => {
  const dispatch = useDispatch();

  const { activatePickerWithText } = usePickerService(
    {
      title: `Delete ${tenant.name}?`,
      activationPrefix: `delete tenant ${tenant.name} directly?:`,
      disableFilter: true,
      disableInput: true,
      options: [
        {
          id: "yes",
          text: `yes`
        },
        {
          id: "no",
          text: "no"
        }
      ],
      onSelected: option => {
        if (option.id === "yes" && tenant.name)
          dispatch(deleteTenant(tenant.name));
      },
      dataTest: "deleteTenant"
    },
    [tenant.name]
  );

  return (
    <Button
      variant="outlined"
      state="error"
      size="small"
      disabled={tenant.type === "SYSTEM"}
      data-test={`tenant/deleteBtn/${tenant.name}`}
      onClick={e => {
        e.stopPropagation();
        activatePickerWithText(`delete tenant ${tenant.name} directly?: `);
      }}
    >
      Delete
    </Button>
  );
};

export default DeleteTenantButton;
