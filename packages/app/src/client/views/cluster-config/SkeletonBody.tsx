import React from "react";
import { TableRow, TableCell } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";

type Props = {
  numberRows: number;
  numberColumns: number;
};
const SkeletonBody = ({ numberRows, numberColumns }: Props) => {
  return (
    <>
      {Array(numberRows)
        .fill(true)
        .map((_, idx) => (
          <TableRow key={idx}>
            {Array(numberColumns)
              .fill(true)
              .map((_, idx) => (
                <TableCell key={idx}>
                  <Skeleton />
                </TableCell>
              ))}
          </TableRow>
        ))}
    </>
  );
};

export default SkeletonBody;
