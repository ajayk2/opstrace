import React from "react";
import Grid from "@material-ui/core/Grid";

import { grafanaUrl } from "client/utils/grafana";

import { Box } from "client/components/Box";
import { Card, CardContent, CardHeader } from "client/components/Card";
import Typography from "client/components/Typography/Typography";
import { useSelectedTenantWithFallback } from "state/tenant/hooks/useTenant";
import { ExternalLink } from "client/components/Link";

const TenantExplore = () => {
  const tenant = useSelectedTenantWithFallback();

  return (
    <>
      <Box pt={1} pb={4}>
        <Typography variant="h1">Overview</Typography>
      </Box>

      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              titleTypographyProps={{ variant: "h5" }}
              title={`Explore Logs for ${tenant.name} tenant`}
            />
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <Typography color="textSecondary" variant="body2">
                    Explore Logs in the <strong>{tenant.name}</strong> tenant.
                  </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Box display="flex" justifyContent="flex-end">
                    <ExternalLink
                      href={`${grafanaUrl({
                        tenant
                      })}/grafana/explore?orgId=1&left=%5B"now-1h","now","logs",%7B%7D%5D`}
                    >
                      Explore Logs →
                    </ExternalLink>
                  </Box>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12}>
          <Card>
            <CardHeader
              titleTypographyProps={{ variant: "h5" }}
              title={`Explore Metrics for ${tenant.name} tenant`}
            />
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <Typography color="textSecondary" variant="body2">
                    Explore Metrics in the <strong>{tenant.name}</strong>{" "}
                    tenant.
                  </Typography>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Box display="flex" justifyContent="flex-end">
                    <ExternalLink
                      href={`${grafanaUrl({
                        tenant
                      })}/grafana/explore?orgId=1&left=%5B"now-1h","now","metrics",%7B%7D%5D`}
                    >
                      Explore Metrics →
                    </ExternalLink>
                  </Box>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </>
  );
};

export default TenantExplore;
