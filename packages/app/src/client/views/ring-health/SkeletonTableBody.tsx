import React from "react";
import { TableRow, TableCell, TableBody } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";

type Props = {
  numberRows: number;
  numberColumns: number;
};
const SkeletonTableBody = ({ numberRows, numberColumns }: Props) => {
  return (
    <TableBody>
      {Array(numberRows)
        .fill(true)
        .map((_, idx) => (
          <TableRow key={idx}>
            {Array(numberColumns)
              .fill(true)
              .map((_, idx) => (
                <TableCell key={idx}>
                  <Skeleton />
                </TableCell>
              ))}
          </TableRow>
        ))}
    </TableBody>
  );
};

export default SkeletonTableBody;
