import React, { ReactNode } from "react";

import { Box } from "client/components/Box";
import Typography from "client/components/Typography/Typography";
import { Redirect, Route, Switch, useLocation } from "react-router-dom";
import MuiTabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";

import RingTable from "./RingTable";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    borderBottom: `1px solid ${theme.palette.divider}`
  }
}));

export type Tab = {
  title: string;
  path: string;
  endpoint: string;
};

type Props = {
  title: ReactNode;
  tabs: Array<Tab>;
};

const RingHealth = ({ tabs, title }: Props) => {
  const location = useLocation();
  const classes = useStyles();

  return (
    <div>
      <Box pt={1} pb={4}>
        <Typography variant="h1">{title}</Typography>
      </Box>
      <Switch>
        {tabs.map(tab => (
          <Route key={tab.path} path={tab.path}>
            <MuiTabs
              value={true}
              indicatorColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              classes={classes}
            >
              {tabs.map(tab => (
                <Tab
                  value={location.pathname.includes(tab.path)}
                  key={tab.title}
                  label={tab.title}
                  component={Link}
                  to={tab.path}
                  data-test={`ringHealth/tab/${tab.title}`}
                />
              ))}
            </MuiTabs>
            <Box mt={3}>
              <RingTable baseUrl={tab.path} ringEndpoint={tab.endpoint} />
            </Box>
          </Route>
        ))}
        <Route>
          {/* default to first tab if none is determined by URL */}
          <Redirect to={tabs[0].path} />;
        </Route>
      </Switch>
    </div>
  );
};

export default RingHealth;
