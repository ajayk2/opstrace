import { Theme } from "@material-ui/core/styles";

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ITheme extends Theme {}

export type PaletteType = "light" | "dark";
