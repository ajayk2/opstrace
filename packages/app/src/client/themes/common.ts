import { ThemeOptions } from "@material-ui/core/styles";
import * as colors from "@material-ui/core/colors";

const common: ThemeOptions = {
  props: {
    MuiBackdrop: {
      transitionDuration: 0
    },
    MuiDialog: {
      transitionDuration: 0
    },
    MuiListItemText: {
      primaryTypographyProps: {
        style: {
          fontWeight: 700
        }
      },
      secondaryTypographyProps: {
        style: {
          fontWeight: 700
        }
      }
    }
  },
  palette: {
    primary: {
      main: "#688EFF",
      contrastText: colors.common.white
    },
    secondary: {
      main: "#F54773",
      contrastText: colors.common.white
    },
    success: {
      main: colors.green.A400,
      contrastText: colors.common.black
    },
    warning: {
      main: colors.amber.A400,
      contrastText: colors.common.black
    },
    error: {
      main: colors.deepOrange.A400,
      contrastText: colors.common.white
    },
    info: {
      main: colors.blueGrey[200],
      contrastText: colors.common.black
    },
    contrastThreshold: 3
  },
  typography: {
    fontFamily: [
      "fakt-web",
      "-apple-system",
      "BlinkMacSystemFont",
      "Segoe UI",
      "Roboto",
      "Helvetica",
      "Arial",
      "sans-serif",
      "Apple Color Emoji",
      "Segoe UI Emoji",
      "Segoe UI Symbol"
    ].join(","),
    button: {
      textTransform: "none"
    },
    body2: {
      fontSize: "0.875rem",
      fontWeight: 400,
      lineHeight: 1.5,
      letterSpacing: "0.00938em"
    },
    body1: {
      fontSize: "1rem",
      fontWeight: 400,
      lineHeight: 1.5,
      letterSpacing: "0.00938em"
    },
    subtitle2: {
      fontWeight: 600,
      fontSize: "0.875rem"
    },
    h1: {
      fontWeight: 400,
      lineHeight: 1.26667,
      letterSpacing: "-0.00833em",
      fontSize: "1.875rem"
    },
    h6: {
      fontWeight: 800,
      fontSize: "0.8rem"
    }
  },
  shape: {
    borderRadius: 6
  }
};

export default common;
