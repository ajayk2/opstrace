import qs from "qs";
import { useHistory } from "react-router-dom";

export function getParametersFromSearchString(search: string) {
  return qs.parse(search.replace(/^\?/, ""));
}
/**
 * return query parameters
 */
function useQueryParams<T extends {}>(): T {
  const history = useHistory();
  return getParametersFromSearchString(history.location.search) as T;
}

export default useQueryParams;
