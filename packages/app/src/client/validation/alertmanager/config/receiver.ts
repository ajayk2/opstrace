import * as yup from "yup";

import { Receiver } from "./types";

import { emailConfigSchema } from "./emailConfig";
import { slackConfigSchema } from "./slackConfig";
import { pagerdutyConfigSchema } from "./pagerdutyConfig";
import { pushoverConfigSchema } from "./pushoverConfig";
import { opsgenieConfigSchema } from "./opsgenieConfig";
import { victoropsConfigSchema } from "./victoropsConfig";
import { webhookConfigSchema } from "./webhookConfig";
import { wechatConfigSchema } from "./wechatConfig";

export const receiverSchema: yup.SchemaOf<Receiver> = yup
  .object({
    name: yup.string().defined(),
    email_configs: yup.array().of(emailConfigSchema).notRequired(),
    slack_configs: yup.array().of(slackConfigSchema).notRequired(),
    pagerduty_configs: yup.array().of(pagerdutyConfigSchema).notRequired(),
    pushover_configs: yup.array().of(pushoverConfigSchema).notRequired(),
    opsgenie_configs: yup.array().of(opsgenieConfigSchema).notRequired(),
    victorops_configs: yup.array().of(victoropsConfigSchema).notRequired(),
    webhook_configs: yup.array().of(webhookConfigSchema).notRequired(),
    wechat_configs: yup.array().of(wechatConfigSchema).notRequired()
  })
  .meta({
    url: "https://www.prometheus.io/docs/alerting/latest/configuration/#receiver"
  })
  .noUnknown();
