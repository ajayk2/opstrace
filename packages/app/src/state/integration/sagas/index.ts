import { all, call, spawn, takeEvery, select, put } from "redux-saga/effects"; // put

import * as actions from "../actions";
import integrationListSubscriptionManager from "./integrationListSubscription";

import { selectIntegration } from "state/integration/hooks/useIntegration";
import { selectTenantById } from "state/tenant/hooks/useTenant";

import { Integration } from "state/integration/types";
import { Tenant } from "state/tenant/types";

import { getFolder, isGrafanaError } from "client/utils/grafana";
import uniqueId from "lodash/uniqueId";
import { actions as notificationActions } from "client/services/Notification/reducer";

// create a generic type
type AsyncReturnType<T extends (...args: any) => any> =
  // if T matches this signature and returns a Promise, extract
  // U (the type of the resolved promise) and use that, or...
  T extends (...args: any) => Promise<infer U>
    ? U // if T matches this signature and returns anything else, // extract the return value U and use that, or...
    : T extends (...args: any) => infer U
    ? U // if everything goes to hell, return an `any`
    : any;

export default function* integrationTaskManager() {
  const sagas = [
    integrationListSubscriptionManager,
    loadGrafanaStateForIntegrationListener
  ];

  // technique to keep the root alive and spawn sagas into their
  // own retry-on-failure loop.
  // https://redux-saga.js.org/docs/advanced/RootSaga.html
  yield all(
    sagas.map(saga =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (e: any) {
            console.error(e);
          }
        }
      })
    )
  );
}

function* loadGrafanaStateForIntegrationListener() {
  yield takeEvery(
    actions.loadGrafanaStateForIntegration,
    loadGrafanaStateForIntegration
  );
}

function* loadGrafanaStateForIntegration(
  action: ReturnType<typeof actions.loadGrafanaStateForIntegration>
) {
  try {
    const integration: Integration = yield select(
      selectIntegration,
      action.payload.id
    );

    const tenant: Tenant = yield select(
      selectTenantById,
      integration.tenant_id
    );

    const folderResponse: AsyncReturnType<typeof getFolder> = yield getFolder({
      integration,
      tenant
    });

    yield put(
      actions.updateGrafanaStateForIntegration({
        id: integration.id,
        grafana: {
          folder: folderResponse
        }
      })
    );
  } catch (error: any) {
    yield put(
      notificationActions.register({
        id: uniqueId(),
        state: "error" as const,
        title: "Could not load grafana state for integration",
        information: isGrafanaError(error)
          ? error.response.data.message
          : error.message
      })
    );
  }
}
