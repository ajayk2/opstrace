import { Integration as DBIntegration } from "state/clients/graphqlClient";

export type Integration = Pick<
  DBIntegration,
  | "id"
  | "tenant_id"
  | "name"
  | "key"
  | "kind"
  | "data"
  | "created_at"
  | "updated_at"
> & { grafana?: IntegrationGrafana };

export type Integrations = Integration[];
export type IntegrationRecords = Record<string, Integration>;

export type IntegrationGrafana = {
  folder?: IntegrationGrafanaFolder;
  status?: "pending" | "active" | "error" | "unknown";
};

export type IntegrationGrafanaFolder = {
  id?: number;
  path?: string;
};
// use this same id to unsubscribe
export type SubscriptionID = number;

export type IntegrationStatus = "pending" | "active" | "error" | "unknown";
export type IntegrationStatusRecords = Record<string, IntegrationStatus>;

export const INTEGRATION_STATUS: IntegrationStatusRecords = {
  pending: "pending",
  active: "active",
  error: "error",
  unknown: "unknown"
};
