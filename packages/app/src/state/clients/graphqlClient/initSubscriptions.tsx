import React, { useEffect } from "react";

import {
  startSubscriptionClient,
  stopSubscriptionClient
} from "state/clients/graphqlClient/subscriptionClient";

export const InitSubscriptions = ({
  children
}: {
  children: React.ReactNode;
}) => {
  useEffect(() => {
    // intentionally doing this once here to ensure that the WS Apollo connection is setup
    startSubscriptionClient();
    return stopSubscriptionClient;
  }, []);

  return <>{children}</>;
};
