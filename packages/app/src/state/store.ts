import { createStore as createReduxStore, applyMiddleware, Store } from "redux";
import createSagaMiddleware from "redux-saga";
import { composeWithDevTools } from "redux-devtools-extension";

import { mainReducer } from "./reducer";
import mainSaga from "./sagas";

let _store: Store;

export default function getStore(): Store {
  if (!_store) {
    _store = createMainStore();
  }
  return _store;
}

export function createMainStore(): Store {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];

  const middlewareEnhancer = applyMiddleware(...middlewares);
  const enhancers = [middlewareEnhancer];

  // mount it on the Store
  const store = createReduxStore(
    mainReducer,
    process.env.NODE_ENV === "development"
      ? composeWithDevTools(...enhancers)
      : middlewareEnhancer
  );

  sagaMiddleware.run(mainSaga);

  return store;
}
