export function sanitizeScope(scope: string) {
  return scope.replace(/^@/, "");
}

export function sanitizeFilePath(path: string) {
  return path?.replace(/^\//, "");
}

export function sanitizeFileExt(ext: string) {
  return ext.replace(/^\./, "").split("?")[0];
}
