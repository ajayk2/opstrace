import { User } from "./types";
export const isActive = (user: User, _id: string): boolean => user.active;
