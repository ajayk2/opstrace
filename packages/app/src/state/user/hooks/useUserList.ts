import { useEffect } from "react";
import { useDispatch, useSelector, State } from "state/provider";
import { subscribeToUserList, unsubscribeFromUserList } from "../actions";
import getSubscriptionID from "state/utils/getSubscriptionID";
import { Users } from "state/user/types";
import { values } from "ramda";

type GetUserListOptions = {
  includeInactive: boolean;
};

export const getUserList = (
  state: State,
  options: GetUserListOptions | null = null
) => {
  return values(
    options?.includeInactive ? state.users.allUsers : state.users.users
  ) as Users;
};

/**
 * Subscribes to users and will update on
 * any changes. Automatically unsubscribeFromUserLists
 * on unmount.
 */
export default function useUserList() {
  const users = useSelector(getUserList);
  const dispatch = useDispatch();

  useEffect(() => {
    const subId = getSubscriptionID();
    dispatch(subscribeToUserList(subId));

    return () => {
      dispatch(unsubscribeFromUserList(subId));
    };
  }, [dispatch]);

  return users;
}
