import yaml from "js-yaml";
import { cortexLimitsSchema, RuntimeConfig, Config } from "./types";

/**
 * Parses runtime config yaml into an Object. Will throw an Error if runtime config yaml is invalid
 * @param config runtime config yaml
 * @returns RuntimeConfig object
 */
export async function validateAndExtractRuntimeConfig(
  config: string
): Promise<RuntimeConfig> {
  let runtimeConfig = yaml.load(config);

  if (!("overrides" in runtimeConfig)) {
    runtimeConfig = { ...runtimeConfig, overrides: {} };
  }

  for (const limits of Object.values(runtimeConfig.overrides)) {
    await cortexLimitsSchema.validate(limits);
  }

  return runtimeConfig;
}

/**
 * Parses cortex config yaml into an Object. Will throw an Error if config yaml is invalid.
 * If config yaml is invalid, it implies that our schema is wrong.
 * @param config cortex config yaml
 * @returns Config object
 */
export async function validateCortexConfig(config: string): Promise<Config> {
  const data = yaml.load(config);
  await cortexLimitsSchema.validate(data.limits);

  return data;
}
