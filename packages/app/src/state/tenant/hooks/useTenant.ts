import { values, find, propEq } from "ramda";
import { useEffect, useState } from "react";
import { useRouteMatch } from "react-router";
import { createSelector } from "reselect";

import { useSelector, State } from "state/provider";

import { Tenant } from "state/tenant/types";
import { useTenantListSubscription } from "./useTenantList";

export const selectTenant = createSelector(
  (state: State) => state.tenants.loading,
  (state: State) => state.tenants.tenants,
  (_: State, name: string) => name,
  (loading, tenants, name: string) => (loading ? null : tenants[name])
);

export const selectTenantById = createSelector(
  (state: State) => state.tenants.loading,
  (state: State) => state.tenants.tenants,
  (_: State, id: string) => id,
  (loading, tenants, id: string) =>
    loading ? null : find(propEq("id", id))(values(tenants))
);

export function useSelectedTenantWithFallback(): Tenant {
  const tenant = useSelectedTenant();
  // We may choose to change this default to pick one from the users tenant list
  return tenant
    ? tenant
    : {
        name: "system",
        type: "SYSTEM",
        created_at: "",
        updated_at: "",
        id: "",
        key: ""
      };
}

export function useSelectedTenant() {
  // Assumes we use the structure in our URL /tenant/<tenantId>/*
  const tenantRouteMatch =
    useRouteMatch<{ tenantId: string }>("/tenant/:tenantId");
  return useTenant(tenantRouteMatch?.params.tenantId || "");
}

export const useLastSelectedTenant = () => {
  /* The difference between `useSelectedTenant` and `useLastSelectedTenant`
   * is that the latter remembers the last, previous selected tenant in case
   * none is selected right now.
   */
  const tenant = useSelectedTenant();
  const [lastSelectedTenant, setLastSelectedTenant] = useState(tenant);
  useEffect(() => {
    if (tenant) setLastSelectedTenant(tenant);
  }, [tenant]);
  return lastSelectedTenant;
};

export default function useTenant(
  tenantName: string
): Tenant | null | undefined {
  useTenantListSubscription();
  // can return undefined if the tenantName does not exist
  return useSelector((state: State) => selectTenant(state, tenantName));
}
