import express from "express";
import { log } from "@opstrace/utils/lib/log";
import { UnexpectedServerError } from "server/errors";

import { handleError } from "../utils";

// This middleware catches and stops errors by logging unexpected errors and returning a response back to the client.
const catchErrorsMiddleware = (
  err: any,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => {
  if (err) {
    if (err.stack) {
      log.error("unexpected error: ", err);
      // wrap error in generic
      return handleError(new UnexpectedServerError(err), res);
    }
    return handleError(err, res);
  }
  next();
};

export default catchErrorsMiddleware;
