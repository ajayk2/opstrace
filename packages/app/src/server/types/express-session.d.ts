import "express";
import { Session } from "express-session";
import "http";

declare module "express-session" {
  interface SessionData {
    userId: string;
    email: string;
    username: string;
    avatar: string;
  }
}

declare module "http" {
  interface IncomingMessage {
    session: Session & {
      userId: string;
      email: string;
      username: string;
      avatar: string;
    };
  }
}
