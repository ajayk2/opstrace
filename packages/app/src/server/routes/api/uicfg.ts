import env from "server/env";
import { NextFunction, Request, Response } from "express";

import { BUILD_INFO } from "@opstrace/utils";

export const AUTH0_CONFIG = {
  auth0_client_id: env.AUTH0_CLIENT_ID,
  auth0_domain: env.AUTH0_DOMAIN
};

export function pubUiCfgHandler(
  req: Request,
  res: Response,
  next: NextFunction
) {
  res.status(200).json(AUTH0_CONFIG);
  return;
}

// require authentication?
export function buildInfoHandler(
  req: Request,
  res: Response,
  next: NextFunction
) {
  res.status(200).json(BUILD_INFO);
  return;
}
