import httpProxy from "http-proxy";
import express from "express";

import env from "server/env";
import { log } from "@opstrace/utils/lib/log";
import { onProxyReq } from "server/utils";

export function getHasuraSessionHeaders(userId: string) {
  return {
    "X-Hasura-User-Id": userId,
    "X-Hasura-Role": "user_admin", // Set every user by default to "user_admin" in community edition.
    "X-Hasura-Admin-Secret": env.HASURA_GRAPHQL_ADMIN_SECRET
  };
}

/**
 * Proxy to our Graphql server
 */
export const graphqlProxy = httpProxy.createProxyServer({
  target: {
    host: env.GRAPHQL_ENDPOINT_HOST,
    port: env.GRAPHQL_ENDPOINT_PORT,
    path: "/v1/graphql"
  },
  ws: true
});

/**
 * bodyParser middleware (which we use earlier in chain to parse POST body content), doesn't play
 * well with the http-proxy, since the body is parsed and altered. It results in POSTs hanging until the connection times out.
 *
 * This is a workaround to restream the already parsed body, for the proxy target to consume.
 */
graphqlProxy.on("proxyReq", onProxyReq);

function createGraphqlHandler(): express.Router {
  const graphql = express.Router();

  graphql.use("/", (req, res) => {
    graphqlProxy.web(
      req,
      res,
      {
        headers: getHasuraSessionHeaders(req.session.userId!)
      },
      (err: Error) => {
        log.warning("error in http graphql proxy upstream (ignoring): %s", err);
      }
    );
  });

  return graphql;
}

export default createGraphqlHandler;
