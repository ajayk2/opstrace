type OriginalError = null | { name: string; message: string; stack?: string };
type ValidationError = Error & { errors: string[] };

export class ServerError extends Error {
  public originalError: OriginalError;
  public errorType: string = "OpstraceServerError";
  static isInstance(err: any): err is ServerError {
    return err.errorType === "OpstraceServerError";
  }
  constructor(public statusCode: number, public message: string, err?: Error) {
    super(message);
    this.name = this.constructor.name;
    this.statusCode = statusCode;
    this.message = message;
    this.originalError = err
      ? { name: err.name, message: err.message, stack: err.stack }
      : null;
    // we don't need a stacktrace for errors we expect.
    // this will help us log expected vs unexpected errors by
    // checking the presence of error.stack.
    this.stack = undefined;
  }
}

export class GeneralServerError extends ServerError {
  static isInstance(err: any): err is GeneralServerError {
    return err.name === "GeneralServerError";
  }
  constructor(public statusCode: number, public message: string, err?: Error) {
    super(statusCode, message, err);
    this.name = "GeneralServerError";
  }
}

export class UnexpectedServerError extends ServerError {
  public stack: string;
  static isInstance(err: any): err is UnexpectedServerError {
    return err.name === "UnexpectedServerError";
  }
  constructor(err: Error) {
    super(500, err.message, err);
    this.stack = err.stack || "";
    this.name = "UnexpectedServerError";
  }
}

export class PayloadValidationError extends ServerError {
  static isInstance(err: any): err is PayloadValidationError {
    return err.name === "PayloadValidationError";
  }
  constructor(err: ValidationError) {
    const msg = (err.errors || ["Validation failed"]).join(", ");
    super(400, msg, err);
    this.name = "PayloadValidationError";
  }
}
