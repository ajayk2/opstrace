// import {
//   LatestAWSInfraConfigType,
//   LatestGCPInfraConfigType
// } from "@opstrace/config";
// import { die, log } from "@opstrace/utils";

// import {
//   ClusterConfigFileSchemaTypeV1,
//   ClusterConfigFileSchemaV1
// } from "./schemasv1";

import {
  AWSInfraConfigSchemaV2,
  GCPInfraConfigSchemaV2,
  ClusterConfigFileSchemaV2,
  RenderedClusterConfigSchemaV2,
  ClusterConfigFileSchemaTypeV2,
  RenderedClusterConfigSchemaTypeV2
} from "./schemasv2";

// const pointing to the latest schema versions
export const LatestAWSInfraConfigSchema = AWSInfraConfigSchemaV2;
export const LatestGCPInfraConfigSchema = GCPInfraConfigSchemaV2;
export const LatestClusterConfigFileSchema = ClusterConfigFileSchemaV2;
export const LatestRenderedClusterConfigSchema = RenderedClusterConfigSchemaV2;

// type aliases poiting to the latest version types
export type LatestRenderedClusterConfigSchemaType =
  RenderedClusterConfigSchemaTypeV2;
export type LatestClusterConfigFileSchemaType = ClusterConfigFileSchemaTypeV2;
