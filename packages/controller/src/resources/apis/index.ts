import { KubeConfig } from "@kubernetes/client-node";

import { ResourceCollection } from "@opstrace/kubernetes";
import { State } from "../../reducer";

// End-user APIs that are authenticated using Grafana Tokens
import { ConfigAPIResources } from "./config";
import { CortexAPIResources } from "./cortex";
import { DDAPIResources } from "./dd";
import { TracingAPIResources } from "./tracing";

/* Translate node count into replica count*/
export function nodecountToReplicacount(nodecount: number): number {
  const NC_RC_MAP: Record<string, number> = {
    "1": 1,
    "2": 2,
    "3": 2,
    "4": 3
  };

  const ncstring = nodecount.toFixed(0);
  if (ncstring in NC_RC_MAP) {
    return NC_RC_MAP[ncstring];
  }

  return Math.floor(nodecount / 2);
}

export function APIResources(
  state: State,
  kubeConfig: KubeConfig
): ResourceCollection {
  const collection = new ResourceCollection();

  // Per-tenant resources
  state.tenants.list.tenants.forEach(tenant => {
    collection.add(ConfigAPIResources(state, tenant, kubeConfig));
    collection.add(CortexAPIResources(state, tenant, kubeConfig));
    collection.add(DDAPIResources(state, tenant, kubeConfig));
    collection.add(TracingAPIResources(state, tenant, kubeConfig));
  });

  return collection;
}
