import {
  Deployment,
  Ingress,
  ResourceCollection,
  Service,
  V1ServicemonitorResource,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import { State } from "../../reducer";
import { getApiDomain, getTenantNamespace } from "../../helpers";
import { Tenant } from "@opstrace/tenants";
import { KubeConfig } from "@kubernetes/client-node";
import { DockerImages, getImagePullSecrets } from "@opstrace/controller-config";

export function ConfigAPIResources(
  state: State,
  tenant: Tenant,
  kubeConfig: KubeConfig
): ResourceCollection {
  const collection = new ResourceCollection();

  const namespace = getTenantNamespace(tenant);
  const api = "config";
  const name = `${api}-api`;

  const probeConfig = {
    httpGet: {
      path: "/metrics",
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      port: "metrics" as any,
      scheme: "HTTP"
    },
    timeoutSeconds: 1,
    periodSeconds: 10,
    successThreshold: 1,
    failureThreshold: 3
  };

  // TODO(nickbp): Remove this per-tenant deployment in favor of a shared/multitenant deployment
  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name,
          namespace,
          labels: {
            app: name
          }
        },
        spec: {
          replicas: 1,
          selector: {
            matchLabels: {
              app: name
            }
          },
          strategy: {
            type: "RollingUpdate",
            rollingUpdate: {
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxSurge: "25%" as any,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxUnavailable: "25%" as any
            }
          },
          template: {
            metadata: {
              labels: {
                app: name
              }
            },
            spec: {
              imagePullSecrets: getImagePullSecrets(),
              affinity: withPodAntiAffinityRequired({
                app: name
              }),
              containers: [
                {
                  name,
                  image: DockerImages.configApi,
                  imagePullPolicy: "IfNotPresent",
                  args: [
                    "-listen=:8080",
                    "-metrics=:8081",
                    `-tenantname=${tenant.name}`
                  ],
                  env: [
                    {
                      name: "CORTEX_RULER_ENDPOINT",
                      value: "http://ruler.cortex.svc.cluster.local"
                    },
                    {
                      name: "CORTEX_ALERTMANAGER_ENDPOINT",
                      value: "http://alertmanager.cortex.svc.cluster.local"
                    }
                  ],
                  ports: [
                    {
                      name: "http",
                      protocol: "TCP",
                      containerPort: 8080
                    },
                    {
                      name: "metrics",
                      protocol: "TCP",
                      containerPort: 8081
                    }
                  ],
                  readinessProbe: probeConfig,
                  livenessProbe: probeConfig,
                  resources: {}
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name,
          labels: {
            app: name,
            job: `${namespace}.${name}`
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 8080,
              protocol: "TCP",
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "http" as any
            },
            {
              name: "metrics",
              port: 8081,
              protocol: "TCP",
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "metrics" as any
            }
          ],
          selector: {
            app: name
          }
        }
      },
      kubeConfig
    )
  );

  const ingressAnnotations: Record<string, string> = {
    "kubernetes.io/ingress.class": "api",
    "external-dns.alpha.kubernetes.io/ttl": "30",
    "nginx.ingress.kubernetes.io/client-body-buffer-size": "10m",

    // On successful auth, assign the correct X-Scope-OrgID header in the backend request.
    // This is used by cortex-api to identify the tenant.
    // WARNING: Don't forget the trailing semicolon or else routes will silently fail.
    "nginx.ingress.kubernetes.io/configuration-snippet": ` more_set_input_headers "X-Scope-OrgID: ${tenant.name}";`
  };

  // Forward "Authorization: Basic xxxxx" API token to tenant Grafana for validation
  // TODO(nickbp): This endpoint just checks that the token is valid/unexpired,
  //     and doesn't check its assigned access level. Switch to a new endpoint later
  //     (via gatekeeper?) that checks the token has editor+ access.
  // TODO(nickbp): This endpoint returns `no-cache` headers that block ingress-nginx
  //     from caching the response. Switch to a new Gatekeeper endpoint that allows caching.
  //     (For now the below caching config doesn't actually do anything)
  ingressAnnotations[
    "nginx.ingress.kubernetes.io/auth-url"
  ] = `http://grafana.${tenant.name}-tenant.svc.cluster.local:3000/api/login/ping`;

  // Cache the auth response to avoid querying the auth endpoint too frequently.
  ingressAnnotations["nginx.ingress.kubernetes.io/auth-cache-key"] =
    "$remote_user$http_authorization";
  // 401: If something fails auth, it's unlikely to start soon, since new tokens are randomly generated.
  // 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
  // 50x: If Grafana is returning 500 errors, allow them to clear up quickly when Grafana comes back,
  //      but allow some caching to avoid Grafana getting hammered with retries when down.
  ingressAnnotations["nginx.ingress.kubernetes.io/auth-cache-duration"] =
    "401 5m, 200 202 2m, 500 503 30s";

  const apiHost = getApiDomain(api, tenant, state);
  collection.add(
    new Ingress(
      {
        apiVersion: "networking.k8s.io/v1beta1",
        kind: "Ingress",
        metadata: {
          name: `${api}`,
          namespace,
          annotations: ingressAnnotations
        },
        spec: {
          tls: [
            {
              hosts: [apiHost],
              secretName: "https-cert"
            }
          ],
          rules: [
            {
              host: apiHost,
              http: {
                paths: [
                  {
                    backend: {
                      // TODO(nickbp): Switch this to send to a shared 'config-api' deployment (not per-tenant)
                      //               Should be possible with X-Scope-OrgID now being set by the ingress after successful tenant auth.
                      serviceName: name,
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      servicePort: 8080 as any
                    },
                    pathType: "ImplementationSpecific",
                    path: "/"
                  }
                ]
              }
            }
          ]
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          labels: {
            app: name,
            tenant: "system"
          },
          name,
          namespace
        },
        spec: {
          endpoints: [
            {
              interval: "30s",
              path: "/metrics",
              port: "metrics"
            }
          ],
          jobLabel: "job",
          namespaceSelector: {
            matchNames: [namespace]
          },
          selector: {
            matchLabels: {
              app: name
            }
          }
        }
      },
      kubeConfig
    )
  );

  return collection;
}
