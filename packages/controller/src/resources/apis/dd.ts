import {
  ResourceCollection,
  ServiceAccount,
  ClusterRole,
  ClusterRoleBinding,
  Deployment,
  Ingress,
  Service,
  V1ServicemonitorResource,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import { State } from "../../reducer";
import { Tenant } from "@opstrace/tenants";
import { KubeConfig } from "@kubernetes/client-node";
import { getApiDomain, getNodeCount, getTenantNamespace } from "../../helpers";
import { nodecountToReplicacount } from "./index";
import { DockerImages, getImagePullSecrets } from "@opstrace/controller-config";

export function DDAPIResources(
  state: State,
  tenant: Tenant,
  kubeConfig: KubeConfig
): ResourceCollection {
  const collection = new ResourceCollection();

  // Do not deploy DD API for system tenant.
  if (tenant.type === "SYSTEM") {
    return collection;
  }

  const config = {
    replicas: nodecountToReplicacount(getNodeCount(state)),
    resources: {}
  };

  const namespace = getTenantNamespace(tenant);
  const apiName = "dd";
  const deplName = `${apiName}-api`;
  const remoteWriteURL =
    "http://distributor.cortex.svc.cluster.local/api/v1/push";

  // TODO(nickbp): Remove this per-tenant deployment in favor of a shared/multitenant deployment
  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name: deplName,
          namespace,
          labels: {
            "k8s-app": deplName
          }
        },
        spec: {
          replicas: config.replicas,
          selector: {
            matchLabels: {
              "k8s-app": deplName
            }
          },
          template: {
            metadata: {
              labels: {
                "k8s-app": deplName
              }
            },
            spec: {
              imagePullSecrets: getImagePullSecrets(),
              affinity: withPodAntiAffinityRequired({
                "k8s-app": deplName
              }),
              containers: [
                {
                  name: "dd-api",
                  image: DockerImages.ddApi,
                  imagePullPolicy: "IfNotPresent",
                  args: [
                    "-listen=:8080",
                    `-tenantname=${tenant.name}`,
                    `-prom-remote-write-url=${remoteWriteURL}`
                  ],
                  ports: [
                    {
                      name: "http",
                      protocol: "TCP",
                      containerPort: 8080
                    }
                  ],
                  readinessProbe: {
                    httpGet: {
                      path: "/metrics",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 8080 as any,
                      scheme: "HTTP"
                    },
                    timeoutSeconds: 1,
                    periodSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3
                  },
                  livenessProbe: {
                    httpGet: {
                      path: "/metrics",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 8080 as any,
                      scheme: "HTTP"
                    },
                    timeoutSeconds: 1,
                    periodSeconds: 10,
                    successThreshold: 1,
                    failureThreshold: 3
                  },
                  resources: config.resources
                }
              ],
              serviceAccountName: deplName
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name: deplName,
          labels: {
            "k8s-app": deplName,
            job: `${namespace}.dd-api`
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 8080,
              protocol: "TCP",
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: 8080 as any
            }
          ],
          selector: {
            "k8s-app": deplName
          }
        }
      },
      kubeConfig
    )
  );

  const ingressAnnotations: Record<string, string> = {
    "kubernetes.io/ingress.class": "api",
    "external-dns.alpha.kubernetes.io/ttl": "30",
    "nginx.ingress.kubernetes.io/client-body-buffer-size": "10m",

    // 1. When forwarding the auth request, get the token from the Datadog '?api_key=...' query string
    //    and pass it to the auth service as an 'Authorization: Bearer ...' header.
    //    This allows us to support the datadog agent which passes api tokens via '?api_key=...'
    //    See also: https://stackoverflow.com/questions/54180469/nginx-decode-url-query-parameter-and-forward-it-as-request-header
    // 2. On successful auth, assign the correct X-Scope-OrgID header in the backend request.
    //    This is used by ddapi to identify the tenant.
    //    WARNING: Don't forget the trailing semicolon or else routes will silently fail.
    "nginx.ingress.kubernetes.io/configuration-snippet": `
more_set_input_headers "X-Scope-OrgID: ${tenant.name}";

# extract "?api_key=..." query string value for "Authorization: Bearer ..." header to auth service
set $api_key_content $arg_api_key;
if ($args ~* (.*)(^|&)api_key=[^&]*(\\2|$)&?(.*)) {
  # remove api_key from query to be forwarded
  set $args $1$3$4;
}
# ideally we'd have a rewrite_by_lua_block for unescaping the api_key URI string,
# but the resulting config fails due to duplicate rewrite_by_lua_block`,
    "nginx.ingress.kubernetes.io/auth-snippet": `
# send the captured and unescaped api_key value to the auth service
proxy_set_header Authorization "Bearer $api_key_content";`
  };

  // Forward "Authorization: Basic xxxxx" API token to tenant Grafana for validation
  // TODO(nickbp): This endpoint just checks that the token is valid/unexpired,
  //     and doesn't check its assigned access level. Switch to a new endpoint later
  //     (via gatekeeper?) that checks the token has editor+ access.
  // TODO(nickbp): This endpoint returns `no-cache` headers that block ingress-nginx
  //     from caching the response. Switch to a new Gatekeeper endpoint that allows caching.
  //     (For now the below caching config doesn't actually do anything)
  ingressAnnotations[
    "nginx.ingress.kubernetes.io/auth-url"
  ] = `http://grafana.${tenant.name}-tenant.svc.cluster.local:3000/api/login/ping`;

  // Cache the auth response to avoid querying the auth endpoint too frequently.
  ingressAnnotations["nginx.ingress.kubernetes.io/auth-cache-key"] =
    "$remote_user$http_authorization";
  // 401: If something fails auth, it's unlikely to start soon, since new tokens are randomly generated.
  // 20x: If a token is revoked or expires, allow them to start failing after at most 2 minutes
  // 50x: If Grafana is returning 500 errors, allow them to clear up quickly when Grafana comes back,
  //      but allow some caching to avoid Grafana getting hammered with retries when down.
  ingressAnnotations["nginx.ingress.kubernetes.io/auth-cache-duration"] =
    "401 5m, 200 202 2m, 500 503 30s";

  const apiHost = getApiDomain(apiName, tenant, state);
  collection.add(
    new Ingress(
      {
        apiVersion: "networking.k8s.io/v1beta1",
        kind: "Ingress",
        metadata: {
          name: apiName,
          namespace,
          annotations: ingressAnnotations
        },
        spec: {
          tls: [
            {
              hosts: [apiHost],
              secretName: "https-cert"
            }
          ],
          rules: [
            {
              host: apiHost,
              http: {
                paths: [
                  {
                    backend: {
                      // TODO(nickbp): Switch this to send to a shared 'ddapi' deployment (not per-tenant).
                      //               Should be possible with X-Scope-OrgID now being set by the ingress after successful tenant auth.
                      serviceName: deplName,
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      servicePort: 8080 as any
                    },
                    pathType: "ImplementationSpecific",
                    path: "/"
                  }
                ]
              }
            }
          ]
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new V1ServicemonitorResource(
      {
        apiVersion: "monitoring.coreos.com/v1",
        kind: "ServiceMonitor",
        metadata: {
          labels: {
            "k8s-app": deplName,
            tenant: "system"
          },
          name: deplName,
          namespace
        },
        spec: {
          endpoints: [
            {
              interval: "30s",
              path: "/metrics",
              port: "http"
            }
          ],
          jobLabel: "job",
          namespaceSelector: {
            matchNames: [namespace]
          },
          selector: {
            matchLabels: {
              "k8s-app": deplName
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ServiceAccount(
      {
        apiVersion: "v1",
        kind: "ServiceAccount",
        metadata: {
          name: deplName,
          namespace
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ClusterRole(
      {
        apiVersion: "rbac.authorization.k8s.io/v1",
        kind: "ClusterRole",
        metadata: {
          name: deplName
        },
        rules: [
          {
            apiGroups: [""],
            resources: ["namespaces", "pods"],
            verbs: ["get", "list", "watch"]
          }
        ]
      },
      kubeConfig
    )
  );

  collection.add(
    new ClusterRoleBinding(
      {
        apiVersion: "rbac.authorization.k8s.io/v1",
        kind: "ClusterRoleBinding",
        metadata: {
          name: deplName
        },
        roleRef: {
          apiGroup: "rbac.authorization.k8s.io",
          kind: "ClusterRole",
          name: deplName
        },
        subjects: [
          {
            kind: "ServiceAccount",
            name: deplName,
            namespace
          }
        ]
      },
      kubeConfig
    )
  );

  return collection;
}
