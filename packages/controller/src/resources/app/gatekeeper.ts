import { strict as assert } from "assert";

import { urlJoin } from "url-join-ts";
import {
  ResourceCollection,
  Deployment,
  ClusterRoleBinding,
  ServiceAccount,
  Namespace,
  Service,
  Secret,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import { log } from "@opstrace/utils";
import { KubeConfig } from "@kubernetes/client-node";
import { State } from "../../reducer";
import { generateSecretValue, getControllerConfig } from "../../helpers";
import {
  CONTROLLER_NAMESPACE,
  DockerImages,
  getImagePullSecrets,
  isPostgresSecret,
  POSTGRES_SECRET_NAME
} from "@opstrace/controller-config";

export function OpstraceAuthServiceResources(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string,
  domain: string
): ResourceCollection {
  const collection = new ResourceCollection();

  const { gitlab } = getControllerConfig(state);

  const {
    oauth_client_id,
    oauth_client_secret,
    instance_url,
    group_allowed_access,
    group_allowed_system_access
  } = gitlab;

  assert(
    group_allowed_system_access,
    "gitlab.group_allowed_system_access not provided"
  );
  assert(group_allowed_access, "gitlab.group_allowed_access not provided");
  assert(instance_url, "gitlab.instance_url not provided");
  assert(oauth_client_id, "gitlab.oauth_client_id not provided");
  assert(oauth_client_secret, "gitlab.oauth_client_secret not provided");

  collection.add(
    new Namespace(
      {
        apiVersion: "v1",
        kind: "Namespace",
        metadata: {
          name: namespace,
          labels: {
            "cert-manager.io/disable-validation": "true"
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name: "gatekeeper",
          labels: {
            app: "gatekeeper"
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 3001,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "http" as any
            }
          ],
          selector: {
            app: "gatekeeper"
          }
        }
      },
      kubeConfig
    )
  );

  const sessionCookieSecret = new Secret(
    {
      apiVersion: "v1",
      data: {
        COOKIE_SECRET: Buffer.from(generateSecretValue()).toString("base64")
      },
      kind: "Secret",
      metadata: {
        name: "session-cookie-secret",
        namespace: namespace
      }
    },
    kubeConfig
  );
  // We don't want this value to change once it exists.
  // This value changing would cause all sessions to be invalidated immediately
  // and that would be a bad experience during an upgrade.
  // This value of this secret can always be updated manually if instant session invalidation is desired.
  sessionCookieSecret.setImmutable();
  collection.add(sessionCookieSecret);

  collection.add(postgresSecret(state, kubeConfig, namespace));

  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name: "gatekeeper",
          namespace,
          labels: {
            app: "gatekeeper"
          }
        },
        spec: {
          replicas: 1,
          strategy: {
            type: "RollingUpdate",
            rollingUpdate: {
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxSurge: "25%" as any,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxUnavailable: "25%" as any
            }
          },
          selector: {
            matchLabels: {
              app: "gatekeeper"
            }
          },
          template: {
            metadata: {
              labels: {
                app: "gatekeeper"
              }
            },
            spec: {
              imagePullSecrets: getImagePullSecrets(),
              serviceAccountName: "gatekeeper",
              terminationGracePeriodSeconds: 10,
              affinity: withPodAntiAffinityRequired({
                app: "gatekeeper"
              }),
              containers: [
                {
                  name: "gatekeeper",
                  image: DockerImages.gatekeeper,
                  imagePullPolicy: "IfNotPresent",
                  // See envvar retrieval in /go/cmd/gatekeeper/main.go
                  env: [
                    // Redis caching
                    {
                      name: "REDIS_ADDRESS",
                      value: `redis-master.${namespace}.svc.cluster.local:6379`
                    },
                    {
                      name: "REDIS_PASSWORD",
                      valueFrom: {
                        secretKeyRef: {
                          name: "redis-password",
                          key: "REDIS_MASTER_PASSWORD"
                        }
                      }
                    },

                    // Postgres host/credentials
                    // TODO(nickbp): start using this in gatekeeper...
                    {
                      name: "POSTGRES_ENDPOINT",
                      valueFrom: {
                        secretKeyRef: {
                          name: POSTGRES_SECRET_NAME,
                          key: "endpoint"
                        }
                      }
                    },

                    // Session cookie
                    {
                      name: "USE_SECURE_COOKIE",
                      value: "true"
                    },
                    {
                      name: "COOKIE_SECRET",
                      valueFrom: {
                        secretKeyRef: {
                          name: "session-cookie-secret",
                          key: "COOKIE_SECRET"
                        }
                      }
                    },

                    // OAuth/GitLab integration
                    {
                      name: "GITLAB_OAUTH_CLIENT_ID",
                      value: oauth_client_id
                    },
                    {
                      name: "GITLAB_OAUTH_CLIENT_SECRET",
                      value: oauth_client_secret
                    },
                    {
                      name: "DOMAIN",
                      value: `https://${domain}`
                    },
                    {
                      name: "GITLAB_INSTANCE_URL",
                      value: instance_url
                    },

                    // GitLab groups
                    {
                      name: "GITLAB_GROUP_ALLOWED_ACCESS",
                      value: group_allowed_access
                    },
                    {
                      name: "GITLAB_GROUP_ALLOWED_SYSTEM_NAMESPACE_ACCESS",
                      value: group_allowed_system_access
                    }
                  ],
                  ports: [
                    {
                      containerPort: 3001,
                      name: "http"
                    }
                  ],
                  resources: {},
                  readinessProbe: {
                    httpGet: {
                      path: "/ready",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 3001 as any,
                      scheme: "HTTP"
                    },
                    failureThreshold: 1,
                    initialDelaySeconds: 5,
                    periodSeconds: 5,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  },
                  livenessProbe: {
                    httpGet: {
                      path: "/live",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 3001 as any,
                      scheme: "HTTP"
                    },
                    failureThreshold: 3,
                    initialDelaySeconds: 5,
                    periodSeconds: 30,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  },
                  startupProbe: {
                    httpGet: {
                      path: "/live",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 3001 as any
                    },
                    failureThreshold: 3,
                    initialDelaySeconds: 10,
                    periodSeconds: 30,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  }
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ServiceAccount(
      {
        apiVersion: "v1",
        kind: "ServiceAccount",
        metadata: {
          name: "gatekeeper",
          namespace
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ClusterRoleBinding(
      {
        apiVersion: "rbac.authorization.k8s.io/v1",
        kind: "ClusterRoleBinding",
        metadata: {
          name: "gatekeeper-clusteradmin-binding"
        },
        roleRef: {
          apiGroup: "rbac.authorization.k8s.io",
          kind: "ClusterRole",
          name: "cluster-admin"
        },
        subjects: [
          {
            kind: "ServiceAccount",
            name: "gatekeeper",
            namespace
          }
        ]
      },
      kubeConfig
    )
  );

  return collection;
}

function postgresSecret(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string
): Secret {
  // copy the secret that we've already set for Postgres in the kube-system namespace
  const postgresSecretToCopy =
    state.kubernetes.cluster.Secrets.resources.find(isPostgresSecret);

  if (postgresSecretToCopy && postgresSecretToCopy.data?.endpoint) {
    // secret found in kube-system: copy to this namespace
    const secret = new Secret(
      {
        apiVersion: "v1",
        data: {
          // already base64-encoded
          endpoint: postgresSecretToCopy.data?.endpoint
        },
        kind: "Secret",
        metadata: {
          name: POSTGRES_SECRET_NAME,
          namespace: namespace
        }
      },
      kubeConfig
    );
    secret.setImmutable();
    return secret;
  } else {
    // secret not found in kube-system: fall back to config data
    // TODO(nickbp): On or after April 2022, replace this fallback with an error.
    //     Clusters created/upgraded after February should have the secret in kube-system
    log.warning(
      `Missing expected ${CONTROLLER_NAMESPACE}/${POSTGRES_SECRET_NAME} secret, falling back to controller config. ` +
        `Add 'POSTGRES_ENDPOINT' envvar to controller that points to ${CONTROLLER_NAMESPACE}/${POSTGRES_SECRET_NAME}.endpoint`
    );
    const fallbackSecret = new Secret(
      {
        apiVersion: "v1",
        data: {
          endpoint: Buffer.from(
            urlJoin(
              state.config.config?.postgreSQLEndpoint,
              state.config.config?.opstraceDBName
            )
          ).toString("base64")
        },
        kind: "Secret",
        metadata: {
          name: POSTGRES_SECRET_NAME,
          namespace: namespace
        }
      },
      kubeConfig
    );
    fallbackSecret.setImmutable();
    return fallbackSecret;
  }
}
