import { Ingress } from "@opstrace/kubernetes";
import { KubeConfig } from "@kubernetes/client-node";

import {
  ResourceCollection,
  V1CertificateResource
} from "@opstrace/kubernetes";
import { State } from "../../reducer";
import { getControllerConfig, getDomain } from "../../helpers";

import { OpstraceApplicationResources } from "./app";
import { OpstraceAuthServiceResources } from "./gatekeeper";

export function ApplicationResources(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string
): ResourceCollection {
  const collection = new ResourceCollection();

  const domain = getDomain(state);

  // App UI
  collection.add(
    OpstraceApplicationResources(state, kubeConfig, namespace, domain)
  );
  // Gatekeeper
  collection.add(
    OpstraceAuthServiceResources(state, kubeConfig, namespace, domain)
  );

  // Certificate for the root domain, used by the App Ingress and Config Ingress
  const { tlsCertificateIssuer } = getControllerConfig(state);
  collection.add(
    new V1CertificateResource(
      {
        apiVersion: "cert-manager.io/v1",
        kind: "Certificate",
        metadata: {
          name: "https-cert",
          namespace: namespace
        },
        spec: {
          secretName: "https-cert",
          dnsNames: [getDomain(state)],
          issuerRef: {
            name: tlsCertificateIssuer,
            kind: "ClusterIssuer"
          }
        }
      },
      kubeConfig
    )
  );

  // Ingress for the root domain, shared by the App UI and Config API
  collection.add(
    new Ingress(
      {
        apiVersion: "networking.k8s.io/v1beta1",
        kind: "Ingress",
        metadata: {
          name: "opstrace-root",
          namespace,
          annotations: {
            "kubernetes.io/ingress.class": "ui",
            "external-dns.alpha.kubernetes.io/ttl": "30",
            "nginx.ingress.kubernetes.io/client-body-buffer-size": "10m"
          }
        },
        spec: {
          tls: [
            {
              hosts: [domain],
              secretName: "https-cert"
            }
          ],
          rules: [
            {
              host: domain,
              http: {
                paths: [
                  {
                    backend: {
                      serviceName: "opstrace-application",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      servicePort: "http" as any
                    },
                    pathType: "Prefix",
                    path: "/"
                  }
                ]
              }
            }
          ]
        }
      },
      kubeConfig
    )
  );

  return collection;
}
