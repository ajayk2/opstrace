import { strict as assert } from "assert";

import {
  ResourceCollection,
  Deployment,
  ClusterRoleBinding,
  ServiceAccount,
  Namespace,
  Service,
  Secret,
  withPodAntiAffinityRequired
} from "@opstrace/kubernetes";
import { KubeConfig } from "@kubernetes/client-node";
import { State } from "../../reducer";
import { generateSecretValue, getControllerConfig } from "../../helpers";
import {
  CONTROLLER_NAMESPACE,
  DockerImages,
  getImagePullSecrets,
  POSTGRES_SECRET_NAME
} from "@opstrace/controller-config";

export function OpstraceApplicationResources(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string,
  domain: string
): ResourceCollection {
  const collection = new ResourceCollection();

  const { custom_auth0_client_id, custom_auth0_domain } =
    getControllerConfig(state);

  assert(custom_auth0_domain, "custom_auth0_domain is not set");
  assert(custom_auth0_client_id, "custom_auth0_client_id is not set");
  const auth0_client_id = custom_auth0_client_id;
  const auth0_domain = custom_auth0_domain;

  collection.add(
    new Namespace(
      {
        apiVersion: "v1",
        kind: "Namespace",
        metadata: {
          name: namespace,
          labels: {
            "cert-manager.io/disable-validation": "true"
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name: "opstrace-application",
          labels: {
            app: "opstrace-application"
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 3001,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "http" as any
            }
          ],
          selector: {
            app: "opstrace-application"
          }
        }
      },
      kubeConfig
    )
  );

  // TODO(nickbp): Remove this once the app no longer uses Hasura
  collection.add(hasuraSecrets(state, kubeConfig, namespace));

  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name: "opstrace-application",
          namespace,
          labels: {
            app: "opstrace-application"
          }
        },
        spec: {
          replicas: 1,
          strategy: {
            type: "RollingUpdate",
            rollingUpdate: {
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxSurge: "25%" as any,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxUnavailable: "25%" as any
            }
          },
          selector: {
            matchLabels: {
              app: "opstrace-application"
            }
          },
          template: {
            metadata: {
              labels: {
                app: "opstrace-application"
              }
            },
            spec: {
              imagePullSecrets: getImagePullSecrets(),
              serviceAccountName: "opstrace-application",
              terminationGracePeriodSeconds: 80, // we give the app 60 seconds to drain existing connections
              affinity: withPodAntiAffinityRequired({
                app: "opstrace-application"
              }),
              containers: [
                {
                  name: "opstrace-application",
                  image: DockerImages.app,
                  imagePullPolicy: "IfNotPresent",
                  command: ["node", "dist/server.js"],
                  env: [
                    {
                      name: "REDIS_HOST",
                      value: `redis-master.${namespace}.svc.cluster.local`
                    },
                    {
                      name: "REDIS_PASSWORD",
                      valueFrom: {
                        secretKeyRef: {
                          name: "redis-password",
                          key: "REDIS_MASTER_PASSWORD"
                        }
                      }
                    },
                    {
                      name: "GRAPHQL_ENDPOINT_HOST",
                      value: `graphql.${namespace}.svc.cluster.local`
                    },
                    { name: "GRAPHQL_ENDPOINT_PORT", value: "8080" },
                    {
                      name: "GRAPHQL_ENDPOINT",
                      value: `http://graphql.${namespace}.svc.cluster.local:8080/v1/graphql`
                    },
                    {
                      name: "AUTH0_CLIENT_ID",
                      value: auth0_client_id
                    },
                    {
                      name: "AUTH0_DOMAIN",
                      value: auth0_domain
                    },
                    { name: "DOMAIN", value: `https://${domain}` },
                    { name: "UI_DOMAIN", value: `https://${domain}` },
                    {
                      name: "COOKIE_SECRET",
                      valueFrom: {
                        secretKeyRef: {
                          // Created by gatekeeper.ts
                          name: "session-cookie-secret",
                          key: "COOKIE_SECRET"
                        }
                      }
                    },
                    {
                      name: "HASURA_GRAPHQL_ADMIN_SECRET",
                      valueFrom: {
                        secretKeyRef: {
                          name: "hasura-admin-secret",
                          key: "HASURA_ADMIN_SECRET"
                        }
                      }
                    }
                  ],
                  ports: [
                    {
                      containerPort: 3001,
                      name: "http"
                    }
                  ],
                  resources: {},
                  readinessProbe: {
                    httpGet: {
                      path: "/ready",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 9000 as any,
                      scheme: "HTTP"
                    },
                    failureThreshold: 1,
                    initialDelaySeconds: 5,
                    periodSeconds: 5,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  },
                  livenessProbe: {
                    httpGet: {
                      path: "/live",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 9000 as any,
                      scheme: "HTTP"
                    },
                    failureThreshold: 3,
                    initialDelaySeconds: 5,
                    periodSeconds: 30,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  },
                  startupProbe: {
                    httpGet: {
                      path: "/live",
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      port: 9000 as any
                    },
                    failureThreshold: 3,
                    initialDelaySeconds: 10,
                    periodSeconds: 30,
                    successThreshold: 1,
                    timeoutSeconds: 5
                  }
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name: "graphql",
          labels: {
            app: "graphql"
          },
          namespace
        },
        spec: {
          ports: [
            {
              name: "http",
              port: 8080,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              targetPort: "http" as any
            }
          ],
          selector: {
            app: "graphql"
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new Deployment(
      {
        apiVersion: "apps/v1",
        kind: "Deployment",
        metadata: {
          name: "graphql",
          namespace,
          labels: {
            app: "graphql"
          }
        },
        spec: {
          replicas: 1,
          strategy: {
            type: "RollingUpdate",
            rollingUpdate: {
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxSurge: "25%" as any,
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              maxUnavailable: "25%" as any
            }
          },
          selector: {
            matchLabels: {
              app: "graphql"
            }
          },
          template: {
            metadata: {
              labels: {
                app: "graphql"
              }
            },
            spec: {
              imagePullSecrets: getImagePullSecrets(),
              affinity: withPodAntiAffinityRequired({
                app: "graphql"
              }),
              containers: [
                {
                  name: "graphql",
                  image: DockerImages.graphqlEngine,
                  imagePullPolicy: "IfNotPresent",
                  env: [
                    {
                      name: "HASURA_GRAPHQL_ADMIN_SECRET",
                      valueFrom: {
                        secretKeyRef: {
                          name: "hasura-admin-secret",
                          key: "HASURA_ADMIN_SECRET"
                        }
                      }
                    },
                    {
                      name: "HASURA_GRAPHQL_DATABASE_URL",
                      valueFrom: {
                        secretKeyRef: {
                          // Created by gatekeeper.ts
                          name: POSTGRES_SECRET_NAME,
                          key: "endpoint"
                        }
                      }
                    },
                    {
                      name: "HASURA_GRAPHQL_ENABLE_CONSOLE",
                      value: "false"
                    },
                    {
                      name: "HASURA_GRAPHQL_ENABLED_LOG_TYPES",
                      value: "startup, http-log, websocket-log"
                    }
                  ],
                  ports: [
                    {
                      containerPort: 8080,
                      name: "http"
                    }
                  ],
                  resources: {}
                }
              ]
            }
          }
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ServiceAccount(
      {
        apiVersion: "v1",
        kind: "ServiceAccount",
        metadata: {
          name: "opstrace-application",
          namespace
        }
      },
      kubeConfig
    )
  );

  collection.add(
    new ClusterRoleBinding(
      {
        apiVersion: "rbac.authorization.k8s.io/v1",
        kind: "ClusterRoleBinding",
        metadata: {
          name: "opstrace-application-clusteradmin-binding"
        },
        roleRef: {
          apiGroup: "rbac.authorization.k8s.io",
          kind: "ClusterRole",
          name: "cluster-admin"
        },
        subjects: [
          {
            kind: "ServiceAccount",
            name: "opstrace-application",
            namespace
          }
        ]
      },
      kubeConfig
    )
  );

  return collection;
}

function hasuraSecrets(
  state: State,
  kubeConfig: KubeConfig,
  namespace: string
): ResourceCollection {
  let hasuraSecretValue = Buffer.from(generateSecretValue()).toString("base64");
  // copy the secret that we've already set for Hasura in the kube-system namespace
  // TODO(nickbp): Remove this copy once we no longer use Hasura
  const hasuraSecretToCopy = state.kubernetes.cluster.Secrets.resources.find(
    secret =>
      secret.name === "hasura-admin-secret" &&
      secret.namespace === CONTROLLER_NAMESPACE
  );
  if (hasuraSecretToCopy) {
    // use the value of the secret we want to copy
    hasuraSecretValue = Buffer.from(
      hasuraSecretToCopy.data?.HASURA_ADMIN_SECRET ?? "",
      "base64"
    ).toString("base64");
  }
  // Specifying this secret in kube-system will ensure it exists if it didn't before.
  const kubeSystemHasuraAdminSecret = new Secret(
    {
      apiVersion: "v1",
      data: {
        HASURA_ADMIN_SECRET: hasuraSecretValue
      },
      kind: "Secret",
      metadata: {
        name: "hasura-admin-secret",
        namespace: "kube-system"
      }
    },
    kubeConfig
  );
  const hasuraAdminSecret = new Secret(
    {
      apiVersion: "v1",
      data: {
        HASURA_ADMIN_SECRET: hasuraSecretValue
      },
      kind: "Secret",
      metadata: {
        name: "hasura-admin-secret",
        namespace: namespace
      }
    },
    kubeConfig
  );

  const collection = new ResourceCollection();

  // We don't want this value to change once it exists either.
  // The value of this secret can always be updated manually in the cluster if needs be (kubectl delete <name> -n application) and the controller will create a new one.
  // The corresponding deployment pods that consume it will need to be restarted also to get the new env var containing the new secret.
  hasuraAdminSecret.setImmutable();
  collection.add(hasuraAdminSecret);
  kubeSystemHasuraAdminSecret.setImmutable();
  collection.add(kubeSystemHasuraAdminSecret);

  return collection;
}
