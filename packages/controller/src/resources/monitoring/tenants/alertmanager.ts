import { KubeConfig } from "@kubernetes/client-node";
import { State } from "../../../reducer";
import { Tenant } from "@opstrace/tenants";
import { getTenantNamespace } from "../../../helpers";
import { ResourceCollection, Service } from "@opstrace/kubernetes";

export function AlertManagerResources(
  state: State,
  kubeConfig: KubeConfig,
  tenant: Tenant
): ResourceCollection {
  const collection = new ResourceCollection();

  const namespace = getTenantNamespace(tenant);

  // Points to the multitenant Cortex Alertmanager.
  // This allows the Ingress at "<tenant>.<cluster>.opstrace.io/alertmanager" to route to Cortex.
  // Requests must include an "X-Scope-OrgID" header to identify the tenant, this is handled at the Ingress.
  collection.add(
    new Service(
      {
        apiVersion: "v1",
        kind: "Service",
        metadata: {
          name: "cortex-alertmanager",
          namespace
        },
        spec: {
          type: "ExternalName",
          externalName: "alertmanager.cortex.svc.cluster.local"
        }
      },
      kubeConfig
    )
  );

  return collection;
}
