import { KubeConfig, V1ConfigMap } from "@kubernetes/client-node";
import { ConfigMap } from "@opstrace/kubernetes";
import {
  ControllerOverrides,
  getControllerOverrides,
  getNodeAffinity
} from "./helpers";

// mock logger
jest.mock("@opstrace/utils", () => ({
  log: {
    info: jest.fn,
    debug: jest.fn,
    warning: jest.fn
  }
}));

jest.mock("@kubernetes/client-node");

test("should parse a simple config map with controller overrides", () => {
  const resource: V1ConfigMap = {
    data: {
      "Deployment__application__redis-master": `
spec:
 replicas: 2
`
    }
  };
  const kubeconfig = new KubeConfig();
  const cm = new ConfigMap(resource, kubeconfig);

  const expected: ControllerOverrides = {
    "Deployment__application__redis-master": { spec: { replicas: 2 } }
  };

  const result = getControllerOverrides(cm);

  expect(result).toMatchObject(expected);
});

test("should parse a more complex config map with controller overrides", () => {
  const resource: V1ConfigMap = {
    data: {
      "Deployment__application__redis-master": `
spec:
  replicas: 2
`,
      "Cortex__cortex__opstrace-cortex": `
spec:
  querier_spec:
    replicas: 1
`
    }
  };
  const kubeconfig = new KubeConfig();
  const cm = new ConfigMap(resource, kubeconfig);

  const expected: ControllerOverrides = {
    "Deployment__application__redis-master": { spec: { replicas: 2 } },
    "Cortex__cortex__opstrace-cortex": {
      spec: { querier_spec: { replicas: 1 } }
    }
  };

  const result = getControllerOverrides(cm);

  expect(result).toMatchObject(expected);
});

describe("node affinity configuration", () => {
  const node_selector_terms = {
    default: {
      key: "cloud.google.com/gke-nodepool",
      values: ["primary_nodes"]
    },
    rules: [
      {
        kind: "statefulset",
        namespace: "cortex",
        name: "ingester",
        match_expression: {
          key: "cloud.google.com/gke-nodepool",
          values: ["cortex_ingester_nodes"]
        }
      },
      {
        kind: "any",
        namespace: "cortex",
        name: "",
        match_expression: {
          key: "cloud.google.com/gke-nodepool",
          values: ["cortex_nodes"]
        }
      },
      {
        kind: "any",
        namespace: "ingress",
        name: "",
        match_expression: {
          key: "cloud.google.com/gke-nodepool",
          values: ["ingress_nodes"]
        }
      }
    ]
  };

  test("should skip when not defined", () => {
    let result = getNodeAffinity(undefined, "", "", "");
    expect(result).toBeUndefined();

    result = getNodeAffinity(
      { default: { key: "", values: [""] }, rules: [] },
      "",
      "",
      ""
    );
    expect(result).toBeUndefined();
  });

  test("should return the default affinity rule", () => {
    const result = getNodeAffinity(
      node_selector_terms,
      "statefulset",
      "doesnotexist",
      "isnotset"
    );
    expect(result).toStrictEqual({
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: [
                {
                  key: "cloud.google.com/gke-nodepool",
                  operator: "In",
                  values: ["primary_nodes"]
                }
              ]
            }
          ]
        }
      }
    });
  });

  test("should return the cortex ingester affinity rule", () => {
    const result = getNodeAffinity(
      node_selector_terms,
      "statefulset",
      "cortex",
      "ingester"
    );
    expect(result).toStrictEqual({
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: [
                {
                  key: "cloud.google.com/gke-nodepool",
                  operator: "In",
                  values: ["cortex_ingester_nodes"]
                }
              ]
            }
          ]
        }
      }
    });
  });

  test("should return the cortex namespace affinity rule", () => {
    const result = getNodeAffinity(
      node_selector_terms,
      "statefulset",
      "cortex",
      "compactor"
    );
    expect(result).toStrictEqual({
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: [
                {
                  key: "cloud.google.com/gke-nodepool",
                  operator: "In",
                  values: ["cortex_nodes"]
                }
              ]
            }
          ]
        }
      }
    });
  });

  test("should return the ingress namespace affinity rule", () => {
    const result = getNodeAffinity(
      node_selector_terms,
      "statefulset",
      "ingress",
      ""
    );
    expect(result).toStrictEqual({
      nodeAffinity: {
        requiredDuringSchedulingIgnoredDuringExecution: {
          nodeSelectorTerms: [
            {
              matchExpressions: [
                {
                  key: "cloud.google.com/gke-nodepool",
                  operator: "In",
                  values: ["ingress_nodes"]
                }
              ]
            }
          ]
        }
      }
    });
  });
});
