import { combineReducers } from "redux";

import { reducer as tenantReducer } from "@opstrace/tenants";
import { reducer as configReducer } from "@opstrace/controller-config";

import {
  storageClassesReducer,
  nodesReducer,
  persistentVolumesReducer,
  persistentVolumeClaimsReducer,
  statefulSetsReducer,
  serviceAccountsReducer,
  servicesReducer,
  secretsReducer,
  roleBindingsReducer,
  rolesReducer,
  namespacesReducer,
  deploymentsReducer,
  daemonSetsReducer,
  crdsReducer,
  configMapsReducer,
  clusterRoleBindingsReducer,
  clusterRolesReducer,
  apiServicesReducer,
  ingressesReducer,
  podSecurityPoliciesReducer,
  V1AlertmanagerReducer,
  V1PodmonitorReducer,
  V1PrometheusReducer,
  V1PrometheusruleReducer,
  V1ServicemonitorReducer,
  V1CertificateReducer,
  V1CertificaterequestReducer,
  V1ChallengeReducer,
  V1ClusterissuerReducer,
  V1IssuerReducer,
  V1OrderReducer,
  V1Alpha1CortexReducer,
  V1ClickhouseinstallationReducer,
  V1JaegerReducer
} from "@opstrace/kubernetes";

import { integrations } from "./reducers/postgres";
import {
  databases as chDatabases,
  users as chUsers
} from "./reducers/clickhouse";

export const rootReducers = {
  tenants: tenantReducer,
  config: configReducer,
  kubernetes: combineReducers({
    cluster: combineReducers({
      Nodes: nodesReducer,
      Ingresses: ingressesReducer,
      StorageClasses: storageClassesReducer,
      PersistentVolumes: persistentVolumesReducer,
      PersistentVolumeClaims: persistentVolumeClaimsReducer,
      StatefulSets: statefulSetsReducer,
      ServiceAccounts: serviceAccountsReducer,
      Services: servicesReducer,
      Secrets: secretsReducer,
      RoleBindings: roleBindingsReducer,
      Roles: rolesReducer,
      Namespaces: namespacesReducer,
      Deployments: deploymentsReducer,
      DaemonSets: daemonSetsReducer,
      CustomResourceDefinitions: crdsReducer,
      ConfigMaps: configMapsReducer,
      ClusterRoleBindings: clusterRoleBindingsReducer,
      ClusterRoles: clusterRolesReducer,
      PodSecurityPolicies: podSecurityPoliciesReducer,
      ApiServices: apiServicesReducer,
      Alertmanagers: V1AlertmanagerReducer,
      PodMonitors: V1PodmonitorReducer,
      Prometheuses: V1PrometheusReducer,
      PrometheusRules: V1PrometheusruleReducer,
      ServiceMonitors: V1ServicemonitorReducer,
      Certificates: V1CertificateReducer,
      CertificateRequests: V1CertificaterequestReducer,
      Challenges: V1ChallengeReducer,
      ClusterIssuers: V1ClusterissuerReducer,
      Issuers: V1IssuerReducer,
      Orders: V1OrderReducer,
      Cortices: V1Alpha1CortexReducer,
      Clickhouses: V1ClickhouseinstallationReducer,
      Jaegers: V1JaegerReducer
    })
  }),
  postgres: combineReducers({
    Integrations: integrations.reducer
  }),
  clickhouse: combineReducers({
    Databases: chDatabases.reducer,
    Users: chUsers.reducer
  })
};

export const rootReducer = combineReducers(rootReducers);
export type State = ReturnType<typeof rootReducer>;
