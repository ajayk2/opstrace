import { ResourceCollection } from "@opstrace/kubernetes";

import { getControllerConfig, getNodeAffinity } from "../helpers";
import { State } from "../reducer";
import { NodeSelectorTermsType } from "@opstrace/controller-config";
import { log } from "@opstrace/utils";

export function setNodeAffinity(
  state: State,
  desired: ResourceCollection
): void {
  const { node_selector_terms } = getControllerConfig(state);
  setNodeAffinityHelper(node_selector_terms, desired);
}

export function setNodeAffinityHelper(
  node_selector_terms: NodeSelectorTermsType | undefined,
  desired: ResourceCollection
): void {
  // Node term affinity is not set so skip early.
  if (
    node_selector_terms === undefined ||
    node_selector_terms.default.key === undefined ||
    node_selector_terms.default.key === ""
  ) {
    log.debug("node_selector_terms not set, skipping...");
    return;
  }

  desired.get().forEach(resource => {
    let affinity = undefined;
    switch (resource.kind) {
      case "Deployment":
        affinity = getNodeAffinity(
          node_selector_terms,
          "deployment",
          resource.namespace,
          resource.name
        );
        break;
      case "StatefulSet":
        affinity = getNodeAffinity(
          node_selector_terms,
          "statefulset",
          resource.namespace,
          resource.name
        );
        break;
      case "DaemonSet":
        affinity = getNodeAffinity(
          node_selector_terms,
          "daemonset",
          resource.namespace,
          resource.name
        );
        break;
    }
    if (affinity !== undefined) {
      if (resource.get().spec.template.spec.affinity !== undefined) {
        resource.get().spec.template.spec.affinity.nodeAffinity =
          affinity.nodeAffinity;
      } else {
        resource.get().spec.template.spec.affinity = affinity;
      }
    }
  });
}
