import { createReducer, ActionType } from "typesafe-actions";
import { configMapActions } from "@opstrace/kubernetes";
import { Tenants } from "../types";
import { isTenantStorage, deserialize } from "../utils";

type Actions = ActionType<typeof configMapActions>;

export type TenantsState = {
  tenants: Tenants;
  loading: boolean;
  backendExists: boolean | null;
  error: Error | null;
};

const tenantsInitialState: TenantsState = {
  tenants: [],
  loading: false,
  backendExists: null, // Indicates that we don't know yet
  error: null
};

export const tenantsReducer = createReducer<TenantsState, Actions>(
  tenantsInitialState
)
  .handleAction(
    configMapActions.fetch.request,
    (state): TenantsState => ({
      ...state,
      loading: true
    })
  )
  .handleAction(
    configMapActions.fetch.success,
    (state, action): TenantsState => {
      const configMap = action.payload.resources.find(isTenantStorage);
      let tenants: Tenants = [];
      if (configMap) {
        tenants = deserialize(configMap);
      }

      return {
        ...state,
        backendExists: configMap ? true : false,
        tenants,
        error: null,
        loading: false
      };
    }
  )
  .handleAction(
    configMapActions.fetch.failure,
    (state, action): TenantsState => ({
      ...state,
      ...action.payload,
      loading: false
    })
  )
  .handleAction(
    [configMapActions.onUpdated, configMapActions.onAdded],
    (state, action): TenantsState => {
      const configMap = isTenantStorage(action.payload)
        ? action.payload
        : false;

      if (!configMap) {
        return state;
      }
      const tenants = deserialize(configMap);

      return {
        ...state,
        backendExists: true,
        tenants,
        error: null,
        loading: false
      };
    }
  );
