import { log } from "@opstrace/utils";
import { Sequelize } from "sequelize";
import {
  initModels,
  integration,
  tenant,
  user,
  user_preference
} from "./models";

let _schemas: {
  integration: typeof integration;
  tenant: typeof tenant;
  user: typeof user;
  user_preference: typeof user_preference;
} | null;

// Initializes schemas for future calls to schemas().
// Can also optionally ensure that the Postgres instance has tables initialized.
export async function initSchemas(createTables: boolean) {
  if (_schemas != null) {
    throw Error("initSchemas() was already called");
  }

  const envval = process.env.POSTGRES_ENDPOINT;
  if (!envval) {
    throw Error(
      "Missing POSTGRES_ENDPOINT envvar, should be like 'postgres://user:pass@host'"
    );
  }

  const client = new Sequelize(envval, {
    // Reuse Opstrace log config when logging Postgres queries
    // TODO(nickbp): once tests are passing, switch this to use log.debug
    logging: (...params) => log.info(params)
  });
  _schemas = initModels(client);

  // TODO(nickbp) should have either controller or app do this (but not both)
  if (createTables) {
    await Promise.all([
      _schemas.integration.sync(),
      _schemas.tenant.sync(),
      _schemas.user.sync(),
      _schemas.user_preference.sync()
    ]);
  }
}

// Lazy load to avoid errors around POSTGRES_ENDPOINT until DB access is actually used.
// For example let's avoid errors when someone is just doing "app --help".
export function schemas() {
  if (_schemas == null) {
    throw Error("Call to schemas() without preceding call to initSchemas()");
  }
  return _schemas;
}
