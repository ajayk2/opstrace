import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { tenant, tenantId } from "./tenant";

export interface integrationAttributes {
  id: string;
  kind: string;
  name: string;
  tenant_id: string;
  data: object;
  key: string;
}

export type integrationPk = "id";
export type integrationId = integration[integrationPk];
// EDIT: omit implicit created_at/updated_at
export type integrationOptionalAttributes = "id" | "kind" | "data";
export type integrationCreationAttributes = Optional<
  integrationAttributes,
  integrationOptionalAttributes
>;

export class integration
  extends Model<integrationAttributes, integrationCreationAttributes>
  implements integrationAttributes
{
  id!: string;
  kind!: string;
  name!: string;
  tenant_id!: string;
  data!: object;
  key!: string;

  // integration belongsTo tenant via tenant_id
  tenant!: tenant;
  getTenant!: Sequelize.BelongsToGetAssociationMixin<tenant>;
  setTenant!: Sequelize.BelongsToSetAssociationMixin<tenant, tenantId>;
  createTenant!: Sequelize.BelongsToCreateAssociationMixin<tenant>;

  static initModel(sequelize: Sequelize.Sequelize): typeof integration {
    return integration.init(
      {
        id: {
          type: DataTypes.UUID,
          allowNull: false,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true
        },
        kind: {
          type: DataTypes.TEXT,
          allowNull: false,
          defaultValue: "k8s_metrics"
        },
        name: {
          type: DataTypes.TEXT,
          allowNull: false,
          unique: "integrations_name_tenant_id_key"
        },
        tenant_id: {
          type: DataTypes.UUID,
          allowNull: false,
          references: {
            model: "tenant",
            key: "id"
          },
          unique: "integrations_name_tenant_id_key"
        },
        data: {
          type: DataTypes.JSONB,
          allowNull: false,
          defaultValue: sequelize.fn("jsonb_build_object") // EDIT: This is a SQL function
        },
        key: {
          type: DataTypes.TEXT,
          allowNull: false,
          unique: "integration_key_tenant_id_key"
        }
      },
      {
        sequelize,
        tableName: "integration",
        schema: "public",
        hasTrigger: true,
        timestamps: true,
        underscored: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        indexes: [
          {
            name: "integration_key_tenant_id_key",
            unique: true,
            fields: [{ name: "key" }, { name: "tenant_id" }]
          },
          {
            name: "integrations_name_tenant_id_key",
            unique: true,
            fields: [{ name: "name" }, { name: "tenant_id" }]
          },
          {
            name: "integrations_pkey",
            unique: true,
            fields: [{ name: "id" }]
          }
        ]
      }
    );
  }
}
