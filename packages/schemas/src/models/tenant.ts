import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { integration, integrationId } from "./integration";

export interface tenantAttributes {
  name: string;
  type: string;
  key: string;
  id: string;
}

export type tenantPk = "name";
export type tenantId = tenant[tenantPk];
// EDIT: omit implicit created_at/updated_at
export type tenantOptionalAttributes = "type" | "id";
export type tenantCreationAttributes = Optional<
  tenantAttributes,
  tenantOptionalAttributes
>;

export class tenant
  extends Model<tenantAttributes, tenantCreationAttributes>
  implements tenantAttributes
{
  name!: string;
  type!: string;
  key!: string;
  id!: string;

  // tenant hasMany integration via tenant_id
  integrations!: integration[];
  getIntegrations!: Sequelize.HasManyGetAssociationsMixin<integration>;
  setIntegrations!: Sequelize.HasManySetAssociationsMixin<
    integration,
    integrationId
  >;
  addIntegration!: Sequelize.HasManyAddAssociationMixin<
    integration,
    integrationId
  >;
  addIntegrations!: Sequelize.HasManyAddAssociationsMixin<
    integration,
    integrationId
  >;
  createIntegration!: Sequelize.HasManyCreateAssociationMixin<integration>;
  removeIntegration!: Sequelize.HasManyRemoveAssociationMixin<
    integration,
    integrationId
  >;
  removeIntegrations!: Sequelize.HasManyRemoveAssociationsMixin<
    integration,
    integrationId
  >;
  hasIntegration!: Sequelize.HasManyHasAssociationMixin<
    integration,
    integrationId
  >;
  hasIntegrations!: Sequelize.HasManyHasAssociationsMixin<
    integration,
    integrationId
  >;
  countIntegrations!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof tenant {
    return tenant.init(
      {
        name: {
          type: DataTypes.TEXT,
          allowNull: false,
          primaryKey: true,
          // EDIT: Add explicit validation rules for tenant names
          validate: {
            is: /^[a-z0-9]{2,63}$/
          }
        },
        type: {
          type: DataTypes.TEXT,
          allowNull: false,
          defaultValue: "USER"
        },
        // TODO(nickbp): In theory we could use the stored set_key_from_name() function for assigning the key?
        key: {
          type: DataTypes.TEXT,
          allowNull: false,
          unique: "tenant_key_key"
        },
        id: {
          type: DataTypes.UUID,
          allowNull: false,
          defaultValue: DataTypes.UUIDV4,
          unique: "tenant_id_key"
        }
      },
      {
        sequelize,
        tableName: "tenant",
        schema: "public",
        hasTrigger: true,
        timestamps: true,
        underscored: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        indexes: [
          {
            name: "tenant_id_key",
            unique: true,
            fields: [{ name: "id" }]
          },
          {
            name: "tenant_key_key",
            unique: true,
            fields: [{ name: "key" }]
          },
          {
            name: "tenant_pkey",
            unique: true,
            fields: [{ name: "name" }]
          }
        ]
      }
    );
  }
}
