import type { Sequelize } from "sequelize";
import { integration as _integration } from "./integration";
import type {
  integrationAttributes,
  integrationCreationAttributes
} from "./integration";
import { tenant as _tenant } from "./tenant";
import type { tenantAttributes, tenantCreationAttributes } from "./tenant";
import { user as _user } from "./user";
import type { userAttributes, userCreationAttributes } from "./user";
import { user_preference as _user_preference } from "./user_preference";
import type {
  user_preferenceAttributes,
  user_preferenceCreationAttributes
} from "./user_preference";

export {
  _integration as integration,
  _tenant as tenant,
  _user as user,
  _user_preference as user_preference
};

export type {
  integrationAttributes,
  integrationCreationAttributes,
  tenantAttributes,
  tenantCreationAttributes,
  userAttributes,
  userCreationAttributes,
  user_preferenceAttributes,
  user_preferenceCreationAttributes
};

export function initModels(sequelize: Sequelize) {
  const integration = _integration.initModel(sequelize);
  const tenant = _tenant.initModel(sequelize);
  const user = _user.initModel(sequelize);
  const user_preference = _user_preference.initModel(sequelize);

  integration.belongsTo(tenant, { as: "tenant", foreignKey: "tenant_id" });
  tenant.hasMany(integration, { as: "integrations", foreignKey: "tenant_id" });
  user_preference.belongsTo(user, { as: "user", foreignKey: "user_id" });
  user.hasOne(user_preference, {
    as: "user_preference",
    foreignKey: "user_id"
  });

  return {
    integration: integration,
    tenant: tenant,
    user: user,
    user_preference: user_preference
  };
}
