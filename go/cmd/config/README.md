# HTTP Config API service

Serves HTTP interfaces for the following operations:

1. Serving Hasura Actions for getting and setting Alertmanager configurations in cortex
2. Serving Hasura Actions for validating credentials and exporters so that the UI can check validity before sending them to Hasura directly
3. Reading/updating/creating/deleting Credentials and Exporters via Hasura from an HTTP client like curl

The Hasura Actions support is on a separate port from the "main" config API port. This ensures that Hasura Actions support is kept private while the config API is exposed to the internet via an Ingress.

## Test environment

Terminal A:

```bash
opstrace/packages/app$ yarn services:start
```

Terminal B:

```bash
opstrace/packages/app$ yarn console
```

Terminal C:

```bash
opstrace/go/cmd/config$ go build && \
./config \
  --loglevel debug \
  --listen "127.0.0.1:8989" \
  --metrics "127.0.0.1:8990" \
  --disable-api-authn
```

The `listen` and `metrics` arguments are for two different ports:

* The `listen` port is meant to be visible to the public internet via an Ingress and is meant for users to directly apply configuration to the system. This port requires authentication via bearer token. The config service extracts the tenant name from the signed bearer token.
* The `metrics` port is for internal access (only) by Prometheus.

## Alertmanager configs

The config-api service supports fetching and setting the Alertmanager configuration for a given tenant. This support is implemented in two places:

* The `action` port implements Hasura Actions named `getAlertmanager` and `updateAlertmanager`. These are ultimately for the UI to display configuration for setting and getting the per-tenant Alertmanager configurations. The UI queries Hasura, which then routes the queries to the config-api service via Hasura Actions.
* The public `config` port meanwhile implements HTTP passthrough endpoints that forward directly to Cortex. The config-api service extracts the tenant name from the signed bearer token, then provides the tenant name to Cortex via a `X-Scope-OrgID` header.

In both cases, the config-api service is acting as a frontend to Cortex, which internally stores the Alertmanager and ruler configs in a configured S3 or GCS bucket.

### Alertmanager HTTP endpoints

The `config-api` service directly exposes `/api/v1/alerts`, `/api/v1/alertmanager` and `/api/v1/multitenant_alertmanager` endpoints, which pass-through to the equivalent Cortex endpoints. Requests must include the bearer token. The tenant name is extracted from the signed bearer token in the request, and provided to Cortex via an `X-Scope-OrgID` header. The most useful endpoint is `/api/v1/alerts`, which allows setting the Alertmanager config. The others are mainly for providing system status.

#### Alertmanager HTTP examples

Setting config via `/api/v1/alerts` using `dev` token file. See [Cortex API reference](https://cortexmetrics.io/docs/api/#set-alertmanager-configuration).

```bash
$ cat valid-test.yaml
alertmanager_config: |
  route:
    receiver: 'default-receiver'
    group_wait: 30s
    group_interval: 5m
    repeat_interval: 4h
    group_by: [cluster, alertname]
  receivers:
    - name: default-receiver

$ curl -k -H "Authorization: Bearer $(cat tenant-api-token-dev)" --data-binary @valid-test.yaml https://config.MYTENANT.MYCLUSTER.opstrace.io/api/v1/alerts
```

Fetching config via `/api/v1/alerts` using `dev` token file. See [Cortex API reference](https://cortexmetrics.io/docs/api/#get-alertmanager-configuration).

```bash
$ curl -k -H "Authorization: Bearer $(cat tenant-api-token-dev)" https://config.MYTENANT.MYCLUSTER.opstrace.io/api/v1/alerts
template_files: {}
alertmanager_config: |
  route:
    receiver: 'default-receiver'
    group_wait: 30s
    group_interval: 5m
    repeat_interval: 4h
    group_by: [cluster, alertname]
  receivers:
    - name: default-receiver
```

## Alert rules

The `config-api` service directly exposes `/api/v1/rules` and `/api/v1/ruler` endpoints, which pass-through to the equivalent Cortex endpoints. The endpoints forward directly to Cortex, with the tenant name extracted from the signed bearer token in the request, and provided to Cortex via an `X-Scope-OrgID` header. The most useful endpoints are under `/api/v1/rules`, which allows configuring alerting rules. Meanwhile `/api/v1/ruler` is mainly for providing system status.

### Alert rules HTTP examples

Setting an alert rule group named `bar` under namespace `foo` via `/api/v1/rules/foo`. See [Cortex API reference](https://cortexmetrics.io/docs/api/#set-rule-group) for this and other available calls under `/api/v1/rules`.

```bash
$ echo '
name: bar
rules:
- alert: DeadMansSwitch
  annotations:
      description: "This is a DeadMansSwitch meant to ensure that the entire Alerting pipeline is functional. See https://deadmanssnitch.com/snitches/e759300835/"
      summary: "Alerting DeadMansSwitch"
  expr: vector(1)
  labels:
      severity: warning

- alert: InstanceDown
  expr: up == 0
  for: 7m
  labels:
      severity: warning
  annotations:
      summary: "Instance {{ $labels.instance }} down"
      description: "{{ $labels.instance }} of job {{ $labels.job }} has been down for more than 7 minutes."
' | curl -v -k -H "Authorization: Bearer $(cat tenant-api-token-dev)" -H "Content-Type: application/yaml" --data-binary @- https://config.MYTENANT.MYCLUSTER.opstrace.io/api/v1/rules/foo
```
