package main

import (
	"flag"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"

	"github.com/opstrace/opstrace/go/pkg/middleware"
)

func main() {
	var loglevel string
	flag.StringVar(&loglevel, "loglevel", "info", "error|info|debug")
	var listenAddress string
	flag.StringVar(&listenAddress, "listen", "", "")
	var metricsAddress string
	flag.StringVar(&metricsAddress, "metrics", "", "")
	var tenantName string
	flag.StringVar(&tenantName, "tenantname", "", "Name of the tenant that the API is serving")

	flag.Parse()

	level, lerr := log.ParseLevel(loglevel)
	if lerr != nil {
		log.Fatalf("bad --loglevel: %s", lerr)
	}
	log.SetLevel(level)

	if listenAddress == "" {
		log.Fatalf("missing required --listen")
	}
	log.Infof("listen address: %s", listenAddress)
	log.Infof("metrics address: %s", metricsAddress)

	cortexDefault := "http://localhost"
	rulerURL := envEndpointURL("CORTEX_RULER_ENDPOINT", &cortexDefault)
	log.Infof("cortex ruler URL: %v", rulerURL)
	alertmanagerURL := envEndpointURL("CORTEX_ALERTMANAGER_ENDPOINT", &cortexDefault)
	log.Infof("cortex alertmanager URL: %v", alertmanagerURL)

	log.Infof("tenant name: %s", tenantName)

	if metricsAddress != "" {
		// Not blocking on this one, but it will panic internally if there's a problem
		go runMetricsHandler(metricsAddress)
	}

	configHandler := buildConfigHandler(tenantName, rulerURL, alertmanagerURL)
	// Block on this one
	log.Fatalf("terminated config listener: %v", http.ListenAndServe(listenAddress, configHandler))
}

func runMetricsHandler(metricsAddress string) {
	router := mux.NewRouter()
	// Run metrics listener on this internal-only address instead of the main/ingress address.
	router.Handle("/metrics", promhttp.Handler())
	log.Fatalf("terminated metrics listener: %v", http.ListenAndServe(metricsAddress, router))
}

func buildConfigHandler(
	tenantName string,
	rulerURL *url.URL,
	alertmanagerURL *url.URL,
) *mux.Router {
	router := mux.NewRouter()

	// Cortex config, see: https://github.com/cortexproject/cortex/blob/master/docs/api/_index.md

	// Cortex Ruler config
	rulerPathReplacement := func(requrl *url.URL) string {
		// Route /api/v1/ruler* requests to /ruler* on the backend
		// Note: /api/v1/rules does not need to change on the way to the backend.
		if replaced := replacePathPrefix(requrl, "/api/v1/ruler", "/ruler"); replaced != nil {
			return *replaced
		}
		return requrl.Path
	}
	rulerProxy := middleware.NewReverseProxyFixedTenant(
		tenantName,
		rulerURL,
	).ReplacePaths(rulerPathReplacement)
	router.PathPrefix("/api/v1/ruler").HandlerFunc(rulerProxy.HandleWithProxy)
	router.PathPrefix("/api/v1/rules").HandlerFunc(rulerProxy.HandleWithProxy)

	// Cortex Alertmanager config
	alertmanagerPathReplacement := func(requrl *url.URL) string {
		// Route /api/v1/multitenant_alertmanager* requests to /multitenant_alertmanager* on the backend.
		// Unlike with /alertmanager, this is more of an internal status page.
		// But we could switch it to an Ingress later if we wanted.
		if replaced := replacePathPrefix(
			requrl,
			"/api/v1/multitenant_alertmanager",
			"/multitenant_alertmanager",
		); replaced != nil {
			return *replaced
		}
		return requrl.Path
	}
	alertmanagerProxy := middleware.NewReverseProxyFixedTenant(
		tenantName,
		alertmanagerURL,
	).ReplacePaths(alertmanagerPathReplacement)
	// We don't route /alertmanager for the Alertmanager UI since it isn't useful via curl.
	// The Alertmanager UI can be viewed at '<tenant>.<cluster>.opstrace.io/alertmanager/'
	router.PathPrefix("/api/v1/alerts").HandlerFunc(alertmanagerProxy.HandleWithProxy)
	router.PathPrefix("/api/v1/multitenant_alertmanager").HandlerFunc(alertmanagerProxy.HandleWithProxy)
	return router
}

func replacePathPrefix(url *url.URL, from string, to string) *string {
	if strings.HasPrefix(url.Path, from) {
		replaced := strings.Replace(url.Path, from, to, 1)
		return &replaced
	}
	return nil
}

func envEndpointURL(envName string, defaultEndpoint *string) *url.URL {
	endpoint := os.Getenv(envName)
	if endpoint == "" {
		if defaultEndpoint == nil {
			log.Fatalf("missing required %s", envName)
		} else {
			// Try default (dev/testing)
			endpoint = *defaultEndpoint
			log.Warnf("missing %s, trying %s", envName, endpoint)
		}
	}

	endpointURL, uerr := url.Parse(endpoint)
	if uerr != nil {
		log.Fatalf("bad %s: %s", envName, uerr)
	}
	return endpointURL
}
