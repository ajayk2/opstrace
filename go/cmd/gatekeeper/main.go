package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/opstrace/opstrace/go/pkg/gatekeeper"
	log "github.com/sirupsen/logrus"
)

var (
	port                     = getEnv("PORT", ":3001")
	redisAddr                = getEnv("REDIS_ADDRESS", "127.0.0.1:6379")
	redisPassword            = getEnv("REDIS_PASSWORD", "")
	useSecureCookie          = getEnvBool("USE_SECURE_COOKIE")
	cookieName               = getEnv("COOKIE_NAME", "opstrace.sid")
	cookieSecret             = getEnv("COOKIE_SECRET", "")
	oauthClientID            = getEnv("GITLAB_OAUTH_CLIENT_ID", "")
	oauthClientSecret        = getEnv("GITLAB_OAUTH_CLIENT_SECRET", "")
	serverDomain             = getEnv("DOMAIN", "http://localhost:3001")
	gitlabAddr               = getEnv("GITLAB_INSTANCE_URL", "https://gitlab.com")
	groupAllowedAccess       = getEnv("GITLAB_GROUP_ALLOWED_ACCESS", "*")
	groupAllowedSystemAccess = getEnv("GITLAB_GROUP_ALLOWED_SYSTEM_NAMESPACE_ACCESS", "")
)

func main() {
	log.SetLevel(log.InfoLevel)

	router := gin.Default()
	router.LoadHTMLGlob("templates/*.html")

	router.Use(gatekeeper.Config(&gatekeeper.ConfigOptions{
		Port:                     port,
		RedisAddr:                redisAddr,
		RedisPassword:            redisPassword,
		UseSecureCookie:          useSecureCookie,
		CookieName:               cookieName,
		CookieSecret:             cookieSecret,
		OauthClientID:            oauthClientID,
		OauthClientSecret:        oauthClientSecret,
		ServerDomain:             serverDomain,
		GitlabAddr:               gitlabAddr,
		GroupAllowedAccess:       groupAllowedAccess,
		GroupAllowedSystemAccess: groupAllowedSystemAccess,
	}))

	// Setup our session middleware to use Redis for session persistence
	router.Use(gatekeeper.Session(&gatekeeper.SessionOptions{
		RedisAddr:       redisAddr,
		RedisPassword:   redisPassword,
		CookieName:      cookieName,
		CookieSecret:    cookieSecret,
		UseSecureCookie: useSecureCookie,
	}))
	// Configure OAuth2 and make it available to the auth controllers
	router.Use(gatekeeper.OAuth2(&gatekeeper.AuthOptions{
		ClientID:     oauthClientID,
		ClientSecret: oauthClientSecret,
		ServerDomain: serverDomain,
		GitlabAddr:   gitlabAddr,
	}))
	// Configure cache and postgres clients and make them available
	// to downstream handlers
	router.Use(gatekeeper.Database(&gatekeeper.DatabaseOptions{
		RedisAddr:     redisAddr,
		RedisPassword: redisPassword,
	}))

	// TODO(nickbp): Check if the above components/clients are ready
	router.GET("/ready", return200)
	router.GET("/live", return200)

	auth := router.Group("/auth")
	{
		// Start the oauth2 flow
		auth.GET("/start", gatekeeper.HandleAuthStart)
		// Finish the oauth2 flow
		auth.GET("/callback", gatekeeper.HandleAuthFinish)
		// Handle the external auth webhook request from nginx-ingress.
		// https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#external-authentication
		auth.GET("/webhook/:group_id", gatekeeper.HandleExternalAuthWebhook)
	}

	log.Infof("log level: %s", log.GetLevel())
	log.Infof("starting HTTP server on %s", port)

	if gin.Mode() != gin.DebugMode {
		serveWithGracefulShutdown(port, router)
	} else {
		router.Run(port)
	}
}

func return200(ctx *gin.Context) {
	ctx.String(200, "Success")
}

func serveWithGracefulShutdown(port string, router http.Handler) {
	srv := &http.Server{
		Addr:    port,
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("failed to start server: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info("shutting down gracefully")

	// Wait 10 seconds to flush existing connections
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	if err := srv.Shutdown(ctx); err != nil {
		cancel()
		log.Fatal("server shutdown error:", err)
	}
	defer cancel()

	<-ctx.Done()
	log.Info("server exiting")
}

func getEnv(key, fallback string) string {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	return v
}

func getEnvBool(key string) bool {
	v := os.Getenv(key)
	if len(v) == 0 {
		return false
	}
	bv, err := strconv.ParseBool(v)
	if err != nil {
		log.Fatalf("failed to convert env var %s into bool: %v", key, err)
	}
	return bv
}
