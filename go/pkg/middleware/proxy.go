package middleware

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"

	log "github.com/sirupsen/logrus"
)

const requestTenantHeader = "X-Scope-OrgID"

// TenantReverseProxy is an HTTP reverse proxy that communicates the static or
// dynamic tenant name tenant name via an HTTP request header to its upstream
// (backend).
//
// If `tenantName` is non-nil, then all requests coming in at the frontend side
// of the reverse proxy are treated to be for that tenant
//
// If `tenantName` is nil, the tenant name is extracted from each HTTP request,
// from a custom "X-Scope-OrgID" header.
//
// If backendPathReplacement is non-nil, then it is expected to be a function.
// It will be invoked with the request URL, and the request Path and RawPath
// (respectively) will be updated with its return values. This is to allow e.g.
// /api/v1/* -> /foo/* rewrites, but can be used for any replacement.
type TenantReverseProxy struct {
	tenantName *string
	backendURL *url.URL
	revproxy   *httputil.ReverseProxy
}

func NewReverseProxyFixedTenant(tenantName string, backendURL *url.URL) *TenantReverseProxy {
	trp := &TenantReverseProxy{
		&tenantName,
		backendURL,
		httputil.NewSingleHostReverseProxy(backendURL),
	}
	trp.revproxy.ErrorHandler = proxyErrorHandler
	if backendURL.Path != "" && backendURL.Path != "/" {
		log.Fatalf("Backend path must be empty, use backendPathReplacement: %s", backendURL.String())
	}
	return trp
}

func NewReverseProxyDynamicTenant(backendURL *url.URL) *TenantReverseProxy {
	trp := &TenantReverseProxy{
		nil,
		backendURL,
		httputil.NewSingleHostReverseProxy(backendURL),
	}
	trp.revproxy.ErrorHandler = proxyErrorHandler
	if backendURL.Path != "" && backendURL.Path != "/" {
		log.Fatalf("Backend path must be empty, use backendPathReplacement: %s", backendURL.String())
	}
	return trp
}

// Updates the internal ReverseProxy to apply the provided backendPathReplacement to request paths.
// This does not rewrite responses for e.g. HTML with embedded links, see ReplaceResponses.
func (trp *TenantReverseProxy) ReplacePaths(reqPathReplacement func(*url.URL) string) *TenantReverseProxy {
	if reqPathReplacement != nil {
		// Requests: Update URL path with replacement
		trp.revproxy.Director = pathReplacementDirector(trp.backendURL, reqPathReplacement)
	}
	return trp
}

// Copied from httputil.NewSingleHostReverseProxy with tweaks to url.Path handling to support non-append overrides.
func pathReplacementDirector(backendURL *url.URL, reqPathReplacement func(*url.URL) string) func(req *http.Request) {
	targetQuery := backendURL.RawQuery
	return func(req *http.Request) {
		req.URL.Scheme = backendURL.Scheme
		req.URL.Host = backendURL.Host

		// CUSTOM PATH LOGIC vs stock NewSingleHostReverseProxy.Director, which only supports appends
		origPath := req.URL.Path
		req.URL.Path = reqPathReplacement(req.URL)
		log.Debugf("Redirecting req=%s to dest=%s%s", origPath, backendURL.Host, req.URL.Path)
		if req.URL.RawPath != "" {
			// Also update RawPath with escaped version of replacement
			req.URL.RawPath = url.PathEscape(req.URL.Path)
		}

		if targetQuery == "" || req.URL.RawQuery == "" {
			req.URL.RawQuery = targetQuery + req.URL.RawQuery
		} else {
			req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
		}
		if _, ok := req.Header["User-Agent"]; !ok {
			// explicitly disable User-Agent so it's not set to default value
			req.Header.Set("User-Agent", "")
		}
	}
}

// Updates the internal ReverseProxy to apply the provided conversion against responses before sending
// them back to the client. This does not apply when e.g. the proxy is failing to reach the backend.
func (trp *TenantReverseProxy) ReplaceResponses(f func(req *http.Response) error) *TenantReverseProxy {
	trp.revproxy.ModifyResponse = f
	return trp
}

func (trp *TenantReverseProxy) HandleWithProxy(w http.ResponseWriter, r *http.Request) {
	// Add the tenant in the request header and then forward the request to the backend.
	if trp.tenantName != nil {
		// Use the configured tenant name
		r.Header.Set(requestTenantHeader, *trp.tenantName)
	} else {
		// Copy through the tenant name from the request
		tenantName := r.Header.Get(requestTenantHeader)
		if tenantName == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("missing %s header", requestTenantHeader)))
			return
		}
	}
	trp.revproxy.ServeHTTP(w, r)
}

func proxyErrorHandler(resp http.ResponseWriter, r *http.Request, proxyerr error) {
	// Native error handler behavior: set status and log
	resp.WriteHeader(http.StatusBadGateway)
	log.Warnf("http: proxy error: %s", proxyerr)

	// Additional: write string representation of proxy error (as bytes) to
	// response stream. Log when that fails.
	_, werr := resp.Write([]byte(proxyerr.Error()))
	if werr != nil {
		log.Errorf("writing response failed: %v", werr)
	}
}
