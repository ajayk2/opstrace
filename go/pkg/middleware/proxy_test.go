package middleware

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"regexp"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

const tenantName string = "test"

/*
Create an HTTP server to be used as upstream (backend) for the proxies to be
tested.

This server responds to requests to the path /.

It checks the value of the `X-Scope-Orgid` (well, `requestTenantHeader`)` header
and writes the tenant name to the response.
*/
func createUpstreamTenantEcho(tenantName string, t *testing.T) (*url.URL, func()) {
	router := mux.NewRouter()
	router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, tenantName, r.Header.Get(requestTenantHeader))
		fmt.Fprintf(w, "%s %s", r.URL.String(), tenantName)
	})

	backend := httptest.NewServer(router)

	upstreamURL, err := url.Parse(backend.URL)

	if err != nil {
		panic(err)
	}

	return upstreamURL, backend.Close
}

func TestReverseProxy_healthy(t *testing.T) {
	upstreamURL, upstreamClose := createUpstreamTenantEcho(tenantName, t)
	defer upstreamClose()

	// Reuse the same backend for both the querier and distributor requests.
	rp := NewReverseProxyFixedTenant(tenantName, upstreamURL)

	// Create a request to the proxy (not to the backend/upstream). The URL
	// does not really matter because we're bypassing the actual router.
	req := httptest.NewRequest("GET", "http://localhost", nil)
	w := httptest.NewRecorder()
	rp.HandleWithProxy(w, req)
	resp := w.Result()
	assert.Equal(t, 200, resp.StatusCode)
	// Check that the proxy's upstream has indeed written the response.
	assert.Equal(t, "/ test", GetStrippedBody(resp))

	req = httptest.NewRequest("GET", "http://localhost/robots.txt", nil)
	w = httptest.NewRecorder()
	rp.HandleWithProxy(w, req)
	resp = w.Result()
	assert.Equal(t, 200, resp.StatusCode)
	// Check that the proxy's upstream has indeed written the response.
	assert.Equal(t, "/robots.txt test", GetStrippedBody(resp))
}

func TestReverseProxy_pathreplace(t *testing.T) {
	upstreamURL, upstreamClose := createUpstreamTenantEcho(tenantName, t)
	defer upstreamClose()

	// Reuse the same backend for both the querier and distributor requests.
	pathReplacement := func(requrl *url.URL) string {
		if strings.HasPrefix(requrl.Path, "/replaceme") {
			return strings.Replace(requrl.Path, "/replaceme", "/foo", 1)
		}
		return requrl.Path
	}
	rp := NewReverseProxyFixedTenant(tenantName, upstreamURL).ReplacePaths(pathReplacement)

	// /replaceme => /foo
	req := httptest.NewRequest("GET", "http://localhost/replaceme", nil)
	w := httptest.NewRecorder()
	rp.HandleWithProxy(w, req)
	resp := w.Result()
	assert.Equal(t, 200, resp.StatusCode)
	// Check that the proxy's upstream has indeed written the response.
	assert.Equal(t, "/foo test", GetStrippedBody(resp))

	// /replaceme/bar => /foo/bar
	req = httptest.NewRequest("GET", "http://localhost/replaceme/bar", nil)
	w = httptest.NewRecorder()
	rp.HandleWithProxy(w, req)
	resp = w.Result()
	assert.Equal(t, 200, resp.StatusCode)
	// Check that the proxy's upstream has indeed written the response.
	assert.Equal(t, "/foo/bar test", GetStrippedBody(resp))

	// /other/bar => /other/bar (no change)
	req = httptest.NewRequest("GET", "http://localhost/other/bar", nil)
	w = httptest.NewRecorder()
	rp.HandleWithProxy(w, req)
	resp = w.Result()
	assert.Equal(t, 200, resp.StatusCode)
	// Check that the proxy's upstream has indeed written the response.
	assert.Equal(t, "/other/bar test", GetStrippedBody(resp))
}

func TestReverseProxy_unhealthy(t *testing.T) {
	// set a url for the querier and distributor so that it always fail to
	// simulate an error reaching the backend
	u, err := url.Parse("http://localhost:0")
	if err != nil {
		t.Errorf("got %v", err)
	}

	// we can reuse the same backend to send both the querier and distributor
	// requests
	rp := NewReverseProxyFixedTenant(tenantName, u)
	// create a request to the test backend
	req := httptest.NewRequest("GET", "http://localhost", nil)

	w := httptest.NewRecorder()
	rp.HandleWithProxy(w, req)
	resp := w.Result()
	assert.Equal(t, http.StatusBadGateway, resp.StatusCode)

	// Confirm that the original error message (for why the request could
	// not be proxied) is contained in the response body.
	assert.Regexp(
		t,
		regexp.MustCompile("^dial tcp .* connect: connection refused$"),
		GetStrippedBody(resp),
	)
}
