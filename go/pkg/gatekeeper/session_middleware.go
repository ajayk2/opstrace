package gatekeeper

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

type SessionOptions struct {
	RedisAddr       string
	RedisPassword   string
	CookieName      string
	CookieSecret    string
	UseSecureCookie bool
}

// Session middleware that stores session state in redis.
func Session(o *SessionOptions) gin.HandlerFunc {
	store, err := redis.NewStore(
		10,
		"tcp",
		o.RedisAddr,
		o.RedisPassword,
		[]byte(o.CookieSecret))

	if err != nil {
		log.Fatalf("failed to create redis session store: %v", err)
	}

	return func(ctx *gin.Context) {
		// Invoke the "github.com/gin-contrib/sessions" middleware
		sessions.Sessions(o.CookieName, store)(ctx)
		// Start a session and set the session options
		session := sessions.Default(ctx)
		session.Options(sessions.Options{
			Secure:   o.UseSecureCookie,
			HttpOnly: o.UseSecureCookie,
		})
		// Call next handler
		ctx.Next()
	}
}
