package gatekeeper

import (
	"github.com/gin-gonic/gin"
	gitlab "github.com/xanzy/go-gitlab"
)

type Group struct {
	ID string `uri:"group_id" binding:"required,string"`
}

func setHeaders(ctx *gin.Context, user *gitlab.User, accessLevel gitlab.AccessLevelValue) {
	ctx.Header("X-WEBAUTH-EMAIL", user.Email)
	ctx.Header("X-WEBAUTH-NAME", user.Name)
	ctx.Header("X-WEBAUTH-USERNAME", user.Username)
	ctx.Header("X-WEBAUTH-ROLE", GrafanaRoleFromGroupAccessLevel(accessLevel))
}

// TODO: update user in argus instance for group.
func updateArgus() {}

// Handles the external auth request from nginx-ingress
//
// See https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#external-authentication
func HandleExternalAuthWebhook(ctx *gin.Context) {
	var group Group
	if err := ctx.ShouldBindUri(&group); err != nil {
		AbortWithError(ctx, 400, "invalid group_id", err, false)
		return
	}
	g := GetGitLabService(ctx)
	user, _ := g.CurrentUser()

	if group.ID == "system" {
		if canAccess, accessLevel, _ := g.CanAccessSystemNamespace(); canAccess {
			updateArgus()
			setHeaders(ctx, user, accessLevel)
			ctx.String(200, "Success")
			return
		}
	} else {
		if canAccess, accessLevel, _ := g.CanAccessGroup(group.ID); canAccess {
			updateArgus()
			setHeaders(ctx, user, accessLevel)
			ctx.String(200, "Success")
			return
		}
	}
	Abort(ctx, 403, "not authorized", false)
}
