package gatekeeper

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	gitlab "github.com/xanzy/go-gitlab"
	"golang.org/x/oauth2"
)

// Grafana roles.
const (
	GrafanaViewer string = "Viewer"
	GrafanaEditor string = "Editor"
	GrafanaAdmin  string = "Admin"
)

const (
	authConfigKey    = "gatekeeper/authConfig"
	gitLabServiceKey = "gatekeeper/gitLabService"
	authTokenKey     = "gatekeeper/authToken"
)

// Map a GitLab group membership access_level to a Grafana role.
func GrafanaRoleFromGroupAccessLevel(groupAccessLevel gitlab.AccessLevelValue) string {
	if groupAccessLevel >= gitlab.MaintainerPermissions {
		return GrafanaAdmin
	}
	if groupAccessLevel >= gitlab.DeveloperPermissions {
		return GrafanaEditor
	}
	return GrafanaViewer
}

// Helper to set the oauth2 client config on the context.
func SetAuthConfig(ctx *gin.Context, cfg *oauth2.Config) {
	ctx.Set(authConfigKey, cfg)
}

// Helper to get the oauth2 client config from the context.
func GetAuthConfig(ctx *gin.Context) *oauth2.Config {
	auth, exists := ctx.Get(authConfigKey)
	if !exists {
		log.Fatal("must use Auth() middleware")
	}
	return auth.(*oauth2.Config)
}

// Helper to set the gitLabService on the context.
func SetGitLabService(ctx *gin.Context, us *GitLabService) {
	ctx.Set(gitLabServiceKey, us)
}

// Helper to get the gitLabService from the context.
func GetGitLabService(ctx *gin.Context) *GitLabService {
	us, exists := ctx.Get(gitLabServiceKey)
	if !exists {
		log.Fatal("must use AuthenticatedSessionRequired() middleware")
	}
	return us.(*GitLabService)
}

// Helper to set the authToken in the session.
func SetAuthToken(ctx *gin.Context, token *oauth2.Token) {
	session := sessions.Default(ctx)
	session.Set(authTokenKey, token)
}

// Helper to get the authToken from the session.
func GetAuthToken(ctx *gin.Context) *oauth2.Token {
	session := sessions.Default(ctx)
	token := session.Get(authTokenKey)
	if token == nil {
		log.Fatal("must use Auth() middleware")
	}
	return token.(*oauth2.Token)
}
