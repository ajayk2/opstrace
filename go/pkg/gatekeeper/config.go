package gatekeeper

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

const (
	configKey = "gatekeeper/serverConfig"
)

type ConfigOptions struct {
	Port                     string
	RedisAddr                string
	RedisPassword            string
	UseSecureCookie          bool
	CookieName               string
	CookieSecret             string
	OauthClientID            string
	OauthClientSecret        string
	ServerDomain             string
	GitlabAddr               string
	GroupAllowedAccess       string
	GroupAllowedSystemAccess string
}

// Middleware to set the config options on the context
// for downstream handlers.
func Config(c *ConfigOptions) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set(configKey, c)
	}
}

// Helper to get the server config from the context.
func GetConfig(ctx *gin.Context) *ConfigOptions {
	conf, exists := ctx.Get(configKey)
	if !exists {
		log.Fatal("must use Config() middleware")
	}
	return conf.(*ConfigOptions)
}
