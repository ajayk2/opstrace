package gatekeeper

import (
	"context"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	cache "github.com/go-redis/cache/v8"
	redis "github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	gitlab "github.com/xanzy/go-gitlab"
	"golang.org/x/oauth2"
)

type GitLabService struct {
	token *oauth2.Token
	g     *gitlab.Client
	req   *gin.Context
}

// Returns an instance of GitLabService that uses an http.Client
// configured with the authToken taken from the request context.
// The http.Client will handle token.access_token expiry and request a new
// access_token using token.refresh_token.
// All GET requests against GitLab are cached.
func NewGitLabService(ctx *gin.Context) *GitLabService {
	auth := GetAuthConfig(ctx)
	token := GetAuthToken(ctx)
	// The HTTP Client returned by auth.Client is supposed to automatically
	// refresh the authToken if it has expired
	httpClient := gitlab.WithHTTPClient(auth.Client(context.TODO(), token))

	client, err := gitlab.NewClient(token.AccessToken, httpClient)
	if err != nil {
		AbortWithError(ctx, 500, "failed to create gitlab client", err, true)
		return nil
	}

	return &GitLabService{
		g:     client,
		token: token,
		req:   ctx,
	}
}

// Get the current user.
func (s *GitLabService) CurrentUser() (*gitlab.User, error) {
	key := fmt.Sprintf("currentuser:%s", s.token.AccessToken)

	var user gitlab.User
	if err := s.cacheGet(key, &user); err == nil {
		return &user, nil
	}

	u, _, err := s.g.Users.CurrentUser()
	if err != nil {
		AbortWithError(s.req, 500, "failed to retrieve currentuser", err, true)
		return nil, err
	}
	s.cacheSet(key, u, 30*time.Second)

	return u, nil
}

// Check if current user can access the system namespace.
func (s *GitLabService) CanAccessSystemNamespace() (bool, gitlab.AccessLevelValue, error) {
	c := GetConfig(s.req)

	membership, err := s.getGroupMembership(c.GroupAllowedSystemAccess)
	if err != nil {
		return false, gitlab.NoPermissions, err
	}
	return membership.State == "active" && membership.AccessLevel > gitlab.NoPermissions,
		membership.AccessLevel,
		nil
}

// Check if current user can access the group by Id.
func (s *GitLabService) CanAccessGroup(gid interface{}) (bool, gitlab.AccessLevelValue, error) {
	membership, err := s.getGroupMembership(gid)
	if err != nil {
		return false, gitlab.NoPermissions, err
	}
	return membership.State == "active" && membership.AccessLevel > gitlab.NoPermissions,
		membership.AccessLevel,
		nil
}

// Get the current user's group membership by group Id.
func (s *GitLabService) getGroupMembership(gid interface{}) (*gitlab.GroupMember, error) {
	u, err := s.CurrentUser()
	if err != nil {
		return nil, err
	}
	key := fmt.Sprintf("groupmember:%d:%d", gid, u.ID)

	var groupMember gitlab.GroupMember
	if err := s.cacheGet(key, &groupMember); err == nil {
		return &groupMember, nil
	}

	g, _, err := s.g.GroupMembers.GetGroupMember(gid, u.ID)
	if err != nil {
		AbortWithError(s.req, 500, "failed to retrieve groupmembership", err, true)
		return nil, err
	}
	s.cacheSet(key, g, 30*time.Second)

	return g, nil
}

// Get key from cache.
func (s *GitLabService) cacheGet(key string, value interface{}) error {
	c := GetCache(s.req)
	ctx := context.TODO()

	err := c.Get(ctx, key, &value)
	if err != nil && err != redis.Nil {
		log.Errorf("cache retrieval failure for key [%s]: %+v", key, err)
	}
	return err
}

// Set key/value in cache with TTL.
func (s *GitLabService) cacheSet(key string, value interface{}, ttl time.Duration) {
	c := GetCache(s.req)
	ctx := context.TODO()

	err := c.Set(&cache.Item{
		Ctx:   ctx,
		Key:   key,
		Value: value,
		TTL:   ttl,
	})
	if err != nil {
		log.Errorf("cache set failure for key [%s]: %+v", key, err)
	}
}
