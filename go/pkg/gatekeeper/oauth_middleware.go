package gatekeeper

import (
	"fmt"
	"net/url"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
)

type AuthOptions struct {
	ClientID     string
	ClientSecret string
	GitlabAddr   string
	ServerDomain string
}

// Ensures request is authenticated. If autoAuth=true, will redirect
// client through the auth flow and then return back to this route.
// If autoAuth=false, will return an unauthorized response to client.
func AuthenticatedSessionRequired(autoAuth bool) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		session := sessions.Default(ctx)
		token := GetAuthToken(ctx)

		if token == nil && autoAuth {
			authURI, err := url.Parse("/auth/start")
			if err != nil {
				AbortWithError(ctx, 500, "failed to parse authURI", err, true)
				return
			}
			queryString := authURI.Query()
			// Add current URL as the rt parameter so we'll return
			// back to this route upon successful authentication
			queryString.Set("rt", ctx.Request.URL.String())
			authURI.RawQuery = queryString.Encode()

			ctx.Redirect(302, authURI.String())
		}

		if token == nil {
			Abort(ctx, 404, "unauthorized", false)
			return
		}

		gitLabService := NewGitLabService(ctx)

		// Make gitLabService available to all down stream handlers
		SetGitLabService(ctx, gitLabService)
		// Invoke downstream handlers
		ctx.Next()
		// Persist the session after any changes in downstream handlers
		if err := session.Save(); err != nil {
			AbortWithError(ctx, 500, "failed to save session", err, true)
			return
		}
	}
}

// Make the oauth2 config available on the request context.
// Handlers can then invoke the the oauth flow and use the
// authenticated http.client for interacting with the gitlab API that
// is available on the oauth2.Config object.
func OAuth2(o *AuthOptions) gin.HandlerFunc {
	ac := &oauth2.Config{
		ClientID:     o.ClientID,
		ClientSecret: o.ClientSecret,
		Scopes:       []string{"api"},
		RedirectURL:  fmt.Sprintf("%s/auth/callback", o.ServerDomain),
		Endpoint: oauth2.Endpoint{
			AuthURL:  fmt.Sprintf("%s/oauth/authorize", o.GitlabAddr),
			TokenURL: fmt.Sprintf("%s/oauth/token", o.GitlabAddr),
		},
	}

	return func(ctx *gin.Context) {
		// Make authConfig available to all downstream handlers
		SetAuthConfig(ctx, ac)
		ctx.Next()
	}
}
