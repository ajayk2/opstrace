package gatekeeper

import (
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

// AbortWithError will log the full error and then call Abort.
//
// If `mask` is true, a generic response will be sent to the client to avoid leaking
// specifics about system internals.
func AbortWithError(ctx *gin.Context, status int, message string, err error, mask bool) {
	if ctx.IsAborted() {
		// No need to log cascading failures
		return
	}
	log.Errorf("AbortError: %+v", err)
	Abort(ctx, status, message, mask)
}

// Abort will log the error message, return an error to the client and abort
// the request context to prevent downstream request handlers/middleware being invoked.
//
// If `mask` is true, a generic response will be sent to the client to avoid leaking
// specifics about system internals.
func Abort(ctx *gin.Context, status int, message string, mask bool) {
	if ctx.IsAborted() {
		// No need to log cascading failures
		return
	}
	log.Errorf("AbortMessage: %s", message)
	title := "Error"

	if status == http.StatusNotFound {
		title = "Not Found"
	}
	if mask {
		title = "Internal Server Error"
		message = "Something went wrong"
		status = 500
	}
	ctx.HTML(status, "error.html", gin.H{"title": title, "message": message})
	ctx.Abort()
}
