module github.com/opstrace/opstrace/go

go 1.15

require (
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/Microsoft/hcsshim v0.8.22 // indirect
	github.com/antonlindstrom/pgstore v0.0.0-20200229204646-b08ebf1105e0 // indirect
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b // indirect
	github.com/bradleypeabody/gorilla-sessions-memcache v0.0.0-20181103040241-659414f458e1 // indirect
	github.com/containerd/containerd v1.5.7 // indirect
	github.com/containerd/continuity v0.2.0 // indirect
	github.com/docker/docker v20.10.9+incompatible // indirect
	github.com/gin-contrib/sessions v0.0.4 // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/go-redis/cache/v8 v8.4.3 // indirect
	github.com/go-redis/redis/v8 v8.11.4 // indirect
	github.com/gogo/protobuf v1.3.2
	github.com/golang-jwt/jwt/v4 v4.1.0
	github.com/golang/snappy v0.0.4
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/json-iterator/go v1.1.12
	github.com/kidstuff/mongostore v0.0.0-20181113001930-e650cd85ee4b // indirect
	github.com/lib/pq v1.10.3 // indirect
	github.com/lithammer/dedent v1.1.0
	github.com/memcachier/mc v2.0.1+incompatible // indirect
	github.com/moby/term v0.0.0-20210619224110-3f7ff695adc6 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/onsi/gomega v1.16.0 // indirect
	github.com/open-telemetry/opentelemetry-collector-contrib/exporter/jaegerexporter v0.43.0
	github.com/open-telemetry/opentelemetry-collector-contrib/extension/pprofextension v0.43.0
	github.com/open-telemetry/opentelemetry-collector-contrib/processor/tailsamplingprocessor v0.43.0
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/prometheus v1.8.2-0.20190525122359-d20e84d0fb64
	github.com/quasoft/memstore v0.0.0-20191010062613-2bce066d2b0b // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/testcontainers/testcontainers-go v0.11.1
	github.com/ugorji/go v1.2.6 // indirect
	github.com/xanzy/go-gitlab v0.55.1
	go.opentelemetry.io/collector v0.43.1
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
	gotest.tools/v3 v3.0.3
)
