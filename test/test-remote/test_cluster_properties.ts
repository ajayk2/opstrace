import got from "got";
import {
  log,
  logHTTPResponse,
  globalTestSuiteSetupOnce,
  CLUSTER_BASE_URL
} from "./testutils";

suite("basic cluster properties", function () {
  suiteSetup(async function () {
    log.info("suite setup");
    await globalTestSuiteSetupOnce();
  });

  suiteTeardown(async function () {
    log.info("suite teardown");
  });

  test("GET / and follow redirects and inspect HTML", async function () {
    log.info("GET %s and follow redirects", CLUSTER_BASE_URL);
    const response = await got(CLUSTER_BASE_URL, {
      throwHttpErrors: false,
      followRedirect: true,
      https: { rejectUnauthorized: false },
      timeout: {
        connect: 10000,
        request: 60000
      }
    });

    logHTTPResponse(response);

    if (response.statusCode == 200 && response.body) {
      const needle = "<title>Opstrace</title>";
      if (response.body.includes(needle)) {
        log.info("found needle in HTML: %s", needle);
        return;
      }
    }

    throw new Error("didn't see expected HTML");
  });
});
