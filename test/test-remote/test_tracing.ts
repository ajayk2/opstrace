import { strict as assert } from "assert";
import fs from "fs";

import {
  ChannelCredentials,
  credentials,
  loadPackageDefinition,
  Metadata
} from "@grpc/grpc-js";
import { loadSync } from "@grpc/proto-loader";
import Long from "long";

import { OTLPTraceExporter } from "@opentelemetry/exporter-trace-otlp-grpc";
import { Resource } from "@opentelemetry/resources";
import { NodeTracerProvider } from "@opentelemetry/sdk-trace-node";
import { Span } from "@opentelemetry/sdk-trace-base";
import api from "@opentelemetry/api";

import { Instant } from "@js-joda/core";
import {
  globalTestSuiteSetupOnce,
  log,
  rndstring,
  sleep,
  OPSTRACE_INSTANCE_DNS_NAME,
  TENANT_DEFAULT_API_TOKEN,
  TENANT_SYSTEM_API_TOKEN
} from "./testutils";
import { PortForward } from "./testutils/portforward";

// Checks for the existence of a letsencrypt staging certificate.
// If one is found, adds it to the returned SSL options.
function getGrpcSSLOptions(caCertPath: string | undefined): ChannelCredentials {
  if (caCertPath !== undefined) {
    log.info(`using custom CA root cert: ${caCertPath}`);
    return credentials.createSsl(fs.readFileSync(caCertPath));
  } else {
    log.info(`using system CA root certs`);
    return credentials.createSsl();
  }
}

// Set up exporter that sends traces to default tenant
// We return the underlying OTLPTraceExporter to expose its send() call.
function getTenantExporter(
  tenantName: string,
  tenantToken: string,
  caCertPath: string | undefined
): OTLPTraceExporter {
  const metadata = new Metadata();
  metadata.set("Authorization", `Bearer ${tenantToken}`);

  const collectorOptions = {
    url: `grpc://tracing.${tenantName}.${OPSTRACE_INSTANCE_DNS_NAME}:4317`,
    // If credentials isn't specified, plain http is used and we get back an error like this:
    // "13 INTERNAL: Received RST_STREAM with code 2 triggered by internal client error: Protocol error"
    credentials: getGrpcSSLOptions(caCertPath),
    metadata
  };
  return new OTLPTraceExporter(collectorOptions);
}

async function sendSpanWithCert(
  tenantName: string,
  tenantToken: string,
  caCertPath: string | undefined,
  span: Span
) {
  const exporter = getTenantExporter(tenantName, tenantToken, caCertPath);
  await new Promise((resolve, reject) =>
    exporter.send(
      [span],
      () => {
        log.info("sent span successfully");
        return resolve(null);
      },
      err =>
        reject(`failed to send span: message=${err.message} stack=${err.stack}`)
    )
  );
  await exporter.shutdown();
}

async function sendSpan(tenantName: string, tenantToken: string, span: Span) {
  try {
    // Try with LE staging CA cert first - CI uses this
    await sendSpanWithCert(
      tenantName,
      tenantToken,
      `${__dirname}/containers/letsencrypt-stg-root-x1.pem`,
      span
    );
  } catch (e: any) {
    log.debug(`sending with staging certs failed: ${e}`);
    // Fall back to system CA certs - manual clusters may be using letsencrypt-prod
    await sendSpanWithCert(tenantName, tenantToken, undefined, span);
  }
}

async function waitForMatchingSpans(
  jaegerNamespace: string,
  traceQuery: Record<string, unknown>,
  protoDescriptor: any,
  expectedSpans = 1
): Promise<any[]> {
  // We could query https://default.<OPSTRACE_HOST>/jaeger, but that requires Auth0 creds.
  // So instead use a port forward to reach the jaeger pod directly.
  const portForwardJaeger = new PortForward(
    "jaeger-default", // name (arbitrary/logging)
    "deployments/jaeger", // k8sobj
    16685, // grpc
    jaegerNamespace
  );
  const localPortJaeger = await portForwardJaeger.setup();

  try {
    const queryService = new protoDescriptor.jaeger.api_v2.QueryService(
      `127.0.0.1:${localPortJaeger}`,
      credentials.createInsecure()
    );

    // Wait up to ~60s for span to appear in queries.
    // There should normally be up to a 5s delay for the next batch from the collector to Jaeger,
    // plus another <5s or so for the trace to appear in queries, but waiting longer should avoid flakes.
    for (const attempt of Array.from({ length: 60 }, (_v, k) => k)) {
      log.info(`querying for spans (attempt ${attempt})...`);
      let gotSpans: any[] = [];
      await new Promise((resolve, reject) => {
        const call = queryService.findTraces({ query: traceQuery });
        call.on("data", (data: any) => {
          // Avoid logging by default: Very noisy when checking e.g. cortex spans in system tenant
          log.debug(`findTraces data: ${JSON.stringify(data)}`);
          gotSpans = data["spans"];
        });
        call.on("end", () => {
          log.debug("findTraces end");
          resolve(null);
        });
        call.on("error", (e: any) => {
          reject(`findTraces error: ${JSON.stringify(e)}`);
        });
        call.on("status", (status: any) => {
          log.debug(`findTraces status: ${JSON.stringify(status)}`);
        });
      });

      if (gotSpans.length >= expectedSpans) {
        log.info(`got ${gotSpans.length}/${expectedSpans} spans, exiting loop`);
        return gotSpans;
      }

      log.info(
        `got ${gotSpans.length}/${expectedSpans} spans, waiting 1s for spans to appear`
      );
      await sleep(1);
    }
    return [];
  } finally {
    await portForwardJaeger.terminate();
  }
}

suite("End-to-end tracing tests", function () {
  // Parse/generate query proto definition: reused across jaeger queries
  const jaegerPackageDefinition = loadSync("jaeger_proto/query.proto", {
    longs: String
  });
  const jaegerProtoDescriptor = loadPackageDefinition(
    jaegerPackageDefinition
  ) as any;

  suiteSetup(async function () {
    log.info("suite setup");
    await globalTestSuiteSetupOnce();
  });

  suiteTeardown(async function () {
    log.info("suite teardown");
  });

  test("Sending and reading default tenant single span", async function () {
    const spanLabel = rndstring(10);
    const serviceName = "test_otlp_tracing_api";
    const operationName = `test_${spanLabel}`;

    // Generate a basic span to send to the server
    const tracer = new NodeTracerProvider({
      resource: new Resource({
        "service.name": serviceName
      })
    }).getTracer("otel_library_name");
    const span = tracer.startSpan(operationName);
    span.setAttribute("testing", spanLabel);
    span.addEvent("emitting test span");
    span.end();

    const sentTimeSecs = Instant.now().toEpochMilli() / 1000;

    log.info(
      `sending span: service=${serviceName} operationName=${operationName}`
    );
    await sendSpan("default", TENANT_DEFAULT_API_TOKEN, span as Span);

    // A very loose query range to avoid e.g. issues with drift on the test machine vs the server
    // Explicit conversion to Long is required, or else server fails to deserialize and returns an error like this:
    //   failed when searching for traces: stream error: rpc error: code = Unknown desc = start time is required for search queries
    const startTimeMinSecs = Long.fromNumber(sentTimeSecs - 3600);
    const startTimeMaxSecs = Long.fromNumber(sentTimeSecs + 3600);

    // This is a TraceQueryParameters, see jaeger_proto/query.proto
    const traceQuery = {
      serviceName,
      operationName,
      tags: {
        // Added to span via setAttribute() above.
        // Not strictly required since operationName is already unique, but doesn't hurt.
        testing: spanLabel
      },
      startTimeMin: {
        seconds: startTimeMinSecs
      },
      startTimeMax: {
        seconds: startTimeMaxSecs
      },
      // Must be explicitly non-zero or else we won't get any data.
      searchDepth: 1
    };

    const foundMatch = await waitForMatchingSpans(
      "default-tenant",
      traceQuery,
      jaegerProtoDescriptor
    );
    log.info(`default tenant single trace: ${JSON.stringify(foundMatch)}`);
    assert.strictEqual(foundMatch.length, 1);
  });

  test("Sending and reading default tenant fragmented spans", async function () {
    const spanLabel = rndstring(10);
    const serviceName = "test_otlp_tracing_api";
    const operationName = `test_${spanLabel}`;

    // Generate a basic span to send to the server
    const tracer = new NodeTracerProvider({
      resource: new Resource({
        "service.name": serviceName
      })
    }).getTracer("otel_library_name");

    const rootContext = api.context.active();
    const rootSpan = tracer.startSpan(
      operationName,
      { root: true },
      rootContext
    );
    rootSpan.setAttribute("testing", spanLabel);
    rootSpan.setAttribute("fragment", "root");
    rootSpan.addEvent("emitting root test span");

    const childContext = api.trace.setSpan(rootContext, rootSpan);
    const childSpan = tracer.startSpan(
      operationName,
      { root: false },
      childContext
    );
    childSpan.setAttribute("testing", spanLabel);
    childSpan.setAttribute("fragment", "child");
    childSpan.addEvent("emitting child test span");

    const grandchildContext = api.trace.setSpan(childContext, childSpan);
    const grandchildSpan = tracer.startSpan(
      operationName,
      { root: false },
      grandchildContext
    );
    grandchildSpan.setAttribute("testing", spanLabel);
    grandchildSpan.setAttribute("fragment", "grandchild");
    grandchildSpan.addEvent("emitting grandchild test span");

    grandchildSpan.end();
    childSpan.end();
    rootSpan.end();

    // check that the resulting spans actually share the same trace:
    assert.strictEqual(
      rootSpan.spanContext().traceId,
      childSpan.spanContext().traceId
    );
    assert.strictEqual(
      childSpan.spanContext().traceId,
      grandchildSpan.spanContext().traceId
    );

    const sentTimeSecs = Instant.now().toEpochMilli() / 1000;

    log.info(
      `sending spans: service=${serviceName} operationName=${operationName}`
    );
    // Send as three separate API calls - check that the server reconstructs
    await sendSpan("default", TENANT_DEFAULT_API_TOKEN, rootSpan as Span);
    await sendSpan("default", TENANT_DEFAULT_API_TOKEN, childSpan as Span);
    await sendSpan("default", TENANT_DEFAULT_API_TOKEN, grandchildSpan as Span);

    // A very loose query range to avoid e.g. issues with drift on the test machine vs the server
    // Explicit conversion to Long is required, or else server fails to deserialize and returns an error like this:
    //   failed when searching for traces: stream error: rpc error: code = Unknown desc = start time is required for search queries
    const startTimeMinSecs = Long.fromNumber(sentTimeSecs - 3600);
    const startTimeMaxSecs = Long.fromNumber(sentTimeSecs + 3600);

    // This is a TraceQueryParameters, see jaeger_proto/query.proto
    const traceQuery = {
      serviceName,
      operationName,
      tags: {
        // Added to span via setAttribute() above.
        // Not strictly required since operationName is already unique, but doesn't hurt.
        testing: spanLabel
      },
      startTimeMin: {
        seconds: startTimeMinSecs
      },
      startTimeMax: {
        seconds: startTimeMaxSecs
      },
      // Must be explicitly non-zero or else we won't get any data.
      searchDepth: 1
    };

    const foundMatch = await waitForMatchingSpans(
      "default-tenant",
      traceQuery,
      jaegerProtoDescriptor,
      3
    );
    log.info(`default tenant fragmented trace: ${JSON.stringify(foundMatch)}`);
    assert.strictEqual(foundMatch.length, 3, JSON.stringify(foundMatch));

    // fragments should have matching traceId
    assert.strictEqual(
      foundMatch[0]["traceId"]["data"],
      foundMatch[1]["traceId"]["data"]
    );
    assert.strictEqual(
      foundMatch[0]["traceId"]["data"],
      foundMatch[2]["traceId"]["data"]
    );

    // child@1 should reference root@0
    assert.strictEqual(
      foundMatch[0]["spanId"]["data"],
      foundMatch[1]["references"][0]["spanId"]["data"]
    );

    // grandchild@2 should reference child@1
    assert.strictEqual(
      foundMatch[1]["spanId"]["data"],
      foundMatch[2]["references"][0]["spanId"]["data"]
    );
  });

  test("Reading cortex system tenant spans", async function () {
    const sentTimeSecs = Instant.now().toEpochMilli() / 1000;
    // A very loose query range to avoid e.g. issues with drift on the test machine vs the server
    // Explicit conversion to Long is required, or else server fails to deserialize and returns an error like this:
    //   failed when searching for traces: stream error: rpc error: code = Unknown desc = start time is required for search queries
    const startTimeMinSecs = Long.fromNumber(sentTimeSecs - 3600);
    const startTimeMaxSecs = Long.fromNumber(sentTimeSecs + 3600);

    // This is a TraceQueryParameters, see jaeger_proto/query.proto
    const traceQuery = {
      serviceName: "cortex-ingester",
      startTimeMin: {
        seconds: startTimeMinSecs
      },
      startTimeMax: {
        seconds: startTimeMaxSecs
      },
      // Must be explicitly non-zero or else we won't get any data.
      searchDepth: 1
    };

    const foundMatch = await waitForMatchingSpans(
      "system-tenant",
      traceQuery,
      jaegerProtoDescriptor
    );
    assert.notEqual(foundMatch.length, 0);
  });

  test("Tracing auth sanity checks", async function () {
    const spanLabel = rndstring(10);
    const serviceName = "test_otlp_tracing_api";
    const operationName = `test_${spanLabel}`;

    // Generate a basic span to send to the server
    const tracer = new NodeTracerProvider({
      resource: new Resource({
        "service.name": serviceName
      })
    }).getTracer("otel_library_name");
    const span = tracer.startSpan(operationName);
    span.setAttribute("testing", spanLabel);
    span.addEvent("emitting test span");
    span.end();

    log.info(
      `sending span: service=${serviceName} operationName=${operationName}`
    );

    // Empty string
    try {
      await sendSpan("default", "", span as Span);
      fail("Expected empty token to throw exception");
    } catch (_e: any) {
      // expected grpc error
    }

    // Bad token
    try {
      await sendSpan("default", "bad" + TENANT_DEFAULT_API_TOKEN, span as Span);
      fail("Expected bad token to throw exception");
    } catch (_e: any) {
      // expected grpc error
    }

    // Wrong tenant token
    try {
      await sendSpan("default", TENANT_SYSTEM_API_TOKEN, span as Span);
      fail(
        "Expected sending system token to default tenant to throw exception"
      );
    } catch (_e: any) {
      // expected grpc error
    }
    try {
      await sendSpan("system", TENANT_DEFAULT_API_TOKEN, span as Span);
      fail(
        "Expected sending default token to system tenant to throw exception"
      );
    } catch (_e: any) {
      // expected grpc error
    }

    // Lastly, check that the tokens work when used correctly
    await sendSpan("default", TENANT_DEFAULT_API_TOKEN, span as Span);
    await sendSpan("system", TENANT_SYSTEM_API_TOKEN, span as Span);
  });
});
