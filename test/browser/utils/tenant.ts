import { expect, Page } from "@playwright/test";

export const createTenant = async (
  tenantName: string,
  { page }: { page: Page }
) => {
  await page.click("[data-test='tenant/addBtn']");
  expect(
    await page.isVisible("[data-test='pickerService/dialog/addTenant']")
  ).toBeTruthy();
  await page.fill(
    "[data-test='pickerService/input'] > input",
    `add tenant: ${tenantName}`
  );
  await page.click("[data-test='pickerService/option/yes']");
  expect(
    await page.waitForSelector(`[data-test='tenant/row/${tenantName}']`)
  ).toBeTruthy();
};

export const makeTenantName = (prefix?: string) =>
  `${prefix || "treetops"}${Math.floor(Math.random() * 10000)}`;
