# Sending traces with otel-collector

In this guide we show how to configure an `otel-collector` agent to securely send traces to your Opstrace instance.

## Prerequisites

* An Opstrace instance.
* A decision: for which Opstrace tenant would you like to send data?
* An Opstrace tenant authentication token file (for the tenant of your choice). Also see [concepts](../../references/concepts.md).

## Example Kubernetes Deployment

This example shows how you could configure an OpenTelemetry Collector instance to send traces to Opstrace.
The OpenTelemetry Collector supports a variety of inputs, automatically converting them to the OTLP format that Opstrace expects.
Note that we are specifically using `otel-collector-contrib` as it includes the required `bearertokenauth` extension.

To get started, copy the ConfigMap and Deployment definitions in the following code block, and then make the following changes to the copy:

* Enable any desired `receivers` supported by `otel-collector-contrib`. This example only enables an OTLP receiver.
* Replace stub values:
  * Replace `OPSTRACE_TENANT` with the tenant name
  * Replace `OPSTRACE_TENANT_TOKEN` with the tenant auth token (from `tenant-api-token-<tenant>`)
  * Replace `OPSTRACE_DOMAIN` with the Opstrace instance domain
* Apply the edited configuration to your cluster as follows:
    `kubectl apply -f filename.yaml`

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: otel-collector
  namespace: kube-system
  labels:
    k8s-app: otel-collector
data:
  otel.yaml: |
    extensions:
      bearertokenauth:
        # Put the content of tenant-api-token-<tenant> here
        token: "OPSTRACE_TENANT_TOKEN"

    receivers:
      # Receive traces from kubelet
      # See also: https://kubernetes.io/docs/concepts/cluster-administration/system-traces
      otlp:
        protocols:
          grpc: {}

    processors:
      batch: {}

    exporters:
      # Log what's happening
      logging: {}
      # Send spans to opstrace instance
      otlp:
        endpoint: tracing.OPSTRACE_TENANT.OPSTRACE_DOMAIN:4317
        auth:
          authenticator: bearertokenauth

    # Set up the pipeline linking together the above components
    service:
      extensions: [bearertokenauth]
      pipelines:
        traces:
          receivers: [otlp]
          processors: [batch]
          exporters: [logging, otlp]

---

kind: Deployment
apiVersion: apps/v1
metadata:
  name: otel-collector
  namespace: kube-system
  labels:
    k8s-app: otel-collector
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: otel-collector
  template:
    metadata:
      labels:
        k8s-app: otel-collector
    spec:
      nodeSelector:
        kubernetes.io/arch: amd64 # image is amd64-only
      containers:
      - name: otel-collector
        image: docker.io/otel/opentelemetry-collector-contrib:0.42.0
        command: [/otelcol-contrib, --config, /config/otel.yaml]
        volumeMounts:
        - name: config
          mountPath: /config
      volumes:
      - name: config
        configMap:
          name: otel-collector
```
